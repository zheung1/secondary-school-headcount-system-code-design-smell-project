﻿using Headcount.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Headcount.Controllers
{
    public class GuestController : Controller
    {
        // GET: Guest
        public ActionResult Index()
        {
            return View();
        }

       
        public ActionResult GetGuestList()
        {
            Guest guest = new Guest();

            guest.Uuid = (string)Session["uuid"];

            int start = Convert.ToInt32(Request["start"]);

            int length = Convert.ToInt32(Request["length"]);

            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];

            string sortDirection = Request["order[0][dir]"];

            List<Guest>guests = guest.GetGuestList(start, length, searchValue, sortColumnName, sortDirection);

            int filteredCount = guest.GetFilteredCount(searchValue);

            int totalCount = guest.GetGuestCount();

            return Json(new
            {
                guests, recordsTotal = totalCount, draw = Request["draw"], recordsFiltered = filteredCount
            },
            JsonRequestBehavior.AllowGet
            );
            
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Save(
            string guestName,
            string guestAddress,
            string guestHp,
            string guestOccupation,
            string guestRemarks,
            string guestEmail,
            string guestHomeTel,
            string guestOfficeTel,
            string username,
            string password
        )
        {
            Guest guest = new Guest();
            guest.Username = DbConfig.EscapeString(username);
            guest.Password = DbConfig.EscapeString(password);
            guest.Type = "guest";
            guest.GuestEmail = DbConfig.EscapeString(guestEmail);
            guest.GuestName = DbConfig.EscapeString(guestName);
            guest.GuestAddress = DbConfig.EscapeString(guestAddress);
            guest.GuestHomeTel = DbConfig.EscapeString(guestHomeTel);
            guest.GuestOfficeTel = DbConfig.EscapeString(guestOfficeTel);
            guest.GuestHp = DbConfig.EscapeString(guestHp);
            guest.GuestOccupation = DbConfig.EscapeString(guestOccupation);
            guest.GuestRemarks = DbConfig.EscapeString(guestRemarks);
            guest.Uuid = (string)Session["uuid"];

            guest.SaveGuest();

            return Json(new
            {
                result = "Maklumat pengguna tetamu berjaya disimpan"
            
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult ShowInfo(int id)
        {
            Guest guest = new Guest();

            guest = guest.GetGuest(id);

            ViewBag.guest = guest;

            return View();
        }

        public ActionResult Edit(int id)
        {
            Guest guest = new Guest();

            guest = guest.GetGuest(id);

            ViewBag.guest = guest;

            return View();
        }

        public ActionResult SaveEdit(
            int userId, 
            string guestName, 
            string guestAddress, 
            string guestHp,
            string guestOccupation,
            string guestRemarks, 
            string guestEmail, 
            string guestHomeTel, 
            string guestOfficeTel, 
            string username, 
            string password
        )
        {
            
            Guest guest = new Guest();
            
            guest.UserId = userId;
            guest.Username = DbConfig.EscapeString(username);
            guest.Password = DbConfig.EscapeString(password);
            guest.GuestEmail = DbConfig.EscapeString(guestEmail);
            guest.GuestName = DbConfig.EscapeString(guestName);
            guest.GuestAddress = DbConfig.EscapeString(guestAddress);
            guest.GuestHomeTel = DbConfig.EscapeString(guestHomeTel);
            guest.GuestOfficeTel = DbConfig.EscapeString(guestOfficeTel);
            guest.GuestHp = DbConfig.EscapeString(guestHp);
            guest.GuestOccupation = DbConfig.EscapeString(guestOccupation);
            guest.GuestRemarks = DbConfig.EscapeString(guestRemarks);

            guest.UpdateGuest();
            

            return Json(new
            {
                result = "Maklumat pengguna tetamu berjaya disunting"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult Delete(int id)
        {
            Guest guest = new Guest();

            guest.DeleteGuest(id);

            return Json(new
            {
                result = "Maklumat pengguna tetamu berjaya dipadam"
            },
            JsonRequestBehavior.AllowGet
            );
        }
        

        public string GuestDropdownList()
        {
            Guest guest = new Guest();

            ArrayList dropdownOptions = guest.GetGuestDropdownList();

            var json = new JavaScriptSerializer().Serialize(dropdownOptions);
            return json;

        }
    }


}