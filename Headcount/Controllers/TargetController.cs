﻿using Headcount.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
    
    
    public class TargetController : Controller
    {
        // GET: Target
        public ActionResult Index(int id, int subjectId, int examId, int page = 1)
        {
            //get exam name, mark type
            Exam exam = new Exam();
            exam = exam.GetExam(examId);

            //get subject name
            Subject subject = new Subject();
            subject = subject.GetSubject(subjectId);

            //get class name
            Class kelas = new Class();
            kelas = kelas.GetClass(id);

            ViewBag.exam = exam;
            ViewBag.subject = subject;
            ViewBag.kelas = kelas;
            ViewBag.page = page;

            return View();
        }

        public ActionResult GetTargetCount(int classId, int subjectId, int examId)
        {
            Class kelas = new Class();

            kelas = kelas.GetClass(classId);

            Target target = new Target();

            Exam exam = new Exam();
            exam.ExamId = examId;

            Subject subject = new Subject();
            subject.SubjectId = subjectId;
            subject.SubjectExam = exam;

            kelas.ClassId = classId;
            kelas.ClassSubject = subject;

            target.TargetClass = kelas;

            //get class list related to the subject
            int targetCount = target.GetTargetCount();

            return Json(new
            {
                result = targetCount
            },
            JsonRequestBehavior.AllowGet
            );
        }


        public ActionResult GetTargetList(int classId, int subjectId, int examId, int pageNumber)
        {
            Class kelas = new Class();

            kelas = kelas.GetClass(classId);

            Target target = new Target();

            ArrayList targets = new ArrayList();

            Exam exam = new Exam();
            exam = exam.GetExam(examId);

            Subject subject = new Subject();
            subject.SubjectId = subjectId;
            subject.SubjectExam = exam;

            kelas = kelas.GetClass(classId);
            kelas.ClassSubject = subject;

            target.TargetClass = kelas;

            string tovYear = "";
            int tovForm = 0;
            string tovMarkType = "";

            //get previous form
            if (kelas.ClassForm == "2" || kelas.ClassForm == "3" || kelas.ClassForm == "5")
            {
                //get previous exam year
                tovYear = (Convert.ToInt32(exam.ExamYear) - 1).ToString();

                tovForm = Convert.ToInt32(kelas.ClassForm) - 1;

                tovMarkType = "Markah Akhir Tahun";
            }
            else if (kelas.ClassForm == "peralihan" || kelas.ClassForm == "1" || kelas.ClassForm == "4")
            {
                tovYear = exam.ExamYear;
                tovForm = Convert.ToInt32(kelas.ClassForm);
                tovMarkType = "AR2";
            }

            target.TovYear = tovYear;
            target.TovForm = tovForm;
            target.TovMarkType = tovMarkType;

            target.ExamYear = exam.ExamYear;
            target.StudentForm = kelas.ClassForm;

            //get class list related to the subject
            targets = target.GetTargetList(pageNumber);

            

            return Json(new
            {
                result = targets
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult SaveTarget(List<string[]> targetMarks)
        {
            foreach (var targetMark in targetMarks)
            {
                Target target = new Target();

                target.StudentId = Convert.ToInt32(targetMark[0]);
                target.SubjectId = Convert.ToInt32(targetMark[1]);
                target.ExamYear = targetMark[2].ToString();
                target.StudentForm = targetMark[3].ToString();
                target.StudentName = targetMark[4].ToString();
                target.StudentIc = targetMark[5].ToString();
                target.Tov = targetMark[6].ToString();
                target.Etr = targetMark[9].ToString();

                //target.Oti1 = Convert.To

                double tov = 0;
                double etr = 0;
                double oti1 = 0;
                double oti2 = 0;

                try
                {
                    tov = Convert.ToDouble(target.Tov);

                }catch(Exception e)
                {
                    tov = 0;
                }

                try
                {
                    etr = Convert.ToDouble(target.Etr);

                }
                catch (Exception e)
                {
                    etr = 0;
                }

                if(etr >= tov)
                {
                    if(target.StudentForm == "peralihan" || target.StudentForm == "1" || target.StudentForm == "4"){

                        oti1 = tov;

                    }
                    else
                    {
                        oti1 = tov + (etr - tov) / 3;
                    }
                    
                    oti2 = oti1 + (etr - tov) / 3;
                }

                target.Oti1 = Math.Round(oti1, 2).ToString();
                target.Oti2 = Math.Round(oti2, 2).ToString();

                target.SaveTarget();
            }

            return Json(new
            {
                result = "Senarai markah berjaya disimpan"
            },
            JsonRequestBehavior.AllowGet
            );
        }

    }
}