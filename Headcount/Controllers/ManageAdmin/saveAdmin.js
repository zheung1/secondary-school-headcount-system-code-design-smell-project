﻿$(document).ready(function () {
    $('#save').on('click', function () {
        $("#error-box").empty();
        var validForm = true;

        //form validation
        var username = $('#username').val();

        //check password
        var password = $('#password').val();
        var confirmPassword = $('#confirmPassword').val();

        if (password != confirmPassword) {
            validForm = false;
            $("#error-box").removeClass("error-box");
            $("#error-box").append("Kata laluan tidak sepadan <br>");
        }

        //required fields
        var fullName = $("#fullName").val();

        var address = $("#address").val();
        var hp = $("#hp").val();

        //check required fields
        if (fullName == "" || address == "" || hp == "" || username == "" || password == "" || confirmPassword == "") {
            validForm = false;
            $("#error-box").removeClass("error-box");
            $("#error-box").append("Sila periksa semula ruangan yang wajib diisi *<br>");
        }

        //normal fields
        var remarks = $("#remarks").val();
        var email = $("#email").val();
        var homeTel = $("#homeTel").val();
        var officeTel = $("#officeTel").val();
        
        $.post("../../User/CheckUsername", {
            username: username,
        },
        function (data, status) {

            if (parseInt(data.result) == 1) {
                 validForm = false;
                 $("#error-box").removeClass("error-box");
                 $("#error-box").append("Nama pengguna ini telah diambil. ");
            }
            
        if (validForm) {
            $("#error-box").addClass("error-box");

            //alert('valid form');
            $.post("../Save/", {
                adminName: fullName,
                adminAddress: address,
                adminHp: hp,
                adminRemarks: remarks,
                adminEmail: email,
                adminHomeTel: homeTel,
                adminOfficeTel: officeTel,
                username: username,
                password: password
            },
                function (data, status) {
                    alert(data.result);
                    window.location = "../../Admin/Index";
                });
        }   
      });        
   });
});