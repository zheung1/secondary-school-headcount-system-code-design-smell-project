﻿$(document).ready(function () {
    $('#save').on('click', function () {

        $("#error-box").empty();
        var validForm = true;

        var userId = $('#userId').val();

        //form validation
        var username = $('#username').val();

        //check password
        var password = $('#password').val();
        var confirmPassword = $('#confirmPassword').val();

        if (password != confirmPassword) {
            validForm = false;
            $("#error-box").removeClass("error-box");
            $("#error-box").append("Kata laluan tidak sepadan<br>");
        }

        //required fields
        var fullName = $("#fullName").val();

        var address = $("#address").val();
        var hp = $("#hp").val();
        var occupation = $("#occupation").val();

        //alert(occupation);

        //check required fields
        if (fullName == "" || address == "" || hp == "" || occupation == "" || username == "" || password == "" || confirmPassword == "") {
            validForm = false;
            $("#error-box").removeClass("error-box");
            $("#error-box").append("Sila periksa semula ruangan yang wajib diisi *<br>");
        }

        //normal fields
        var remarks = $("#remarks").val();
        var email = $("#email").val();
        var homeTel = $("#homeTel").val();
        var officeTel = $("#officeTel").val();
       
        if (validForm) {
            $("#error-box").addClass("error-box");

            //alert('valid form');
            $.post("../SaveEdit", {
                userId: userId,
                guestName: fullName,
                guestAddress: address,
                guestHp: hp,
                guestOccupation: occupation,
                guestRemarks: remarks,
                guestEmail: email,
                guestHomeTel: homeTel,
                guestOfficeTel: officeTel,
                username: username,
                password: password
            },
                function (data, status) {
                    alert(data.result);
                    window.location = "../../Guest/Index";
                });
        }
       
    });
});