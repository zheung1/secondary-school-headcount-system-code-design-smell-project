﻿using Headcount.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Headcount.Controllers
{
    public class MethodController : Controller
    {
        // GET: Method
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetMethodList()
        {
            Method method = new Method();

            method.Uuid = (string)Session["uuid"];

            int start = Convert.ToInt32(Request["start"]);

            int length = Convert.ToInt32(Request["length"]);

            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];

            string sortDirection = Request["order[0][dir]"];

            List<Method> methods = method.GetMethodList(start, length, searchValue, sortColumnName, sortDirection);

            int filteredCount = method.GetFilteredCount(searchValue);

            int totalCount = method.GetMethodCount();

            return Json(new
            {
                methods,
                recordsTotal = totalCount,
                draw = Request["draw"],
                recordsFiltered = filteredCount
            },
            JsonRequestBehavior.AllowGet
            );

        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult CheckMethod(string name)
        {
            Method method = new Method();

            method.MethodName = name;

            method.Uuid = (string)Session["uuid"];

            bool exist = method.CheckMethod();

            if (!exist)
            {
                return Json(new
                {
                    result = 1
                },
                    JsonRequestBehavior.AllowGet
                );
            }
            else
            {
                return Json(new
                {
                    result = 0
                },
                    JsonRequestBehavior.AllowGet
                );
            }

        }

        public ActionResult Save(string name)
        {
            Method method = new Method();

            method.MethodName = name;

            method.Uuid = (string)Session["uuid"];

            method.SaveMethod();

            return Json(new
            {
                result = "Maklumat teknik pengajaran berjaya disimpan"

            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult Delete(int id)
        {
            Method method = new Method();

            method.DeleteMethod(id);

            return Json(new
            {
                result = "Maklumat teknik pengajaran berjaya dipadam"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public string MethodDropdownList()
        {
            Method method = new Method();

            method.Uuid = (string)Session["uuid"];

            ArrayList dropdownOptions = method.GetMethodDropdownList();

            var json = new JavaScriptSerializer().Serialize(dropdownOptions);
            return json;

        }
    }
}
