﻿$(document).ready(function() {

    var examId = $("#examId").val();
    var subjectId = $("#subjectId").val();
    var classId = $("#classId").val();
    var pageNumber = $("#pageNumber").val();

    var totalRows = 0;
    var pages = 0;

    


    $.ajax('/Result/GetResultCount/', {
        data: {
            'classId': classId,
            'subjectId': subjectId,
            'examId': examId,
        },
        success: function (data, status, xhr) {

            totalRows = data.result;
            pages = Math.ceil(totalRows / 10);

            for (var i = 0; i < pages; i++) {
                $("#pageNavBtn").append("<button type='button' class=\'btn btn-primary page-btn pure-material-button-contained\' style='margin-right: 5px' id='page" + (i + 1) + "'>" + (i + 1) + "</button>");

                //var link = "window.location.href=\'/Result/Index/" + classId + "?subjectId=" + subjectId + "&examId=" + examId + "&page=" + (i + 1) + "\';";

                //document.getElementById("page" + (i + 1)).setAttribute("onclick", link);

                if ((i + 1) == pageNumber) {
                    document.getElementById("page" + (i + 1)).setAttribute("style", "margin-right: 5px; background-color: green");
                } else {
                    document.getElementById("page" + (i + 1)).setAttribute("style", "margin-right: 5px;");
                }

            }

            
        }
    });

    $.ajax('/Result/GetResultList/', {
        data: {
            'classId': classId,
            'subjectId': subjectId,
            'examId': examId,
            'pageNumber': pageNumber

        },
        success: function(data, status, xhr) {

            if (data.result == null) {
                data.result = []
            }

            console.log(data.result);

            var hotElement = document.querySelector('#hot');
            var hotElementContainer = hotElement.parentNode;

            var hotSettings = {
                data: data.result,
                columns: [
                    {
                        data: 'subjectClassId',
                        type: 'text',
                        readOnly: true
                    },
                    {
                        data: 'studentId',
                        type: 'text',
                        readOnly: true
                    },
                    {
                        data: 'studentName',
                        type: 'text',
                        readOnly: true
                    },
                    {
                        data: 'studentIc',
                        type: 'text',
                        readOnly: true
                    },
                    {
                        data: 'marks',
                        type: 'text'
                    }

                ],
                stretchH: 'all',
                autoWrapRow: true,
                rowHeaders: true,
                colHeaders: [
                    'subjectClassId',
                    'studentId',
                    'Nama',
                    'No. IC',
                    'Mark',
                ],
                columnSorting: true,
                hiddenColumns: {
                    columns: [0, 1],
                    indicators: false
                },
                //contextMenu: true,
                manualRowResize: true,
                manualColumnResize: true,
                language: 'en-US',
                outsideClickDeselects: false
            }

            var hot = new Handsontable(hotElement, hotSettings);

            $(".btn.btn-primary.page-btn.pure-material-button-contained").click(function () {
                $("#error-box").empty();
                $("#error-box").addClass("error-box");
                //alert("save");

                var pageNumber = $(this).index();

                console.log(pageNumber);

                console.log(pageNumber);
                var studentMarks = hot.getData();

                var validList = true;
                //console.log(students);
                var invalidRows = [];

                for (var i = 0; i < studentMarks.length; i++) {

                    if (isNaN(studentMarks[i][4])) {
                        if (studentMarks[i][4] == '-') {

                        } else {
                            validList = false;
                            invalidRows.push(i + 1);
                        }
                    } else {
                        if (studentMarks[i][4] < 0 || studentMarks[i][4] > 100) {
                            validList = false;
                            invalidRows.push(i + 1);
                        }
                    }
                }

                if (validList) {

                    //console.log(validStudents);


                    var studentMarkArray = JSON.stringify({
                        'studentMarks': studentMarks
                    });

                    $.ajax({
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: 'POST',
                        url: '/Result/SaveResult/',
                        data: studentMarkArray,
                        success: function (data) {
                            alert(data.result);
                            window.location = "/Result/Index/" + classId + "?subjectId=" + subjectId + "&examId=" + examId + "&page=" + (pageNumber + 1);
                        }
                    })

                } else {

                    var message = "<i class='fas fa-exclamation-triangle'></i>&nbsp; Sila periksa semula baris ";

                    for (var i = 0; i < invalidRows.length; i++) {
                        message += invalidRows[i] + ", ";
                    }

                    message += "Input ruangan markah di baris-baris tersebut mesti betul atau dikosongkan sebelum senarai ini dapat disimpan ke pangkalan data";

                    $("#error-box").removeClass("error-box");
                    $("#error-box").append(message);
                }

            });

            //hot.alter('insert_row', hot.countRows(), 50);
            $("#save").click(function () {
                $("#error-box").empty();
                $("#error-box").addClass("error-box");
                //alert("save");

                var studentMarks = hot.getData();

                var validList = true;
                //console.log(students);
                var invalidRows = [];

                for (var i = 0; i < studentMarks.length; i++) {

                    if (isNaN(studentMarks[i][4])) {
                        if (studentMarks[i][4] == '-') {

                        } else {
                            validList = false;
                            invalidRows.push(i + 1);
                        }
                    } else {
                        if (studentMarks[i][4] < 0 || studentMarks[i][4] > 100) {
                            validList = false;
                            invalidRows.push(i + 1);
                        }
                    }
                }

                if (validList) {

                    var studentMarkArray = JSON.stringify({
                        'studentMarks': studentMarks
                    });
        
                    $.ajax({
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: 'POST',
                        url: '/Result/SaveResult/',
                        data: studentMarkArray,
                        success: function(data) {
                            alert(data.result);
                            window.location = "/Result/Index/" + classId + "?subjectId=" + subjectId + "&examId=" + examId;
                        }
                    })
                    
                } else {

                    var message = "<i class='fas fa-exclamation-triangle'></i>&nbsp; Sila periksa semula baris ";

                    for (var i = 0; i < invalidRows.length; i++) {
                        message += invalidRows[i] + ", ";
                    }

                    message += "Input ruangan markah di baris-baris tersebut mesti betul atau dikosongkan sebelum senarai ini dapat disimpan ke pangkalan data";

                    $("#error-box").removeClass("error-box");
                    $("#error-box").append(message);
                }

            });

        }
    });




});

