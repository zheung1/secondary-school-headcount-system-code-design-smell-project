﻿using Headcount.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
    public class ResultController : Controller
    {
        // GET: Result
        
        public ActionResult Index(int id, int subjectId, int examId, int page = 1)
        {
            //get exam name, mark type
            Exam exam = new Exam();
            exam = exam.GetExam(examId);

            //get subject name
            Subject subject = new Subject();
            subject = subject.GetSubject(subjectId);

            //get class name
            Class kelas = new Class();
            kelas = kelas.GetClass(id);

            ViewBag.exam = exam;
            ViewBag.subject = subject;
            ViewBag.kelas = kelas;
            ViewBag.page = page;

            return View();
        }

        public ActionResult GetResultCount(int classId, int subjectId, int examId)
        {
            Class kelas = new Class();

            kelas = kelas.GetClass(classId);

            Result result = new Result();

            Exam exam = new Exam();
            exam.ExamId = examId;

            Subject subject = new Subject();
            subject.SubjectId = subjectId;
            subject.SubjectExam = exam;

            kelas.ClassId = classId;
            kelas.ClassSubject = subject;

            result.StudentClass = kelas;

            //get class list related to the subject
            int resultCount = result.GetClassStudentCount();

            return Json(new
            {
                result = resultCount
            },
            JsonRequestBehavior.AllowGet
            );
        }


        public ActionResult GetResultList(int classId, int subjectId, int examId, int pageNumber)
        {
            Class kelas = new Class();

            kelas = kelas.GetClass(classId);

            Result result = new Result();

            ArrayList results = new ArrayList();

            Exam exam = new Exam();
            exam.ExamId = examId;

            Subject subject = new Subject();
            subject.SubjectId = subjectId;
            subject.SubjectExam = exam;

            kelas.ClassId = classId;
            kelas.ClassSubject = subject;

            result.StudentClass = kelas;

            //get class list related to the subject
            results = result.GetClassStudentList(pageNumber);

            return Json(new
            {
                result = results
            },
            JsonRequestBehavior.AllowGet
            );
        }


        public ActionResult SaveResult(List<string[]> studentMarks)
        {
            foreach (var studentMark in studentMarks)
            {
                Result result = new Result();

                result.SubjectClassId = Convert.ToInt32(studentMark[0]);
                result.StudentId = Convert.ToInt32(studentMark[1]);
                result.Marks = studentMark[4];

                result.SaveResultList();
            }

            return Json(new
            {
                result = "Senarai markah berjaya disimpan"
            },
            JsonRequestBehavior.AllowGet
            );
        }
    }
}