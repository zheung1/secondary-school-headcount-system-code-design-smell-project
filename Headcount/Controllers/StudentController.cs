﻿using Headcount.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetStudentIndex()
        {
            Student student = new Student();

            student.Uuid = (string)Session["uuid"];

            int start = Convert.ToInt32(Request["start"]);

            int length = Convert.ToInt32(Request["length"]);

            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];

            string sortDirection = Request["order[0][dir]"];

            List<Student> students = student.GetStudentList(start, length, searchValue, sortColumnName, sortDirection);

            int filteredCount = student.GetFilteredCount(searchValue);

            int totalCount = student.GetStudentCount();

            return Json(new
            {
                students,
                recordsTotal = totalCount,
                draw = Request["draw"],
                recordsFiltered = filteredCount
            },
            JsonRequestBehavior.AllowGet
            );

        }

        


        public ActionResult Create()
        {
            return View();
        }

        public ActionResult CheckStudentExistence(string studentIc)
        {
            Student student = new Student();
            
            student.StudentIc = studentIc;

            student.Uuid = (string)Session["uuid"];

            bool exist = student.CheckExistence();

            if (exist)
            {
                return Json(new
                {
                    result = 1
                },
                    JsonRequestBehavior.AllowGet
                );
            }
            else
            {
                return Json(new
                {
                    result = 0
                },
                    JsonRequestBehavior.AllowGet
                );
            }
        }


        public ActionResult Save(string studentName, string studentIc)
        {
            Student student = new Student();

            student.StudentIc = DbConfig.EscapeString(studentIc);
            student.StudentName = DbConfig.EscapeString(studentName);
            student.Uuid = (string)Session["uuid"];

            student.SaveStudent();

            return Json(new
            {
                result = "Maklumat pelajar berjaya disimpan"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult Edit(int id)
        {
            Student student = new Student();

            student = student.GetStudent(id);

            ViewBag.student = student;

            return View();
        }

        public ActionResult SaveEdit(
            int studentId,
            string studentName,
            string studentIc
        )
        {
            Student student = new Student();
            student.StudentId = studentId;
            student.StudentName = DbConfig.EscapeString(studentName);
            student.StudentIc = DbConfig.EscapeString(studentIc);

            student.UpdateStudent();

            return Json(new
            {
                result = "Maklumat pelajar berjaya disunting"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult BulkInsertStudent()
        {
            return View();
        }

        public ActionResult SaveBulkInsert(List<string[]> students)
        {
            foreach (var student in students)
            {
                Student s = new Student();

                s.StudentName = student[0];
                s.StudentIc = student[1];
                s.Uuid = (string)Session["uuid"];

                bool exist = s.CheckExistence();

                if (exist)
                {
                    //continue;
                    s.UpdateStudent();
                }
                else
                {
                    s.SaveStudent();
                }
            }

            return Json(new
            {
                result = "Senarai pelajar berjaya disimpan"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult ClassStudentIndex(int id, int subjectId, int examId) //classId, subjectId, examId
        {
            //get class info
            Class kelas = new Class();

            kelas = kelas.GetClass(id);

            Student student = new Student();

            //ArrayList students = new ArrayList();

            Exam exam = new Exam();
            exam.ExamId = examId;

            Subject subject = new Subject();
            subject.SubjectId = subjectId;
            subject.SubjectExam = exam;

            kelas.ClassId = id;
            kelas.ClassSubject = subject;

            student.StudentClass = kelas;

            student.Uuid = (string)Session["uuid"];
            //get class list related to the subject
            //students = student.GetClassStudentList();

            ViewBag.kelas = kelas;
            //ViewBag.students = students;
            ViewBag.subjectId = subjectId;
            ViewBag.examId = examId;

            return View();
        }

        public ActionResult GetClassStudentIndex(int id, int subjectId, int examId) //id = classId
        {
            Student student = new Student();

            student.Uuid = (string)Session["uuid"];

            int start = Convert.ToInt32(Request["start"]);

            int length = Convert.ToInt32(Request["length"]);

            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];

            string sortDirection = Request["order[0][dir]"];

            List<Student> students = student.GetClassStudentList(start, length, searchValue, sortColumnName, sortDirection, id, subjectId, examId);

            int filteredCount = student.GetFilteredClassStudentCount(searchValue, id, subjectId, examId);

            int totalCount = student.GetClassStudentCount(id, subjectId, examId);

            return Json(new
            {
                students,
                recordsTotal = totalCount,
                draw = Request["draw"],
                recordsFiltered = filteredCount
            },
            JsonRequestBehavior.AllowGet
            );

        }


        public ActionResult GetStudentList()
        {
            ArrayList students = new ArrayList();

            Student student = new Student();

            student.Uuid = (string)Session["uuid"];

            students = student.GetStudentList();

            return Json(new
            {
                result = students
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult SearchStudent(string query)
        {
           
            ArrayList students = new ArrayList();

            Student student = new Student();

            student.Uuid = (string)Session["uuid"];

            students = student.SearchStudent(query);

            return Json(new
            {
                result = students
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult SearchStudentStatistic(string query, int classId, int subjectId, int examId) //classId, subjectId, examId
        {
            //get class info
            Class kelas = new Class();

            kelas = kelas.GetClass(classId);

            Student student = new Student();

            ArrayList students = new ArrayList();

            Exam exam = new Exam();
            exam.ExamId = examId;

            Subject subject = new Subject();
            subject.SubjectId = subjectId;
            subject.SubjectExam = exam;

            kelas.ClassId = classId;
            kelas.ClassSubject = subject;

            student.StudentClass = kelas;

            //student.Uuid = (string)Session["uuid"];

            //get class list related to the subject
            students = student.SearchStudentStatistic(query);

            return Json(new
            {
                result = students
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult AddToClass(int studentId, int subjectId, int examId, int classId)
        {
            Student student = new Student();

            student.StudentId = studentId;

            Class kelas = new Class();

            kelas.ClassId = classId;

            Subject subject = new Subject();

            subject.SubjectId = subjectId;

            Exam exam = new Exam();

            exam.ExamId = examId;
            
            subject.SubjectExam = exam;

            kelas.ClassSubject = subject;

            student.StudentClass = kelas;

            student.Uuid = (string)Session["uuid"];

            student.AddToClass();

            return Json(new
            {
                result = "Pelajar berjaya ditambah ke kelas"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult CheckClassStudentExistence(int studentId, int subjectId, int examId, int classId)
        {
            Student student = new Student();

            student.StudentId = studentId;

            Class kelas = new Class();

            kelas.ClassId = classId;

            Subject subject = new Subject();

            subject.SubjectId = subjectId;

            Exam exam = new Exam();

            exam.ExamId = examId;

            subject.SubjectExam = exam;

            kelas.ClassSubject = subject;

            student.StudentClass = kelas;

            bool exist = student.CheckClassStudentExistence();

            if (exist)
            {
                return Json(new
                {
                    result = 1
                },
                    JsonRequestBehavior.AllowGet
                );
            }
            else
            {
                return Json(new
                {
                    result = 0
                },
                    JsonRequestBehavior.AllowGet
                );
            }
        }

        public ActionResult BulkAddToClass(int id, int subjectId, int examId) //classId, subjectId, examId
        {
            //get class info
            Class kelas = new Class();

            kelas = kelas.GetClass(id);

            //Student student = new Student();

            ArrayList students = new ArrayList();

            //get class list related to the subject
            //students = student.GetClassStudentList(id);

            ViewBag.kelas = kelas;
            //ViewBag.students = students;
            ViewBag.subjectId = subjectId;
            ViewBag.examId = examId;

            return View();
        }

        public ActionResult SaveBulkAddToClass(List<string[]> students, int examId, int subjectId, int classId)
        {
            Class kelas = new Class();

            kelas.ClassId = classId;

            Subject subject = new Subject();

            subject.SubjectId = subjectId;

            Exam exam = new Exam();

            exam.ExamId = examId;

            foreach (var student in students)
            {
                Student s = new Student();
                s.StudentName = student[0];
                s.StudentIc = student[1];

                s.StudentClass = kelas;
                s.StudentClass.ClassSubject = subject;
                s.StudentClass.ClassSubject.SubjectExam = exam;

                //get id of student with student ic

                int studentId = s.GetIdWithIc();
                
                if(studentId == 0)
                {
                    //if id 0 (student not exist), add student first before assign to class

                    int studId = s.SaveStudent();
                    s.StudentId = studId;

                    s.AddToClass();
                }
                else
                {
                    //assign to class if not exist in the class
                    s.StudentId = studentId;
                    
                    bool exist = s.CheckClassStudentExistence();

                    if (!exist)
                    {
                        s.AddToClass();
                    }

                }

            }  

             

            return Json(new
            {
                result = "Pelajar-pelajar berjaya ditambah kelas"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult Delete(int id)
        {
            Student student = new Student();

            student.DeleteStudent(id);
           

            return Json(new
            {
                result = "Maklumat pelajar berjaya dipadam"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult RemoveFromClass(int id, int subjectId, int examId, int studentId)
        {
            Class kelas = new Class();
            kelas.ClassId = id;

            Subject subject = new Subject();
            subject.SubjectId = subjectId;

            Exam exam = new Exam();
            exam.ExamId = examId;

            Student student = new Student();
            student.StudentId = studentId;

            subject.SubjectExam = exam;

            kelas.ClassSubject = subject;
            kelas.ClassStudent = student;

            student.RemoveFromClass(kelas);

            return Json(new
            {
                result = "Kelas berjaya dikeluarkan dari mata pelajaran"
            },
                    JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult GradeAverageList(int id, int form)
        {
            Student student = new Student();

            //ArrayList students = new ArrayList();
            
            //students = student.GetGradeAverageList(id);

            Exam exam = new Exam();

            exam = exam.GetExam(id);

            string studentForm = "";

            if (form == 0)
            {
                studentForm = "Peralihan";
            }
            else
            {
                studentForm = form.ToString();
            }


            ViewBag.exam = exam;
            ViewBag.form = form;
            ViewBag.studentForm = studentForm;

            //ViewBag.students = students;

            return View();
        }

        public ActionResult FormList(int id)
        {
            ViewBag.examId = id;
            
            return View();
        }

        public ActionResult GetGradeAverageIndex(int id, int form) //examId
        {
            Student student = new Student();

            string studentForm = "";
            
            if(form == 0)
            {
                studentForm = "peralihan";
            }
            else
            {
                studentForm = form.ToString();
            }

            student.Uuid = (string)Session["uuid"];

            int start = Convert.ToInt32(Request["start"]);

            int length = Convert.ToInt32(Request["length"]);

            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];

            string sortDirection = Request["order[0][dir]"];

            List<Student> students = student.GetGradeAverageList(start, length, searchValue, sortColumnName, sortDirection, id, studentForm);

            int filteredCount = student.GetFilteredGradeAverageCount(searchValue, id, studentForm);

            int totalCount = student.GetGradeAverageCount(id, studentForm);

            return Json(new
            {
                students,
                recordsTotal = totalCount,
                draw = Request["draw"],
                recordsFiltered = filteredCount
            },
            JsonRequestBehavior.AllowGet
            );

        }

        public ActionResult GetStudentSubjectMarkList(int id, int examId)
        {
            Subject subject = new Subject();

            ArrayList subjects = new ArrayList();

            subjects = subject.GetStudentSubjectMarkList(id, examId);

            return Json(new
            {
                result = subjects
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult GetStudentSubjectTargetList(int id, string examYear)
        {
            Subject subject = new Subject();

            ArrayList subjects = new ArrayList();

            subjects = subject.GetStudentSubjectTargetList(id, examYear);

            return Json(new
            {
                result = subjects
            },
            JsonRequestBehavior.AllowGet
            );
        }



    }

    
}