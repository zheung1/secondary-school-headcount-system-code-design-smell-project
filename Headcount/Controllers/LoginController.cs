﻿using Headcount.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Validate(string username, string password)
        {

            User user = new User();

            user.Username = username;

            user.Password = password;

            string userType = user.ValidateLogin();

            return Json(new
            {
                result = userType
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult StartUserSession(string username, string password, string userType)
        {

            Session["username"] = username;

            Session["password"] = password;

            Session["userType"] = userType;

            //get UDID
            User user = new User();

            user.Username = username;

            string uuid = "";

            uuid = user.GetUuid();

            Session["uuid"] = uuid;

            return Json(new
            {
                result = userType,
                uuid = uuid
            },
            JsonRequestBehavior.AllowGet
            );
        }
    }
}