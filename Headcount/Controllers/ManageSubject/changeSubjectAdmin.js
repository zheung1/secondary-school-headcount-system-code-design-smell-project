﻿$(document).ready(function() {

    $('#save').on('click', function() {

        var subjectId = $("#subjectId").val();
        var examId = $("#examId").val();

        var administratorId = $("#administratorId").select2('data')[0].id;

        var validForm = true;

        if (validForm) {
            $("#error-box").addClass("error-box");

            //alert('valid form');
            $.post("/SubjectAdmin/SaveChangeAdmin/", {
                    subjectId: subjectId,
                    examId: examId,
                    adminId: administratorId,

                },
                function(data, status) {
                    alert(data.result);
                    window.location = "/ExamSubjects/ExamSubjectIndex/" + examId;
                });
        }

    });
});