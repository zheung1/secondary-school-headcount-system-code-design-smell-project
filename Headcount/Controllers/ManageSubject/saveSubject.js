﻿$(document).ready(function () {

   
    $('#save').on('click', function () {

        $("#error-box").empty();
        var validForm = true;

        var code = $('#code').val();
        var name = $('#name').val();
        var type = $('#type').val();
        var level = $('#level').val();
        var remarks = $('#remarks').val();
        
        //form validation
        //check required fields
        if (name == "" || code == "" || type == "" || level == "") {
            validForm = false;
            $("#error-box").removeClass("error-box");
            $("#error-box").append("Sila periksa semula ruangan yang wajib diisi *<br>");
        }

        var checkExistence = {
            subjectName: name,
            subjectCode: code,
        };
        
        $.post("../CheckSubjectExistence", {
            subjectJson: JSON.stringify(checkExistence) 
        },
        function (data, status) {

            if (parseInt(data.result) == 1) {
                 validForm = false;
                 $("#error-box").removeClass("error-box");
                 $("#error-box").append("Kod dan nama mata pelajaran ini telah diguna.");
            }
            
        if (validForm) {
            $("#error-box").addClass("error-box");

            var newSubject = {
                
                subjectCode: code,
                subjectName: name,
                subjectType: type,
                subjectLevel: level,
                subjectRemarks: remarks,
           
            };

            //alert('valid form');
            $.post("/Subject/Save/", {
                subjectJson: JSON.stringify(newSubject)  
            },
                function (data, status) {
                    alert(data.result);
                    window.location = "/Subject/Index/";
                });
        }
        });
         
    });
    
});