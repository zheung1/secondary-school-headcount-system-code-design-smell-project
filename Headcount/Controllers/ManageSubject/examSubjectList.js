﻿$(document).ready(function() {

    var examId = $("#examId").val();

    var table = $('#data-table').DataTable({

        "ajax": {
            "url": "/ExamSubjects/GetExamSubjectIndex/" + examId,
            "type": "POST",
            "datatype": "json",
            "dataSrc": 'subjects',

        },
        "columns": [
            { "data": "SubjectName", "name": "SubjectName" },
            { "data": "SubjectCode", "name": "SubjectCode" },
            { "data": "SubjectType", "name": "SubjectType" },
            { "data": "SubjectLevel", "name": "SubjectLevel" },
            { "data": "SubjectAdmin.AdminName", "name": "SubjectAdmin.AdminName" },
            {
                "mRender": function(data, type, row) {
                    return '<h6><b>Nama Mata Pelajaran</b></h6><h6>' + row.SubjectName + '</h6><h6><b>Kod Mata Pelajaran</b></h6><h6>' + row.SubjectCode + '</h6><b>Jenis</b></h6><h6>' + row.SubjectType + '</h6><h6><b>Peringkat</b></h6><h6>' + row.SubjectLevel + '</h6><h6><b>Penyelia</b></h6><h6>' + row.SubjectAdmin.AdminName + '</h6>';
                }
            },
            {
                "mRender": function(data, type, row) {
                    return '<div class="dropdown"><button class="btn btn-sm btn-secondary pure-material-button-contained" type = "button" style ="width: 23.25px;" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button><div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"><a class="dropdown-item" onclick="window.location.href=\'/SubjectAdmin/ChangeAdmin/' + row.SubjectId + '?examId=' + examId + '\'"><i class="fas fa-pencil-alt"></i><span style="margin-left: 5px;" class="btn-text">Tukar Penyelia</span></a><a class="dropdown-item" onclick="window.location.href=\'/SubjectClass/SubjectClassIndex/' + row.SubjectId + '?examId=' + examId + '\'"><i class="fas fa-clipboard"></i><span style="margin-left: 5px;" class="btn-text">Senarai Kelas</span></a><a class="dropdown-item" onclick="window.location.href=\'/SubjectTarget/SubjectTargetIndex/' + row.SubjectId + '?examYear=' + row.SubjectExam.ExamYear + '&examId=' + examId + '\'"><i class="fas fa-bullseye"></i><span style="margin-left: 5px;" class="btn-text">Sasaran Mata Pelajaran</span></a><a class="dropdown-item" onclick="deleteItem(' + row.SubjectId + ',' + examId + ')"><i class="fas fa-trash"></i><span style="margin-left: 5px;" class="btn-text">Keluarkan Dari Peperiksaan</span></a></div></div>';
                }
            }
        ],
        "columnDefs": [{
                targets: 0,
                className: 'desktop-column',
            },
            {
                targets: 1,
                className: 'desktop-column',
            },
            {
                targets: 2,
                className: 'desktop-column',
            },
            {
                targets: 3,
                className: 'desktop-column',
            },
            {
                targets: 4,
                className: 'desktop-column',
            },
            {
                targets: 5,
                className: 'mobile-column',
            },
        ],
        "serverSide": "true",
        "order": [0, "asc"],

        "language": {
            search: 'Cari',
            searchPlaceholder: 'Nama Mata Pelajaran',
            zeroRecords: "Tiada Rekod",
            oPaginate: {
                "sNext": "seterus",
                "sPrevious": "sebelum"
            },
            "emptyTable": "Tiada data",
            "info": "Paparan dari _START_ hingga _END_ dari _TOTAL_ rekod",
            "infoEmpty": "Paparan 0 hingga 0 dari 0 rekod",
            "infoFiltered": "(Ditapis dari jumlah _MAX_ rekod)",
            "infoThousands": ",",
            "lengthMenu": "Papar _MENU_ rekod",
            "loadingRecords": "Diproses...",
            "processing": "Sedang diproses...",
            "search": "Carian:",
            "zeroRecords": "Tiada padanan rekod yang dijumpai.",
            "paginate": {
                "first": "Pertama",
                "previous": "Sebelum",
                "next": "Seterusnya",
                "last": "Akhir"
            },
            "aria": {
                "sortAscending": ": diaktifkan kepada susunan lajur menaik",
                "sortDescending": ": diaktifkan kepada susunan lajur menurun"
            }

        }

    });


    $('#data-table_filter').append("<div class='dropdown' ><button class='pure-material-button-contained' type='button' id = 'dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Tambah Mata Pelajaran</button><div class='dropdown-menu dropdown-menu-right' style='width: 300px' id='dropMenu' aria-labelledby='dropdownMenuButton'><input type='text' id='search' class='form-control' placeholder = 'taip di sini untuk mencari mata pelajaran' onKeyUp = 'searchSubject()' /><div id='menuOptions'></div><a class='subject-btn dropdown-item' href='/Subject/Index'>Mengurus Mata Pelajaran</a></div></div >");

    //$('#data-table_filter').append("<button onclick='window.location.href=\"/Exam/Create/\"' class='pure-material-button-contained'><i class='fas fa-plus'></i><span style='margin-left: 5px; ' class='btn-text'>Tambah Baru</span></button>");

    $('#dropdownMenuButton').on('click', function() {

        //get exam id

        var examId = $("#examId").val();

        $.post("/Subject/GetSubjectList", {
                id: examId
            },
            function(data, status) {

                $('#menuOptions').empty();

                var subjects = data;

                for (var i = 0; i < subjects.result.length; i++) {

                    $('#menuOptions').append("<a class='dropdown-item' href='/AssignExam/AddToExam/" + subjects.result[i].SubjectId + "?examId=" + examId + "'>" + subjects.result[i].SubjectName + " (" + subjects.result[i].SubjectCode + ")</a>");

                    //alert(subjects.result[i].SubjectName);

                }
            });

    });

});

function deleteItem(subjectId, examId) {
    //alert(id);

    var confirmation = confirm("Adakah anda pasti mengeluarkan mata pelajaran ini dari peperiksaan? Rekod-rekod markah yang terlibat juga akan dipadam");
    if (confirmation) {
        $.post("/ExamSubjects/RemoveFromExam/" + subjectId + "?examId=" + examId, {

            },
            function(data, status) {

                alert(data.result);
                window.location = "/ExamSubjects/ExamSubjectIndex/" + examId;

            });
    }
}