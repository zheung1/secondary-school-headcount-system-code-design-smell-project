﻿$(document).ready(function () {
    $('#save').on('click', function () {

        $("#error-box").empty();
        var validForm = true;

        var subjectId = $('#subjectId').val();
        var code = $('#code').val();
        var name = $('#name').val();
        var type = $('#type').val();
        var level = $('#level').val();
        var remarks = $('#remarks').val();

        //form validation
        //check required fields
        if (name == "" || code == "" || type == "" || level == "") {
            validForm = false;
            $("#error-box").removeClass("error-box");
            $("#error-box").append("Sila periksa semula ruangan yang wajib diisi *<br>");
        }
       
       
        if (validForm) {
           $("#error-box").addClass("error-box");

            var newSubject = {
                subjectId: subjectId,
                subjectCode: code,
                subjectName: name,
                subjectRemarks: remarks,
                subjectType: type,
                subjectLevel: level
            };

               //alert('valid form');
               $.post("/Subject/SaveEdit/", {
                   subjectJson: JSON.stringify(newSubject) 
                
               },
               function (data, status) {
                       alert(data.result);
                       window.location = "/Subject/Index/";
               });
        }


    });

});