﻿$(document).ready(function() {

    $('#save').on('click', function() {

        var subjectId = $("#subjectId").val();
        var examId = $("#examId").val();

        var administratorId = $("#administratorId").select2('data')[0].id;

        var validForm = true;
        var notExisted = true;


        $.post("/ExamSubjects/CheckExamSubjectExistence", {
                subjectId: subjectId,
                examId: examId,
            },
            function(data, status) {

                if (parseInt(data.result) == 1) {
                    validForm = false;
                    notExisted = false;

                    alert("Mata pelajaran ini sudah ada dalam peperiksaan ini");

                    window.location = "/ExamSubjects/ExamSubjectIndex/" + examId;
                }

                //form validation
                //check required fields
                if (administratorId == "" && notExisted) {
                    validForm = false;
                    $("#error-box").removeClass("error-box");
                    $("#error-box").append("Sila periksa semula ruangan yang wajib diisi *<br>");
                }

                if (validForm) {
                    $("#error-box").addClass("error-box");

                    //alert('valid form');
                    $.post("/AssignExam/SaveAddToExam/", {
                            subjectId: subjectId,
                            examId: examId,
                            adminId: administratorId,

                        },
                        function(data, status) {
                            alert(data.result);
                            window.location = "/ExamSubjects/ExamSubjectIndex/" + examId;
                        });
                }
            });
    });
});