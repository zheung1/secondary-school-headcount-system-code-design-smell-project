﻿using Headcount.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
    public class AssignExamController : Controller
    {
        public ActionResult AddToExam(int id, int examId) //subjectId, examId
        {
            ViewBag.subjectId = id;
            ViewBag.examId = examId;

            return View();
        }

        public ActionResult SaveAddToExam(int subjectId, int examId, int adminId)
        {

            ExamSubject examSubject = new ExamSubject();
            examSubject.SubjectId = subjectId;

            Exam exam = new Exam();
            exam.ExamId = examId;

            examSubject.SubjectExam = exam;

            Admin admin = new Admin();
            admin.AdminId = adminId;

            examSubject.SubjectAdmin = admin;

            examSubject.Uuid = (string)Session["uuid"];

            examSubject.AddToExam();

            return Json(new
            {
                result = "Mata Pelajaran berjaya ditambah ke peperiksaan"
            },
            JsonRequestBehavior.AllowGet
            );
        }


    }
}