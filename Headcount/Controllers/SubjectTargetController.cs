﻿using Headcount.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
    public class SubjectTargetController : Controller
    {
        public ActionResult SubjectTargetIndex(int id, string examYear, int examId)
        {
            Subject subject = new Subject();

            subject = subject.GetSubject(id);

            string[] lowerForms = new string[] { "peralihan", "1", "2", "3" };
            string[] higherForms = new string[] { "4", "5" };

            ViewBag.lowerForms = lowerForms;
            ViewBag.higherForms = higherForms;
            ViewBag.subjectLevel = subject.SubjectLevel;

            ViewBag.subjectId = id;
            ViewBag.examYear = examYear;
            ViewBag.examId = examId;

            return View();
        }

        public ActionResult LowerSecondaryTarget(int id, int examId, string form)
        {
            Subject subject = new Subject();

            subject = subject.GetSubject(id);

            Exam exam = new Exam();
            exam = exam.GetExam(examId);

            string examYear = exam.ExamYear;

            int[,] gradeCounts = new int[7, 7] {
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                };

            double[,] gradePercentage = new double[7, 7] {
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                };

            double[] gpmp = new double[7] { 0, 0, 0, 0, 0, 0, 0 };

            int[] total = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

            int[] passes = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

            int[] fails = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

            double[] passPercent = new double[7] { 0, 0, 0, 0, 0, 0, 0 };

            double[] failPercent = new double[7] { 0, 0, 0, 0, 0, 0, 0 };

            string[] grades = { "A", "B", "C",
                                    "D", "E", "F",
                                    "TH"};

            SubjectTarget subjectTarget = new SubjectTarget();

            gradeCounts = subjectTarget.GetGradeCounts(id, examYear, form, "Menengah Bawah");

            gradePercentage = subjectTarget.GetGradePercentage(gradeCounts, "Menengah Bawah");

            gpmp = subjectTarget.GetGpmp(gradeCounts, "Menengah Bawah");

            total = subjectTarget.GetTotalStudent(gradeCounts);

            passes = subjectTarget.GetPassNumber(total, gradeCounts, "Menengah Bawah");

            fails = subjectTarget.GetFailNumber(gradeCounts, "Menengah Bawah");

            passPercent = subjectTarget.GetPassPercent(passes, fails);

            failPercent = subjectTarget.GetFailPercent(passes, fails);

            ViewBag.grades = grades;

            ViewBag.gradeCounts = gradeCounts;

            ViewBag.gradePercentage = gradePercentage;

            ViewBag.gpmp = gpmp;

            ViewBag.total = total;

            ViewBag.passes = passes;

            ViewBag.fails = fails;

            ViewBag.passPercent = passPercent;

            ViewBag.failPercent = failPercent;

            ViewBag.subject = subject;

            ViewBag.examYear = examYear;

            ViewBag.examId = examId;

            string[] columns = { "TOV", "OTI1", "AR1", "OTI2", "AR2", "ETR", "Akhir Tahun" };

            ViewBag.columns = columns;

            return View();
        }

        public ActionResult UpperSecondaryTarget(int id, int examId, string form)
        {
            Subject subject = new Subject();

            subject = subject.GetSubject(id);

            Exam exam = new Exam();
            exam = exam.GetExam(examId);

            string examYear = exam.ExamYear;

            int[,] gradeCounts = new int[7, 11] {
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                };

            double[,] gradePercentage = new double[7, 11] {
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                };

            double[] gpmp = new double[7] { 0, 0, 0, 0, 0, 0, 0 };

            int[] total = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

            int[] passes = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

            int[] fails = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

            double[] passPercent = new double[7] { 0, 0, 0, 0, 0, 0, 0 };

            double[] failPercent = new double[7] { 0, 0, 0, 0, 0, 0, 0 };

            string[] grades = { "A+", "A", "A-",
                                    "B+", "B", "C+",
                                    "C", "D",
                                    "E", "G", "TH"};

            SubjectTarget subjectTarget = new SubjectTarget();
            
            gradeCounts = subjectTarget.GetGradeCounts(id, examYear, form, "Menengah Atas");

            //gradePercentage = subjectTarget.GetSpmGradePercentage(gradeCounts);

            gradePercentage = subjectTarget.GetGradePercentage(gradeCounts, "Menengah Atas");
            
            gpmp = subjectTarget.GetGpmp(gradeCounts, "Menengah Atas");

            total = subjectTarget.GetTotalStudent(gradeCounts);

            passes = subjectTarget.GetPassNumber(total, gradeCounts, "Menengah Atas");

            fails = subjectTarget.GetFailNumber(gradeCounts, "Menengah Atas");

            passPercent = subjectTarget.GetPassPercent(passes, fails);

            failPercent = subjectTarget.GetFailPercent(passes, fails);

            ViewBag.grades = grades;

            ViewBag.gradeCounts = gradeCounts;

            ViewBag.gradePercentage = gradePercentage;

            ViewBag.gpmp = gpmp;

            ViewBag.total = total;

            ViewBag.passes = passes;

            ViewBag.fails = fails;

            ViewBag.passPercent = passPercent;

            ViewBag.failPercent = failPercent;

            ViewBag.subject = subject;

            ViewBag.examYear = examYear;

            ViewBag.examId = examId;

            string[] columns = { "TOV", "OTI1", "AR1", "OTI2", "AR2", "ETR", "Akhir Tahun" };

            ViewBag.columns = columns;

            return View();
        }

    }
}