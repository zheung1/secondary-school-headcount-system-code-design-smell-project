﻿$(document).ready(function () {

    var examId = $("#examId").val();
    var subjectId = $("#subjectId").val();
    var classId = $("#classId").val();
    var pageNumber = $("#pageNumber").val();


    var totalRows = 0;
    var pages = 0;




    $.ajax('/Target/GetTargetCount/', {
        data: {
            'classId': classId,
            'subjectId': subjectId,
            'examId': examId,
        },
        success: function (data, status, xhr) {

            totalRows = data.result;
            pages = Math.ceil(totalRows / 10);


            for (var i = 0; i < pages; i++) {
                $("#pageNavBtn").append("<button type='button' class=\'btn btn-primary page-btn pure-material-button-contained\' style='margin-right: 5px' id='page" + (i + 1) + "'>" + (i + 1) + "</button>");

                //var link = "window.location.href=\'/Target/Index/" + classId + "?subjectId=" + subjectId + "&examId=" + examId + "&page=" + (i + 1) + "\'";
                //var link = "#";

                //document.getElementById("page" + (i + 1)).setAttribute("onclick", navigateToPage(i + 1));

                if ((i + 1) == pageNumber) {
                    document.getElementById("page" + (i + 1)).setAttribute("style", "margin-right: 5px; background-color: green");
                } else {
                    document.getElementById("page" + (i + 1)).setAttribute("style", "margin-right: 5px;");
                }

            }


        }
    });

    $.ajax('/Target/GetTargetList/', {
        data: {
            'classId': classId,
            'subjectId': subjectId,
            'examId': examId,
            'pageNumber': pageNumber

        },
        success: function (data, status, xhr) {

            if (data.result == null) {
                data.result = []
            }

            console.log(data.result);

            var hotElement = document.querySelector('#hot');
            var hotElementContainer = hotElement.parentNode;

            var hotSettings = {
                data: data.result,
                columns: [
                    {
                        data: 'StudentId',
                        type: 'text',
                        readOnly: true
                    },
                    {
                        data: 'SubjectId',
                        type: 'text',
                        readOnly: true
                    },
                    {
                        data: 'ExamYear',
                        type: 'text',
                        readOnly: true
                    },
                    {
                        data: 'StudentForm',
                        type: 'text',
                        readOnly: true
                    },
                    {
                        data: 'StudentName',
                        type: 'text',
                        readOnly: true
                    },
                    {
                        data: 'StudentIc',
                        type: 'text',
                        readOnly: true
                    },
                    {
                        data: 'Tov',
                        type: 'text'
                    },
                    {
                        data: 'Oti1',
                        type: 'text'
                    },
                    {
                        data: 'Oti2',
                        type: 'text'
                    },
                    {
                        data: 'Etr',
                        type: 'text'
                    }

                ],
                stretchH: 'all',
                autoWrapRow: true,
                rowHeaders: true,
                colHeaders: [
                    'studentId',
                    'subjectId',
                    'examYear',
                    'studentForm',
                    'Nama',
                    'No. IC',
                    'TOV',
                    'OTI1',
                    'OTI2',
                    'ETR'
                ],
                columnSorting: true,
                hiddenColumns: {
                    columns: [0, 1, 2, 3],
                    indicators: false
                },
                //contextMenu: true,
                manualRowResize: true,
                manualColumnResize: true,
                language: 'en-US',
                outsideClickDeselects: false
            }

            var hot = new Handsontable(hotElement, hotSettings);

            $(".btn.btn-primary.page-btn.pure-material-button-contained").click(function () {
                $("#error-box").empty();
                $("#error-box").addClass("error-box");

                var pageNumber = $(this).index();
                console.log(pageNumber);
                var targetMarks = hot.getData();

                var validList = true;
                //console.log(students);
                var invalidRows = [];

                for (var i = 0; i < targetMarks.length; i++) {

                    if (targetMarks[i][6] == "") {

                        //alert("emp" + targetMarks[i][7]);
                        targetMarks[i][7] = "";
                        targetMarks[i][8] = "";
                        targetMarks[i][9] = "";

                    }

                    if (isNaN(targetMarks[i][6])) {
                        if (targetMarks[i][6] == '-') {

                        } else {
                            validList = false;
                            invalidRows.push(i + 1);
                        }
                    } else {
                        if (targetMarks[i][6] < 0 || targetMarks[i][6] > 100) {
                            validList = false;
                            invalidRows.push(i + 1);
                        }
                    }

                    if (isNaN(targetMarks[i][9])) {
                        if (targetMarks[i][9] == '-') {

                        } else {
                            validList = false;
                            invalidRows.push(i + 1);
                        }
                    } else {
                        if (targetMarks[i][9] < 0 || targetMarks[i][9] > 100) {
                            validList = false;
                            invalidRows.push(i + 1);
                        }
                    }
                }

                if (validList) {

                    //console.log(validStudents);

                    var studentTargetArray = JSON.stringify({
                        'targetMarks': targetMarks
                    });

                    $.ajax({
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: 'POST',
                        url: '/Target/SaveTarget/',
                        data: studentTargetArray,
                        success: function (data) {
                            console.log(data.result);
                            window.location = "/Target/Index/" + classId + "?subjectId=" + subjectId + "&examId=" + examId + "&page=" + (pageNumber + 1);
                        }
                    })

                } else {

                    var message = "<i class='fas fa-exclamation-triangle'></i>&nbsp; Sila periksa semula baris ";

                    for (var i = 0; i < invalidRows.length; i++) {
                        message += invalidRows[i] + ", ";
                    }

                    message += "Input ruangan markah di baris-baris tersebut mesti betul atau dikosongkan sebelum senarai ini dapat disimpan ke pangkalan data";

                    $("#error-box").removeClass("error-box");
                    $("#error-box").append(message);
                }

            });

            //hot.alter('insert_row', hot.countRows(), 50);
            $("#save").click(function () {
                $("#error-box").empty();
                $("#error-box").addClass("error-box");
                //alert("save");

                var targetMarks = hot.getData();

                var validList = true;
                //console.log(students);
                var invalidRows = [];

                for (var i = 0; i < targetMarks.length; i++) {

                    if (targetMarks[i][6] == "") {

                        //alert("emp" + targetMarks[i][7]);
                        targetMarks[i][7] = "";
                        targetMarks[i][8] = "";
                        targetMarks[i][9] = "";

                    }

                    if (isNaN(targetMarks[i][6])) {
                        if (targetMarks[i][6] == '-') {

                        } else {
                            validList = false;
                            invalidRows.push(i + 1);
                        }
                    } else {
                        if (targetMarks[i][6] < 0 || targetMarks[i][6] > 100) {
                            validList = false;
                            invalidRows.push(i + 1);
                        }
                    }

                    if (isNaN(targetMarks[i][9])) {
                        if (targetMarks[i][9] == '-') {

                        } else {
                            validList = false;
                            invalidRows.push(i + 1);
                        }
                    } else {
                        if (targetMarks[i][9] < 0 || targetMarks[i][9] > 100) {
                            validList = false;
                            invalidRows.push(i + 1);
                        }
                    }
                }

                if (validList) {

                    //console.log(validStudents);

                    var studentTargetArray = JSON.stringify({
                        'targetMarks': targetMarks
                    });

                    $.ajax({
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: 'POST',
                        url: '/Target/SaveTarget/',
                        data: studentTargetArray,
                        success: function (data) {
                            alert(data.result);
                            window.location = "/Target/Index/" + classId + "?subjectId=" + subjectId + "&examId=" + examId;
                        }
                    })

                } else {

                    var message = "<i class='fas fa-exclamation-triangle'></i>&nbsp; Sila periksa semula baris ";

                    for (var i = 0; i < invalidRows.length; i++) {
                        message += invalidRows[i] + ", ";
                    }

                    message += "Input ruangan markah di baris-baris tersebut mesti betul atau dikosongkan sebelum senarai ini dapat disimpan ke pangkalan data";

                    $("#error-box").removeClass("error-box");
                    $("#error-box").append(message);
                }

            });


            


        }
    });

    


});



