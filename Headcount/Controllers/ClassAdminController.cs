﻿using Headcount.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
    public class ClassAdminController : Controller
    {
        
        public ActionResult ChangeAdmin(int id, int subjectId, int examId) //classId, subjectId
        {
            SubjectClass kelas = new SubjectClass();

            kelas = kelas.GetSubjectClass(id, subjectId, examId);

            Subject subject = new Subject();

            subject = subject.GetSubject(subjectId);

            ViewBag.subject = subject;

            ViewBag.exam = subject.GetRelatedExam(examId); ;

            ViewBag.kelas = kelas;

            return View();

        }

        public ActionResult SaveChangeAdmin(int subjectClassId, int adminId, int teacherId, int methodId, double classHour)
        {
            SubjectClass kelas = new SubjectClass();

            Teacher teacher = new Teacher();

            teacher.TeacherId = teacherId;

            Admin admin = new Admin();

            admin.AdminId = adminId;

            kelas.ClassHour = classHour;

            Method method = new Method();

            method.MethodId = methodId;

            Subject subject = new Subject();

            subject.SubjectId = subjectClassId;

            kelas.ClassTeacher = teacher;

            kelas.ClassAdmin = admin;

            kelas.ClassMethod = method;

            kelas.ClassSubject = subject;

            kelas.SaveChangeAdmin();

            return Json(new
            {
                result = "Penyelia mata pelajaran berjaya ditukar"
            },
            JsonRequestBehavior.AllowGet
            );

        }
    }

    
}