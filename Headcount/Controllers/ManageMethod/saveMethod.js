﻿$(document).ready(function () {
    $('#save').on('click', function () {
        $("#error-box").empty();
        var validForm = true;

        //form validation
        var name = $('#name').val();

        
        //check required fields
        if (name == "") {
            validForm = false;
            $("#error-box").removeClass("error-box");
            $("#error-box").append("Sila periksa semula ruangan yang wajib diisi *<br>");
        }

        
        $.post("/Method/CheckMethod", {
            name: name,
        },
        function (data, status) {

            if (parseInt(data.result) == 1) {
                 validForm = false;
                 $("#error-box").removeClass("error-box");
                 $("#error-box").append("Teknik pengajaran ini telah wujud dalam sistem.");
            }
            
        if (validForm) {
            $("#error-box").addClass("error-box");

            //alert('valid form');
            $.post("/Method/Save/", {
                name: name
            },
                function (data, status) {
                    alert(data.result);
                    window.location = "/Method/Index";
                });
        }   
      });        
   });
});