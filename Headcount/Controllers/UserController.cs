﻿using Headcount.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
    public class UserController : Controller
    {
        public ActionResult CheckUsername(string username)
        {

            string sql = "SELECT * FROM Users where username = '" + DbConfig.EscapeString(username) + "'";

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                return Json(new
                {
                    result = 1
                },
                    JsonRequestBehavior.AllowGet
                );
            }
            else
            {
                return Json(new
                {
                    result = 0
                },
                    JsonRequestBehavior.AllowGet
                );
            }


        }
    }
}