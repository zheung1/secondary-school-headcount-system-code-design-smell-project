﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Headcount.Controllers
{
    public class DbConfig
    {
        public static string connectionString = "Data Source =.; Initial Catalog = headcountsys; Integrated Security = True";

        public static String EscapeString(string value)
        {
            return value.Replace(@"'", @"''");
        }

        public static int InsertData(string sql)
        {
            int id = 0;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sql, connection);
                id = Convert.ToInt32(command.ExecuteScalar());
            }

            return id;

        }

        public static int UpdateData(string sql)
        {

            int id = 0;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sql, connection);
                id = Convert.ToInt32(command.ExecuteScalar());
            }

            return id;

        }

        public static int DeleteData(string sql)
        {

            int id = 0;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sql, connection);
                id = Convert.ToInt32(command.ExecuteScalar());
            }

            return id;

        }

        public static ArrayList Retrieve(string sql)
        {
            ArrayList rows = new ArrayList();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sql, connection);
                

                var reader = command.ExecuteReader();
                if (!reader.HasRows)
                {
                    return null;
                }
                else
                {
                    while (reader.Read())
                    {

                        IDictionary<string, object> cols = new Dictionary<string, object>();

                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            
                            cols[reader.GetName(i)] = reader[i];

                        }

                        rows.Add(cols);
                    }
                }

                return rows;
            }
            
        }
    }
}
