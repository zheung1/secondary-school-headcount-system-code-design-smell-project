﻿using Headcount.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult HomePage()
        {
            Announcement announcement = new Announcement();

            ArrayList announcements = new ArrayList();

            announcement.Uuid = (string)Session["uuid"];

            announcements = announcement.GetAnnouncementList(true);

            ViewBag.announcements = announcements;

            return View();
        }

        

    }
}