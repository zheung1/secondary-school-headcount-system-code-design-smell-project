﻿using Headcount.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
    public class SubjectAdminController : Controller
    {
        public ActionResult ChangeAdmin(int id, int examId) //sdubjectId, examId
        {
            Subject subject = new Subject();
            
            ExamSubject examSubject = new ExamSubject();

            subject = examSubject.GetSubject(id);

            //get subject admin
            Admin admin = examSubject.GetSubjectAdmin(id, examId);

            Exam exam = new Exam();
            exam = exam.GetExam(examId);

            ViewBag.subject = subject;
            ViewBag.exam = exam;
            ViewBag.admin = admin;


            return View();
        }

        public ActionResult SaveChangeAdmin(int subjectId, int examId, int adminId)
        {
            ExamSubject examSubject = new ExamSubject();
            examSubject.SubjectId = subjectId;

            Exam exam = new Exam();
            exam.ExamId = examId;

            Admin admin = new Admin();
            admin.AdminId = adminId;

            examSubject.SubjectExam = exam;
            examSubject.SubjectAdmin = admin;

            examSubject.SaveChangeAdmin();

            return Json(new
            {
                result = "Penyelia mata pelajaran berjaya ditukar"
            },
            JsonRequestBehavior.AllowGet
            );
        }
    }


}