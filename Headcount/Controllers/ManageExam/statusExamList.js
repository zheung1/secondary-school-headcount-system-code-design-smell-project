﻿$(document).ready(function () {



    var table = $('#data-table').DataTable({

        "ajax": {
            "url": "/Exam/GetExamList",
            "type": "POST",
            "datatype": "json",
            "dataSrc": 'exams',

        },
        "serverSide": "true",
        "columns": [
            { "data": "ExamName", "name": "ExamName" },
            { "data": "ExamMarkType", "name": "ExamMarkType" },
            { "data": "ExamYear", "name": "ExamYear" },
            { "data": "ExamLevel", "name": "ExamLevel" },
            { "data": "ExamAdmin.AdminName", "name": "AdminName" },
            {
                "mRender": function (data, type, row) {
                    return '<h6><b>Nama Peperiksaan</b></h6><h6>' + row.ExamName + '</h6><h6><b>Jenis Markah</b></h6><h6>' + row.ExamMarkType + '</h6><b>Tahun Peperiksaan</b></h6><h6>' + row.ExamYear + '</h6><h6><b>Peringkat</b></h6><h6>' + row.ExamLevel + '</h6><h6><b>Penyelia</b></h6><h6>' + row.AdminName + '</h6>';
                }
            },
            {
                "mRender": function (data, type, row) {
                    return '<div class="dropdown"><button class="btn btn-sm btn-secondary btn-drop pure-material-button-contained" type = "button" id = "dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><i class="fas fa-ellipsis-v"></i></button><div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"><a class="dropdown-item" onclick="window.location.href=\'/Status/Show/' + row.ExamId + '\'"><i class="fas fa-eye"></i><span style="margin-left: 5px;" class="btn-text">Lihat Status Penyelesaian</span></a></div></div>';
                }
            }
        ],
        "columnDefs": [
            {
                targets: 0,
                className: 'desktop-column',
            },
            {
                targets: 1,
                className: 'desktop-column',
            },
            {
                targets: 2,
                className: 'desktop-column',
            },
            {
                targets: 3,
                className: 'desktop-column',
            },
            {
                targets: 4,
                className: 'desktop-column',
            },
            {
                targets: 5,
                className: 'mobile-column',
            },
        ],

        "order": [0, "asc"],

        "language": {
            search: 'Cari',
            searchPlaceholder: 'Nama Peperiksaan',
            zeroRecords: "Tiada Rekod",
            oPaginate: {
                "sNext": "seterus",
                "sPrevious": "sebelum"
            },
            "emptyTable": "Tiada data",
            "info": "Paparan dari _START_ hingga _END_ dari _TOTAL_ rekod",
            "infoEmpty": "Paparan 0 hingga 0 dari 0 rekod",
            "infoFiltered": "(Ditapis dari jumlah _MAX_ rekod)",
            "infoThousands": ",",
            "lengthMenu": "Papar _MENU_ rekod",
            "loadingRecords": "Diproses...",
            "processing": "Sedang diproses...",
            "search": "Carian:",
            "zeroRecords": "Tiada padanan rekod yang dijumpai.",
            "paginate": {
                "first": "Pertama",
                "previous": "Sebelum",
                "next": "Seterusnya",
                "last": "Akhir"
            },
            "aria": {
                "sortAscending": ": diaktifkan kepada susunan lajur menaik",
                "sortDescending": ": diaktifkan kepada susunan lajur menurun"
            }

        }

    });
});


function deleteItem(examid) {
    //alert(id);
    var confirmation = confirm("Adakah anda pasti memadam rekod peperiksaan ini? Markah dan sasaran yang terlibat juga akan dipadam");
    if (confirmation) {
        $.post("/Exam/Delete/" + examid, {

        },
            function (data, status) {
                alert(data.result);
                window.location = "/Exam/Index";
            });
    }
}