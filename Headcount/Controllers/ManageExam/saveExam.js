﻿$(document).ready(function() {
    $('#save').on('click', function() {
        $("#error-box").empty();
        var validForm = true;

        //form validation

        //required fields
        var name = $('#name').val();
        var startDate = $("#startDate").val();
        var endDate = $("#endDate").val();
        var type = $("#type").val();
        var year = $("#year").val();
        var markType = $("#markType").val();
        var administratorId = $("#administratorId").select2('data')[0].id;


        //check required fields
        if (name == "" || startDate == "" || endDate == "" || type == "" || administratorId == "" || year == "" || markType == "") {
            validForm = false;
            $("#error-box").removeClass("error-box");
            $("#error-box").append("Sila periksa semula ruangan yang wajib diisi *<br>");
        }


        if (isNaN(parseInt(year))) {
            validForm = false;
            $("#error-box").removeClass("error-box");
            $("#error-box").append("Sila periksa semula ruangan tahun peperiksaan*<br>");
        } else {

            if (parseInt(year) < 1970 || parseInt(year) > 2099) {
                validForm = false;
                $("#error-box").removeClass("error-box");
                $("#error-box").append("Sila periksa semula ruangan tahun peperiksaan*<br>");
            } else {

            }
        }


        //normal fields
        var remarks = $("#remarks").val();

        $.post("/Exam/CheckExamExistence/", {
                examLevel: type,
                examYear: year,
                examMarkType: markType
            },
            function(data, status) {
                if (parseInt(data.result) == 1) {
                    validForm = false;
                    $("#error-box").removeClass("error-box");
                    $("#error-box").append(markType + " " + year + " " + type + " sudah ada dalam sistem");
                }

                if (validForm) {
                    $("#error-box").addClass("error-box");

                    //alert('valid form');
                    $.post("../Save/", {
                            examName: name,
                            examStartDate: startDate,
                            examEndDate: endDate,
                            examLevel: type,
                            examRemarks: remarks,
                            adminId: administratorId,
                            examYear: year,
                            examMarkType: markType


                        },
                        function(data, status) {
                            alert(data.result);
                            window.location = "../../Exam/Index";
                        });

                }


            }
        );



    });
});