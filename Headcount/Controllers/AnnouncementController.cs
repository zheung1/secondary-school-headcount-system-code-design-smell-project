﻿using Headcount.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
    public class AnnouncementController : Controller
    {
        // GET: Announcement
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAnnouncementList()
        {
            Announcement announcement = new Announcement();

            announcement.Uuid = (string)Session["uuid"];

            int start = Convert.ToInt32(Request["start"]);

            int length = Convert.ToInt32(Request["length"]);

            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];

            string sortDirection = Request["order[0][dir]"];

            List<Announcement> announcements = announcement.GetAnnouncementList(start, length, searchValue, sortColumnName, sortDirection);

            int filteredCount = announcement.GetFilteredCount(searchValue);

            int totalCount = announcement.GetAnnouncementCount();

            return Json(new
            {
                announcements,
                recordsTotal = totalCount,
                draw = Request["draw"],
                recordsFiltered = filteredCount
            },
            JsonRequestBehavior.AllowGet
            );

        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {
            Announcement announcement = new Announcement();

            announcement = announcement.GetAnnouncement(id);

            ViewBag.announcement = announcement;

            return View();
        }

        public ActionResult Save(string title, string body)
        {
            Announcement announcement = new Announcement();

            announcement.Title = DbConfig.EscapeString(title);

            announcement.Body = DbConfig.EscapeString(body);

            announcement.Uuid = (string)Session["uuid"];

            announcement.SaveAnnouncement();

            return Json(new
            {
                result = "Pengumunan berjaya disimpan"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult SaveEdit(int announcementId, string title, string body)    
        {
            Announcement announcement = new Announcement();

            announcement.AnnouncementId = announcementId;

            announcement.Title = DbConfig.EscapeString(title);

            announcement.Body = DbConfig.EscapeString(body);


            announcement.UpdateAnnouncement();

            return Json(new
            {
                result = "Pengumunan berjaya disunting"
            },
            JsonRequestBehavior.AllowGet
            );

        }

        public ActionResult Delete(int id)
        {
            Announcement announcement = new Announcement();

            announcement.DeleteAnnouncement(id);


            return Json(new
            {
                result = "Pengumunan berjaya dipadam"
            },
            JsonRequestBehavior.AllowGet
            );
        }


    }
}