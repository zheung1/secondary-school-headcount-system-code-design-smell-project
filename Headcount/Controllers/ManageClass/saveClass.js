﻿$(document).ready(function () {

   
    $('#save').on('click', function () {

        $("#error-box").empty();
        var validForm = true;

        var name = $('#name').val();
        var type = $('#type').val();
        var form = $('#form').val();
        var remarks = $('#remarks').val();
        
        //form validation
        //check required fields
        if (name == "" || form == "" || type == "") {
            validForm = false;
            $("#error-box").removeClass("error-box");
            $("#error-box").append("Sila periksa semula ruangan yang wajib diisi *<br>");
        }
        
        $.post("/Class/CheckClassExistence", {
            className: name,
            
        },
        function (data, status) {

            if (parseInt(data.result) == 1) {
                 validForm = false;
                 $("#error-box").removeClass("error-box");
                 $("#error-box").append("Nama kelas ini telah diguna.");
            }
            
        if (validForm) {
            $("#error-box").addClass("error-box");

            //alert('valid form');
            $.post("/Class/Save/", {
                className: name,
                classType: type,
                classForm: form,
                classRemarks: remarks,
            
            },
                function (data, status) {
                    alert(data.result);
                    window.location = "/Class/Index/";
                });
        }
        });
         
    });
    
});