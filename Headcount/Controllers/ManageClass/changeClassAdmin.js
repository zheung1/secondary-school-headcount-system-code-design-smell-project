﻿$(document).ready(function() {

    $('#save').on('click', function() {

        var subjectClassId = $("#subjectClassId").val();
        var examId = $("#examId").val();
        var subjectId = $("#subjectId").val();

        var administratorId = $("#administratorId").select2('data')[0].id;
        var teacherId = $("#teacherId").select2('data')[0].id;
        var methodId = $("#methodId").select2('data')[0].id;
        var classHour = $("#classHour").val();

        var validForm = true;

        if (!methodId) {
            methodId = 0;
        }

        if (classHour == "" || classHour == null) {
            classHour = 0;
        }

        if (validForm) {
            $("#error-box").addClass("error-box");

            //alert('valid form');
            $.post("/ClassAdmin/SaveChangeAdmin/", {
                    subjectClassId: subjectClassId,
                    adminId: administratorId,
                    teacherId: teacherId,
                    methodId: methodId,
                    classHour: classHour

                },
                function(data, status) {
                    alert(data.result);
                    window.location = "/SubjectClass/SubjectClassIndex/" + subjectId + "?examId=" + examId;
                    //window.history.back();

                    //window.location.replace("/Class/SubjectClassIndex/" + subjectId + "?examId=" + examId + "&pop=false");
                });
        }

    });
});