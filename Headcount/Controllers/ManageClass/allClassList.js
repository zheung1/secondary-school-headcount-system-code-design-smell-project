﻿$(document).ready(function () {

    var table = $('#data-table').DataTable({

        "ajax": {
            "url": "/Class/GetClassIndex",
            "type": "POST",
            "datatype": "json",
            "dataSrc": 'classes',

        },
        "columns": [
            { "data": "ClassName", "name": "ClassName" },
            { "data": "ClassForm", "name": "ClassForm" },
            { "data": "ClassType", "name": "ClassType" },
            {
                "mRender": function (data, type, row) {
                    return '<h6><b>Nama Kelas</b></h6><h6>' + row.ClassName + '</h6><h6><b>Tingkatan</b></h6><h6>' + row.ClassForm + '</h6><h6><b>Jenis</b></h6><h6>' + row.ClassType + '</h6>';
                }
            },
            {
                "mRender": function (data, type, row) {
                    return '<div class="dropdown" ><button class="btn btn-sm btn-secondary pure-material-button-contained" type="button" style="width: 23.25px;" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><i class="fas fa-ellipsis-v"></i></button ><div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"><a class="dropdown-item" onclick="window.location.href=\'/Class/Edit/' + row.ClassId + '\'"><i class="fas fa-pencil-alt"></i><span style="margin-left: 5px;" class="btn-text">Sunting</span></a><a class="dropdown-item" onclick="deleteItem(' + row.ClassId + ')"><i class="fas fa-trash"></i><span style="margin-left: 5px;" class="btn-text">Padam</span></a></div></div>';
                }
            }
        ],
        "columnDefs": [
            {
                targets: 0,
                className: 'desktop-column',
            },
            {
                targets: 1,
                className: 'desktop-column',
            },
            {
                targets: 2,
                className: 'desktop-column',
            },
            {
                targets: 3,
                className: 'mobile-column',
            },
        ],
        "serverSide": "true",
        "order": [0, "asc"],

        "language": {
            search: 'Cari',
            searchPlaceholder: 'Nama Kelas',
            zeroRecords: "Tiada Rekod",
            oPaginate: {
                "sNext": "seterus",
                "sPrevious": "sebelum"
            },
            "emptyTable": "Tiada data",
            "info": "Paparan dari _START_ hingga _END_ dari _TOTAL_ rekod",
            "infoEmpty": "Paparan 0 hingga 0 dari 0 rekod",
            "infoFiltered": "(Ditapis dari jumlah _MAX_ rekod)",
            "infoThousands": ",",
            "lengthMenu": "Papar _MENU_ rekod",
            "loadingRecords": "Diproses...",
            "processing": "Sedang diproses...",
            "search": "Carian:",
            "zeroRecords": "Tiada padanan rekod yang dijumpai.",
            "paginate": {
                "first": "Pertama",
                "previous": "Sebelum",
                "next": "Seterusnya",
                "last": "Akhir"
            },
            "aria": {
                "sortAscending": ": diaktifkan kepada susunan lajur menaik",
                "sortDescending": ": diaktifkan kepada susunan lajur menurun"
            }

        }

    });


    $('#data-table_filter').append("<button onclick='window.location.href=\"/Class/Create/\"' class='btn btn-primary btn-add pure-material-button-contained'><i class='fas fa-plus'></i><span style='margin-left: 5px; ' class='btn-text'>Tambah Baru</span></button>");
});


function deleteItem(classid) {
    //alert(id);
    var confirmation = confirm("Adakah anda pasti memadam rekod kelas ini? Markah dan sasaran yang terlibat juga akan dipadam?");
    if (confirmation) {
        $.post("/Class/Delete/" + classid, {

        },
            function (data, status) {
                alert(data.result);
                window.location = "/Class/Index";
            });
    }
}