﻿$(document).ready(function() {

    var examId = $("#examId").val();
    var subjectId = $("#subjectId").val();

    var table = $('#data-table').DataTable({

        "ajax": {
            "url": "/SubjectClass/GetSubjectClassIndex/" + subjectId + "?examId=" + examId,
            "type": "POST",
            "datatype": "json",
            "dataSrc": 'classes',

        },
        "columns": [
            { "data": "ClassName", "name": "SubjectName" },
            { "data": "ClassType", "name": "SubjectCode" },
            { "data": "ClassForm", "name": "SubjectType" },
            { "data": "ClassAdmin.AdminName", "name": "ClassAdmin.AdminName" },
            { "data": "ClassTeacher.TeacherName", "name": "ClassTeacher.TeacherName" },
            {
                "mRender": function(data, type, row) {
                    return '<h6><b>Nama Kelas</b></h6><h6' + row.ClassName + '</h6><h6><b>Jenis Kelas</b></h6><h6>' + row.ClassType + '</h6><h6><b>Tingkatan</b></h6><h6>' + row.ClassForm + '</h6><h6><b>Penyelia</b></h6><h6>' + row.AdminName + '</h6><h6><b>Guru Bertugas</b></h6><h6>' + row.TeacherName + '</h6>';
                }
            },
            {
                "mRender": function(data, type, row) {
                    return '<div class="dropdown"> <button class="btn btn-sm btn-secondary pure-material-button-contained" style = "width: 23.25px;" type="button" id= "dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <i class="fas fa-ellipsis-v"></i> </button > <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"> <a class="dropdown-item" onclick="window.location.href=\'/ClassAdmin/ChangeAdmin/' + row.ClassId + '?subjectId=' + subjectId + '&examId=' + examId + '\'"> <i class="fas fa-pencil-alt"></i><span style="margin-left: 5px;" class="btn-text">Tukar Penyelia</span> </a> <a class="dropdown-item" onclick="window.location.href=\'/Student/ClassStudentIndex/' + row.ClassId + '?subjectId=' + subjectId + '&examId=' + examId + '\'"><i class="fas fa-clipboard"></i> <span style="margin-left: 5px;" class="btn-text">Senarai Pelajar</span></a><a class="dropdown-item" onclick="deleteItem(' + row.ClassId + ', ' + subjectId + ')"> <i class="fas fa-trash"></i> <span style="margin-left: 5px;" class="btn-text">Keluarkan Dari Mata Pelajaran</span> </a> </div> </div >';
                }
            }
        ],
        "columnDefs": [{
                targets: 0,
                className: 'desktop-column',
            },
            {
                targets: 1,
                className: 'desktop-column',
            },
            {
                targets: 2,
                className: 'desktop-column',
            },
            {
                targets: 3,
                className: 'desktop-column',
            },
            {
                targets: 4,
                className: 'desktop-column',
            },
            {
                targets: 5,
                className: 'mobile-column',
            },
        ],
        "serverSide": "true",
        "order": [0, "asc"],

        "language": {
            search: 'Cari',
            searchPlaceholder: 'Nama Mata Pelajaran',
            zeroRecords: "Tiada Rekod",
            oPaginate: {
                "sNext": "seterus",
                "sPrevious": "sebelum"
            },
            "emptyTable": "Tiada data",
            "info": "Paparan dari _START_ hingga _END_ dari _TOTAL_ rekod",
            "infoEmpty": "Paparan 0 hingga 0 dari 0 rekod",
            "infoFiltered": "(Ditapis dari jumlah _MAX_ rekod)",
            "infoThousands": ",",
            "lengthMenu": "Papar _MENU_ rekod",
            "loadingRecords": "Diproses...",
            "processing": "Sedang diproses...",
            "search": "Carian:",
            "zeroRecords": "Tiada padanan rekod yang dijumpai.",
            "paginate": {
                "first": "Pertama",
                "previous": "Sebelum",
                "next": "Seterusnya",
                "last": "Akhir"
            },
            "aria": {
                "sortAscending": ": diaktifkan kepada susunan lajur menaik",
                "sortDescending": ": diaktifkan kepada susunan lajur menurun"
            }

        }

    });


    $('#data-table_filter').append("<div class='dropdown' ><button class='btn btn-primary btn-add pure-material-button-contained' type='button' id = 'dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Tambah Kelas</button><div class='dropdown-menu dropdown-menu-right' style='width: 300px' id='dropMenu' aria-labelledby='dropdownMenuButton'><input type='text' id='search' class='form-control' placeholder = 'taip di sini untuk mencari kelas' onKeyUp = 'searchClass()' /><div id='menuOptions'></div><a class='class-btn dropdown-item subject-btn' href='/Class/Index'>Mengurus Senarai Kelas</a></div></div >");

    $('#dropdownMenuButton').on('click', function() {

        //get exam id
        var examId = $("#examId").val();

        var subjectId = $("#subjectId").val();

        $.post("/SubjectClass/GetClassList", {
                id: subjectId
            },
            function(data, status) {

                $('#menuOptions').empty();

                var classes = data;

                //console.log(subjects);

                for (var i = 0; i < classes.result.length; i++) {

                    $('#menuOptions').append("<a class='dropdown-item' href='/SubjectClass/AddToSubject/" + classes.result[i].ClassId + "?subjectId=" + subjectId + "&examId=" + examId + "'>" + classes.result[i].ClassName + "</a>");

                    //alert(subjects.result[i].SubjectName);

                }
            });

    });
});

function searchClass() {
    var query = $("#search").val();
    var subjectId = $("#subjectId").val();
    var examId = $("#examId").val();

    $.post("/Class/SearchClass", {
            query: query,
            subjectId: subjectId
        },
        function(data, status) {

            $('#menuOptions').empty();

            var classes = data;

            //console.log(subjects);

            for (var i = 0; i < classes.result.length; i++) {

                $('#menuOptions').append("<a class='dropdown-item' href='/SubjectClass/AddToSubject/" + classes.result[i].ClassId + "?subjectId=" + subjectId + "&examId=" + examId + "'>" + classes.result[i].ClassName + "</a>");

                //alert(classes.result[i].ClassName);

            }
        });
}


function deleteItem(classId, subjectId) {
    //alert(id);
    var examId = $("#examId").val();

    var confirmation = confirm("Adakah anda pasti mengeluarkan kelas ini dari mata pelajaran? Rekod-rekod markah yang terlibat juga akan dipadam");
    if (confirmation) {
        $.post("/SubjectClass/RemoveFromSubject/" + classId + "?subjectId=" + subjectId + "&examId=" + examId, {

            },
            function(data, status) {
                alert("Kelas berjaya dikeluarkan dari mata pelajaran");
                window.location = "/SubjectClass/SubjectClassIndex/" + subjectId + "?examId=" + examId;
            });
    }
}