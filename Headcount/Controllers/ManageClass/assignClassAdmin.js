﻿$(document).ready(function() {

    $('#save').on('click', function() {
        $("#error-box").empty();
        var subjectId = $("#subjectId").val();
        var examId = $("#examId").val();

        var classId = $("#classId").val();

        var administratorId = $("#administratorId").select2('data')[0].id;
        var teacherId = $("#teacherId").select2('data')[0].id;

        var methodId = $("#methodId").select2('data')[0].id;
        var classHour = $("#classHour").val();

        var validForm = true;
        var notExisted = true;

        if (!methodId) {
            methodId = 0;
        }

        if (classHour == "" || classHour == null) {
            classHour = 0;
        }

        $.post("/SubjectClass/CheckSubjectClassExistence", {
                classId: classId,
                subjectId: subjectId,
                examId: examId
            },
            function(data, status) {

                if (parseInt(data.result) == 1) {
                    validForm = false;
                    notExisted = false;

                    alert("Kelas ini sudah ada dalam mata pelajaran ini.");

                    window.location = "/SubjectClass/SubjectClassIndex/" + subjectId + "?examId=" + examId;
                }

                //form validation
                //check required fields
                if ((administratorId == "" || teacherId == "") && notExisted) {
                    validForm = false;
                    $("#error-box").removeClass("error-box");
                    $("#error-box").append("Sila periksa semula ruangan yang wajib diisi *<br>");
                }

                if (isNaN(classHour)) {
                    validForm = false;
                    $("#error-box").removeClass("error-box");
                    $("#error-box").append("Sila isikan nombor untuk ruangan 'Jumlah Masa Dalam Seminggu'<br>");
                }

                if (validForm) {
                    $("#error-box").addClass("error-box");

                    //alert('valid form');
                    $.post("/SubjectClass/SaveAddToSubject/", {
                            subjectId: subjectId,
                            classId: classId,
                            examId: examId,
                            adminId: administratorId,
                            teacherId: teacherId,
                            methodId: methodId,
                            classHour: classHour

                        },
                        function(data, status) {
                            alert(data.result);

                            window.location = "/SubjectClass/SubjectClassIndex/" + subjectId + "?examId=" + examId;
                        });
                }
            });
    });
});