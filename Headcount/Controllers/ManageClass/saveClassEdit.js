﻿$(document).ready(function () {
    $('#save').on('click', function () {

        $("#error-box").empty();
        var validForm = true;

        var classId = $('#classId').val();
        var name = $('#name').val();
        var form = $('#form').val();
        var type = $('#type').val();
        var remarks = $('#remarks').val();

        //form validation
        //check required fields
        if (name == "" || form == "" || type == "") {
            validForm = false;
            $("#error-box").removeClass("error-box");
            $("#error-box").append("Sila periksa semula ruangan yang wajib diisi *<br>");
        }
       
       
        if (validForm) {
           $("#error-box").addClass("error-box");

               //alert('valid form');
               $.post("/Class/SaveEdit/", {
                   classId: classId,
                   className: name,
                   classForm: form,
                   classType: type,
                   classRemarks: remarks,
                
               },
               function (data, status) {
                       alert(data.result);
                       window.location = "/Class/Index/";
               });
        }


    });

});