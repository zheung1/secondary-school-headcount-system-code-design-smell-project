﻿$(document).ready(function () {

    var table = $('#data-table').DataTable({

        "ajax": {
            "url": "/Teacher/GetTeacherList",
            "type": "POST",
            "datatype": "json",
            "dataSrc": 'teachers',

        },
        "columns": [
            { "data": "TeacherName", "name": "TeacherName" },
            { "data": "TeacherEmail", "name": "TeacherEmail" },
            { "data": "Username", "name": "Username" },
            { "data": "TeacherHp", "name": "TeacherHp" },
            { "data": "TeacherOfficeTel", "name": "TeacherOfficeTel" },
            {
                "mRender": function (data, type, row) {
                    return '<h6><b>Nama Penuh</b></h6><h6>' + row.TeacherName + '</h6><h6><b>E-Mel</b></h6><h6>' + row.TeacherEmail + '</h6><b>Nama Pengguna</b></h6><h6>' + row.Username + '</h6><h6><b>No Telefon Bimbit</b></h6><h6>' + row.TeacherHp + '</h6><h6><b>No Telefon Pejabat</b></h6><h6>' + row.TeacherOfficeTel + '</h6>';
                }
            },
            {
                "mRender": function (data, type, row) {
                    return '<div class="dropdown" ><button class="btn btn-sm btn-secondary pure-material-button-contained" type="button" style="width: 23.25px;" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><i class="fas fa-ellipsis-v"></i></button ><div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"><a class="dropdown-item" onclick="window.location.href=\'/Teacher/Edit/' + row.UserId + '\'"><i class="fas fa-pencil-alt"></i><span style="margin-left: 5px;" class="btn-text">Sunting</span></a><a class="dropdown-item" onclick="deleteItem(' + row.UserId + ')"><i class="fas fa-trash"></i><span style="margin-left: 5px;" class="btn-text">Padam</span></a></div></div>';
                }
            }
        ],
        "columnDefs": [
            {
                targets: 0,
                className: 'desktop-column',
            },
            {
                targets: 1,
                className: 'desktop-column',
            },
            {
                targets: 2,
                className: 'desktop-column',
            },
            {
                targets: 3,
                className: 'desktop-column',
            },
            {
                targets: 4,
                className: 'desktop-column',
            },
            {
                targets: 5,
                className: 'mobile-column',
            },
        ],
        "serverSide": "true",
        "order": [0, "asc"],

        "language": {
            search: 'Cari',
            searchPlaceholder: 'Nama Penuh',
            zeroRecords: "Tiada Rekod",
            oPaginate: {
                "sNext": "seterus",
                "sPrevious": "sebelum"
            },
            "emptyTable": "Tiada data",
            "info": "Paparan dari _START_ hingga _END_ dari _TOTAL_ rekod",
            "infoEmpty": "Paparan 0 hingga 0 dari 0 rekod",
            "infoFiltered": "(Ditapis dari jumlah _MAX_ rekod)",
            "infoThousands": ",",
            "lengthMenu": "Papar _MENU_ rekod",
            "loadingRecords": "Diproses...",
            "processing": "Sedang diproses...",
            "search": "Carian:",
            "zeroRecords": "Tiada padanan rekod yang dijumpai.",
            "paginate": {
                "first": "Pertama",
                "previous": "Sebelum",
                "next": "Seterusnya",
                "last": "Akhir"
            },
            "aria": {
                "sortAscending": ": diaktifkan kepada susunan lajur menaik",
                "sortDescending": ": diaktifkan kepada susunan lajur menurun"
            }

        }

    });


    $('#data-table_filter').append("<button onclick='window.location.href=\"/Teacher/Create/\"' class='btn btn-primary btn-add pure-material-button-contained'><i class='fas fa-plus'></i><span style='margin-left: 5px; ' class='btn-text'>Tambah Baru</span></button>");
});


function deleteItem(userid) {
    //alert(id);
    var confirmation = confirm("Adakah anda pasti mahu menutup akaun pengguna guru ini?");
    if (confirmation) {
        $.post("/Teacher/Delete/" + userid, {

        },
            function (data, status) {
                alert(data.result);
                window.location = "/Teacher/Index";
            });
    }
}