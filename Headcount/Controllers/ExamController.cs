﻿using Headcount.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Models
{
    public class ExamController : Controller
    {
        // GET: Exam
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetExamList()
        {
            Exam exam = new Exam();

            exam.Uuid = (string)Session["uuid"];

            int start = Convert.ToInt32(Request["start"]);

            int length = Convert.ToInt32(Request["length"]);

            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];

            string sortDirection = Request["order[0][dir]"];

            List<Exam> exams = exam.GetExamList(start, length, searchValue, sortColumnName, sortDirection);

            int filteredCount = exam.GetFilteredCount(searchValue);

            int totalCount = exam.GetExamCount();

            return Json(new
            {
                exams,
                recordsTotal = totalCount,
                draw = Request["draw"],
                recordsFiltered = filteredCount
            },
            JsonRequestBehavior.AllowGet
            );

        }

        public ActionResult Create()
        {
            ViewBag.months = Constants.months;

            return View();
        }

        public ActionResult ShowInfo(int id)
        {
            Exam exam = new Exam();

            exam = exam.GetExam(id);

            ViewBag.exam = exam;

            return View();
        }

        public ActionResult Edit(int id)
        {
            Exam exam = new Exam();

            exam = exam.GetExam(id);

            ViewBag.exam = exam;

            return View();
        }

        public ActionResult SaveEdit(int examId, string examName, string examStartDate, string examEndDate, string examLevel, string examRemarks, int adminId, string examYear, string examMarkType)
        {
            Exam exam = new Exam();

            exam.ExamId = examId;

            exam.ExamName = DbConfig.EscapeString(examName);

            exam.ExamStartDate = DbConfig.EscapeString(examStartDate);

            exam.ExamEndDate = DbConfig.EscapeString(examEndDate);

            exam.ExamLevel = DbConfig.EscapeString(examLevel);

            exam.ExamRemarks = DbConfig.EscapeString(examRemarks);

            Admin admin = new Admin();

            admin.AdminId = adminId;

            exam.ExamAdmin = admin;

            exam.ExamYear = DbConfig.EscapeString(examYear);

            exam.ExamMarkType = DbConfig.EscapeString(examMarkType);

            exam.UpdateExam();

            return Json(new
            {
                result = "Maklumat peperiksaan berjaya disunting"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult Save(string examName, 
            string examStartDate, 
            string examEndDate,
            string examLevel,
            string examRemarks,
            int adminId,
            string examYear,
            string examMarkType
        )
        {
            Exam exam = new Exam();

            exam.ExamName = DbConfig.EscapeString(examName);
            exam.ExamStartDate = DbConfig.EscapeString(examStartDate);
            exam.ExamEndDate = DbConfig.EscapeString(examEndDate);
            exam.ExamLevel = DbConfig.EscapeString(examLevel);
            exam.ExamRemarks = DbConfig.EscapeString(examRemarks);

            Admin admin = new Admin();
            admin.AdminId = adminId;
            exam.ExamAdmin = admin;

            exam.ExamYear = DbConfig.EscapeString(examYear);
            exam.ExamMarkType = DbConfig.EscapeString(examMarkType);

            exam.Uuid = (string)Session["uuid"];

            exam.SaveExam();

            return Json(new
            {
                result = "Maklumat peperiksaan berjaya disimpan"
                //result = exam.ExamName
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult CheckExamExistence(string examLevel, string examYear, string examMarkType)
        {
            Exam exam = new Exam();
            
            exam.ExamLevel = DbConfig.EscapeString(examLevel);
            exam.ExamYear = DbConfig.EscapeString(examYear);
            exam.ExamMarkType = DbConfig.EscapeString(examMarkType);
           
            exam.Uuid = (string)Session["uuid"];

            bool exist = exam.CheckExistence();

            if (exist)
            {
                return Json(new
                {
                    result = 1
                },
                    JsonRequestBehavior.AllowGet
                );
            }
            else
            {
                return Json(new
                {
                    result = 0
                },
                    JsonRequestBehavior.AllowGet
                );
            }
        }

        public ActionResult Delete(int id)
        {
            Exam exam = new Exam();
            exam.DeleteExam(id);

            return Json(new
            {
                result = "Maklumat peperiksaan berjaya dipadam"
            },
            JsonRequestBehavior.AllowGet
            );


        }



    }
}