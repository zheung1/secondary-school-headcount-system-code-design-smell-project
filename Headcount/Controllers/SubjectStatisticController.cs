﻿using Headcount.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
    public class SubjectStatisticController : Controller
    {
        public ActionResult ClassHourStatistic(int id, int examId, int pageNumber = 0)
        {
            
            Subject subject = new Subject();

            subject.SubjectId = id;

            subject = subject.GetSubject(id);

            SubjectStatistic subjectStatistic = new SubjectStatistic(subject);
            
            ArrayList examYears = subjectStatistic.GetSubjectYears(pageNumber);


            ViewBag.examYears = examYears;

            ViewBag.examId = examId;

            ViewBag.subject = subject;

            return View();
        }

        public ActionResult GetClassHourStatistic(string examYear, int subjectId)
        {
            Subject subject = new Subject();

            Exam exam = new Exam();

            exam.ExamYear = examYear;

            subject.SubjectExam = exam;

            subject.SubjectId = subjectId;

            SubjectStatistic subjectStatistic = new SubjectStatistic(subject);

            ArrayList averages = subjectStatistic.GetClassHourAverage();

            return Json(new
            {
                results = averages
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult TeachingMethodStatistic(int id, int examId, int pageNumber = 0)
        {
            Subject subject = new Subject();

            subject.SubjectId = id;

            subject = subject.GetSubject(id);

            SubjectStatistic subjectStatistic = new SubjectStatistic(subject);

            ArrayList examYears = subjectStatistic.GetSubjectYears(pageNumber);

            ViewBag.examYears = examYears;

            ViewBag.examId = examId;

            ViewBag.subject = subject;

            ViewBag.pageNumber = pageNumber;

            return View();
        }

        public ActionResult GetTeachingMethodStatistic(string examYear, int subjectId)
        {
            Subject subject = new Subject();

            Exam exam = new Exam();

            exam.ExamYear = examYear;

            subject.SubjectExam = exam;

            subject.SubjectId = subjectId;

            SubjectStatistic subjectStatistic = new SubjectStatistic(subject);

            ArrayList averages = subjectStatistic.GetTeachingMethodAverage();

            return Json(new
            {
                results = averages
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult GetSubjectYearCount(int subjectId)
        {
            Subject subject = new Subject();

            subject.SubjectId = subjectId;

            SubjectStatistic subjectStatistic = new SubjectStatistic(subject);

            int subjectYearCount = subjectStatistic.GetSubjectYearCount();

            return Json(new
            {
                result = subjectYearCount
            },
            JsonRequestBehavior.AllowGet
            );
        }

    }

}