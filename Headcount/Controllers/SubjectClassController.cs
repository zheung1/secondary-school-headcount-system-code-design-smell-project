﻿using Headcount.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
    public class SubjectClassController : Controller
    {
        public ActionResult SubjectClassIndex(int id, int examId) //subjectId, examId
        {
            Subject subject = new Subject();

            subject = subject.GetSubject(id);

            ViewBag.subject = subject;

            ViewBag.exam = subject.GetRelatedExam(examId);

            return View();
        }

        public ActionResult GetSubjectClassIndex(int id, int examId) //subjectId, examId
        {
            SubjectClass subjectClass = new SubjectClass();

            subjectClass.Uuid = (string)Session["uuid"];

            int start = Convert.ToInt32(Request["start"]);

            int length = Convert.ToInt32(Request["length"]);

            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];

            string sortDirection = Request["order[0][dir]"];

            List<Class> classes = subjectClass.GetSubjectClassIndex(start, length, searchValue, sortColumnName, sortDirection, id, examId);

            int filteredCount = subjectClass.GetFilteredSubjectClassCount(searchValue, id, examId);

            int totalCount = subjectClass.GetSubjectClassCount(id, examId);

            return Json(new
            {
                classes,
                recordsTotal = totalCount,
                draw = Request["draw"],
                recordsFiltered = filteredCount
            },
            JsonRequestBehavior.AllowGet
            );

        }

        public ActionResult GetClassList(int id) // subjectId
        {
            //get subject level
            Subject subject = new Subject();

            subject = subject.GetSubject(id);

            string examLevel = subject.SubjectLevel;

            ArrayList classes = new ArrayList();

            Class kelas = new Class();

            kelas.Uuid = (string)Session["uuid"];

            classes = kelas.GetClassList(examLevel);

            return Json(new
            {
                result = classes
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult AddToSubject(int id, int subjectId, int examId) //classId, subjectId, examId
        {

            ViewBag.classId = id;

            ViewBag.subjectId = subjectId;

            ViewBag.examId = examId;

            return View();
        }

        public ActionResult CheckSubjectClassExistence(int classId, int subjectId, int examId)
        {
            SubjectClass subjectClass = new SubjectClass();

            subjectClass.ClassId = classId;

            Subject subject = new Subject();

            subject.SubjectId = subjectId;

            subject.SubjectExam = subject.GetRelatedExam(examId);

            subjectClass.ClassSubject = subject;

            subjectClass.Uuid = (string)Session["uuid"];

            bool exist = subjectClass.CheckSubjectClassExistence();

            if (exist)
            {
                return Json(new
                {
                    result = 1
                },
                    JsonRequestBehavior.AllowGet
                );
            }
            else
            {
                return Json(new
                {
                    result = 0
                },
                    JsonRequestBehavior.AllowGet
                );
            }
        }

        public ActionResult SaveAddToSubject(int subjectId, int classId, int examId, int adminId, int teacherId, int methodId, double classHour)
        {
            SubjectClass subjectClass = new SubjectClass();

            subjectClass.ClassId = classId;

            Subject subject = new Subject();

            subject.SubjectId = subjectId;

            subjectClass.ClassSubject = subject;

            subjectClass.ClassSubject.SubjectExam = subject.GetRelatedExam(examId);

            Admin admin = new Admin();

            admin.AdminId = adminId;

            subjectClass.ClassAdmin = admin;

            Teacher teacher = new Teacher();

            teacher.TeacherId = teacherId;

            Method method = new Method();

            if (methodId != 0)
            {
                method.MethodId = methodId;
            }
            else
            {
                method.MethodId = 0;
            }

            subjectClass.ClassMethod = method;

            subjectClass.ClassTeacher = teacher;

            subjectClass.ClassHour = classHour;

            subjectClass.Uuid = (string)Session["uuid"];

            subjectClass.AddToSubject();

            return Json(new
            {
                result = "Kelas berjaya ditambah ke mata pelajaran"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult RemoveFromSubject(int id, int subjectId, int examId)
        {
            SubjectClass subjectClass = new SubjectClass();

            subjectClass.ClassId = id;

            Subject subject = new Subject();

            subject.SubjectId = subjectId;

            subject.SubjectExam = subject.GetRelatedExam(examId); 

            subjectClass.ClassSubject = subject;

            subjectClass.RemoveFromSubject();

            return Json(new
            {
                result = "Kelas berjaya dikeluarkan dari mata pelajaran"
            },
                    JsonRequestBehavior.AllowGet
            );
        }
    }

    


}