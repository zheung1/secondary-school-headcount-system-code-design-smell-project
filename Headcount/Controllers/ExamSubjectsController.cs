﻿using Headcount.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
    public class ExamSubjectsController : Controller
    {
        public ActionResult ExamSubjectIndex(int id) //examId
        {
            Exam exam = new Exam();

            exam = exam.GetExam(id);

            ViewBag.exam = exam;

            return View();
        }

        public ActionResult GetExamSubjectIndex(int id)//examId
        {
            ExamSubject examSubject = new ExamSubject();

            examSubject.Uuid = (string)Session["uuid"];

            int start = Convert.ToInt32(Request["start"]);

            int length = Convert.ToInt32(Request["length"]);

            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];

            string sortDirection = Request["order[0][dir]"];

            List<Subject> subjects = examSubject.GetExamSubjectIndex(start, length, searchValue, sortColumnName, sortDirection, id);

            int filteredCount = examSubject.GetFilteredExamSubjectCount(searchValue, id);

            int totalCount = examSubject.GetExamSubjectCount(id);

            return Json(new
            {
                subjects,
                recordsTotal = totalCount,
                draw = Request["draw"],
                recordsFiltered = filteredCount
            },
            JsonRequestBehavior.AllowGet
            );

        }


        public ActionResult CheckExamSubjectExistence(int subjectId, int examId)
        {
            ExamSubject examSubject = new ExamSubject();

            examSubject.SubjectId = subjectId;

            Exam exam = new Exam();
            exam.ExamId = examId;

            examSubject.Uuid = (string)Session["uuid"];

            examSubject.SubjectExam = exam;

            bool exist = examSubject.CheckExamSubjectExistence();

            if (exist)
            {
                return Json(new
                {
                    result = 1
                },
                    JsonRequestBehavior.AllowGet
                );
            }
            else
            {
                return Json(new
                {
                    result = 0
                },
                    JsonRequestBehavior.AllowGet
                );
            }
        }

        public ActionResult RemoveFromExam(int id, int examId)
        {
            ExamSubject examSubject = new ExamSubject();
            examSubject.SubjectId = id;

            Exam exam = new Exam();
            exam.ExamId = examId;

            examSubject.SubjectExam = exam;

            examSubject.RemoveFromExam();

            return Json(new
            {
                result = "Mata pelajaran berjaya dikeluarkan dari peperiksaan"
            },
                    JsonRequestBehavior.AllowGet
            );

        }
    }
}