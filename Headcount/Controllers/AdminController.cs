﻿using Headcount.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Headcount.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAdminList()
        {
            Admin admin = new Admin();

            admin.Uuid = (string)Session["uuid"];

            int start = Convert.ToInt32(Request["start"]);

            int length = Convert.ToInt32(Request["length"]);

            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];

            string sortDirection = Request["order[0][dir]"];

            List<Admin> admins = admin.GetAdminList(start, length, searchValue, sortColumnName, sortDirection);

            int filteredCount = admin.GetFilteredCount(searchValue);

            int totalCount = admin.GetAdminCount();

            return Json(new
            {
                admins,
                recordsTotal = totalCount,
                draw = Request["draw"],
                recordsFiltered = filteredCount
            },
            JsonRequestBehavior.AllowGet
            );

        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Save(
            string adminName,
            string adminAddress,
            string adminHp,
            string adminRemarks,
            string adminEmail,
            string adminHomeTel,
            string adminOfficeTel,
            string username,
            string password
        )
        {
            Admin admin = new Admin();
            admin.Username = DbConfig.EscapeString(username);
            admin.Password = DbConfig.EscapeString(password);
            admin.AdminEmail = DbConfig.EscapeString(adminEmail);
            admin.AdminName = DbConfig.EscapeString(adminName);
            admin.AdminAddress = DbConfig.EscapeString(adminAddress);
            admin.AdminHomeTel = DbConfig.EscapeString(adminHomeTel);
            admin.AdminOfficeTel = DbConfig.EscapeString(adminOfficeTel);
            admin.AdminHp = DbConfig.EscapeString(adminHp);
            admin.AdminRemarks = DbConfig.EscapeString(adminRemarks);
            admin.Type = "admin";
            admin.Uuid = (string)Session["uuid"];

            admin.SaveAdmin();

            return Json(new
            {
                result = "Maklumat penyelia berjaya disimpan"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult ShowInfo(int id)
        {
            Admin admin = new Admin();

            admin = admin.GetAdmin(id);

            ViewBag.admin = admin;

            return View();
        }

        public ActionResult Edit(int id)
        {
            Admin admin = new Admin();

            admin = admin.GetAdmin(id);

            ViewBag.admin = admin;

            return View();
        }

        public ActionResult SaveEdit(
            int userId, 
            string adminName, 
            string adminAddress, 
            string adminHp, 
            string adminRemarks, 
            string adminEmail, 
            string adminHomeTel, 
            string adminOfficeTel, 
            string username, 
            string password
        )
        {
            Admin admin = new Admin();
            admin.UserId = userId;
            admin.Username = DbConfig.EscapeString(username);
            admin.Password = DbConfig.EscapeString(password);
            admin.Type = "admin";
            admin.AdminEmail = DbConfig.EscapeString(adminEmail);
            admin.AdminName = DbConfig.EscapeString(adminName);
            admin.AdminAddress = DbConfig.EscapeString(adminAddress);
            admin.AdminHomeTel = DbConfig.EscapeString(adminHomeTel);
            admin.AdminOfficeTel = DbConfig.EscapeString(adminOfficeTel);
            admin.AdminHp = DbConfig.EscapeString(adminHp);
            admin.AdminRemarks = DbConfig.EscapeString(adminRemarks);

            admin.UpdateAdmin();

            return Json(new
            {
                result = "Maklumat penyelia berjaya disunting"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        
        public ActionResult Delete(int id) //admin user id
        {
            string uuid = "12535117-e19b-401b-9ff2-631023b7a6d8";

            Admin admin = new Admin();
            admin.Uuid = uuid;

            int ownerId = 0;

            ownerId = admin.GetOwnerId();

            admin.DeleteAdmin(ownerId, id); //ownerId, soon2be delete  user id

            return Json(new
            {
                result = "Akaun penyelia berjaya ditutup. Peperiksaan, mata pelajaran dan kelas yang terlibat sekarang diletakkan dibawah penyeliaan pemilik sistem."
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public string AdminDropdownList()
        {
            Admin admin = new Admin();

            ArrayList dropdownOptions = admin.GetAdminDropdownList();

            var json = new JavaScriptSerializer().Serialize(dropdownOptions);
            return json;

        }
    }


}