﻿using Headcount.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Headcount.Controllers
{
    public class TeacherController : Controller
    {
        // GET: Teacher
        public ActionResult Index()
        { 
            return View();
        }

        public ActionResult GetTeacherList()
        {
            Teacher teacher = new Teacher();

            teacher.Uuid = (string)Session["uuid"];

            int start = Convert.ToInt32(Request["start"]);

            int length = Convert.ToInt32(Request["length"]);

            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];

            string sortDirection = Request["order[0][dir]"];

            List<Teacher> teachers = teacher.GetTeacherList(start, length, searchValue, sortColumnName, sortDirection);

            int filteredCount = teacher.GetFilteredCount(searchValue);

            int totalCount = teacher.GetTeacherCount();

            return Json(new
            {
                teachers,
                recordsTotal = totalCount,
                draw = Request["draw"],
                recordsFiltered = filteredCount
            },
            JsonRequestBehavior.AllowGet
            );

        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Save(
            string teacherName,
            string teacherAddress,
            string teacherHp,
            string teacherRemarks,
            string teacherEmail,
            string teacherHomeTel,
            string teacherOfficeTel,
            string username,
            string password
        )
        {
            Teacher teacher = new Teacher();
            teacher.Username = DbConfig.EscapeString(username);
            teacher.Password = DbConfig.EscapeString(password);
            teacher.Type = "teacher";
            teacher.TeacherEmail = DbConfig.EscapeString(teacherEmail);
            teacher.TeacherName = DbConfig.EscapeString(teacherName);
            teacher.TeacherAddress = DbConfig.EscapeString(teacherAddress);
            teacher.TeacherHomeTel = DbConfig.EscapeString(teacherHomeTel);
            teacher.TeacherOfficeTel = DbConfig.EscapeString(teacherOfficeTel);
            teacher.TeacherHp = DbConfig.EscapeString(teacherHp);
            teacher.TeacherRemarks = DbConfig.EscapeString(teacherRemarks);
            teacher.Uuid = (string)Session["uuid"];

            teacher.SaveTeacher();

            return Json(new
            {
                result = "Maklumat guru berjaya disimpan"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult ShowInfo(int id)
        {
            Teacher teacher = new Teacher();

            teacher = teacher.GetTeacher(id);

            ViewBag.teacher = teacher;

            return View();
        }

        public ActionResult Edit(int id)
        {
            Teacher teacher = new Teacher();

            teacher = teacher.GetTeacher(id);

            ViewBag.teacher = teacher;

            return View();
        }

        public ActionResult SaveEdit(
            int userId, 
            string teacherName, 
            string teacherAddress, 
            string teacherHp, 
            string teacherRemarks, 
            string teacherEmail, 
            string teacherHomeTel, 
            string teacherOfficeTel, 
            string username, 
            string password
        )
        {
            Teacher teacher = new Teacher();
            teacher.UserId = userId;
            teacher.Username = DbConfig.EscapeString(username);
            teacher.Password = DbConfig.EscapeString(password);
            teacher.TeacherEmail = DbConfig.EscapeString(teacherEmail);
            teacher.TeacherName = DbConfig.EscapeString(teacherName);
            teacher.TeacherAddress = DbConfig.EscapeString(teacherAddress);
            teacher.TeacherHomeTel = DbConfig.EscapeString(teacherHomeTel);
            teacher.TeacherOfficeTel = DbConfig.EscapeString(teacherOfficeTel);
            teacher.TeacherHp = DbConfig.EscapeString(teacherHp);
            teacher.TeacherRemarks = DbConfig.EscapeString(teacherRemarks);

            teacher.UpdateTeacher();

            return Json(new
            {
                result = "Maklumat guru berjaya disunting"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult Delete(int id)
        {
            string uuid = "12535117-e19b-401b-9ff2-631023b7a6d8";

            Teacher teacher = new Teacher();
            teacher.Uuid = uuid;

            int ownerId = 0;

            ownerId = teacher.GetOwnerId();

            teacher.DeleteTeacher(ownerId, id); //ownerId, soon2be delete  user id

            return Json(new
            {
                result = "Akaun guru berjaya ditutup. Peperiksaan, mata pelajaran dan kelas yang terlibat sekarang diletakkan dibawah penyeliaan pemilik sistem."
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public string TeacherDropdownList()
        {
            Teacher teacher = new Teacher();

            ArrayList dropdownOptions = teacher.GetTeacherDropdownList();

            var json = new JavaScriptSerializer().Serialize(dropdownOptions);
            return json;

        }
    }


}