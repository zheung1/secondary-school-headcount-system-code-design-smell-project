﻿using Headcount.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
   
    public class SubjectController : Controller
    {
        // GET: Subject
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult GetSubjectIndex()
        {
            Stopwatch timer = new Stopwatch();

            timer.Start();

            Subject subject = new Subject();

            subject.Uuid = (string)Session["uuid"];

            int start = Convert.ToInt32(Request["start"]);

            int length = Convert.ToInt32(Request["length"]);

            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];

            string sortDirection = Request["order[0][dir]"];

            List<Subject> subjects = subject.GetSubjectList(start, length, searchValue, sortColumnName, sortDirection);

            int filteredCount = subject.GetFilteredCount(searchValue);

            int totalCount = subject.GetSubjectCount();

            timer.Stop();

            System.Diagnostics.Debug.WriteLine("Subject list loading time : " + timer.Elapsed);

            return Json(new
            {
                subjects,
                recordsTotal = totalCount,
                draw = Request["draw"],
                recordsFiltered = filteredCount
            },
            JsonRequestBehavior.AllowGet
            );

        }

        public ActionResult GetSubjectList(int id)
        {
            //get exam level
            Exam exam = new Exam();
            exam = exam.GetExam(id);

            string examLevel = exam.ExamLevel;

            ArrayList subjects = new ArrayList();

            Subject subject = new Subject();

            subject.Uuid = (string)Session["uuid"];

            subjects = subject.GetSubjectList(examLevel);
            
            return Json(new
            {
                result = subjects
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Save(string subjectJson)
        {
            
            Subject subject = JsonConvert.DeserializeObject<Subject>(subjectJson);

            subject.Uuid = (string)Session["uuid"];

            subject.SaveSubject();

            return Json(new
            {
                result = "Maklumat mata pelajaran berjaya disimpan"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult Edit(int id)
        {
            Subject subject = new Subject();

            subject = subject.GetSubject(id);

            ViewBag.subject = subject;

            return View();
        }

        public ActionResult SaveEdit(string subjectJson)
        {
            Subject subject = JsonConvert.DeserializeObject<Subject>(subjectJson);

            subject.UpdateSubject();

            return Json(new
            {
                result = "Maklumat Mata Pelajaran berjaya disunting"
            },
            JsonRequestBehavior.AllowGet
            );

        }

        public ActionResult ShowInfo(int id)
        {
            Subject subject = new Subject();

            subject = subject.GetSubject(id);

            ViewBag.subject = subject;

            return View();

        }

        public ActionResult CheckSubjectExistence(string subjectJson)
        {
            Subject subject = JsonConvert.DeserializeObject<Subject>(subjectJson);

            subject.Uuid = (string)Session["uuid"];


            bool exist = subject.CheckExistence();

            if (exist)
            {
                return Json(new
                {
                    result = 1
                },
                    JsonRequestBehavior.AllowGet
                );
            }
            else
            {
                return Json(new
                {
                    result = 0
                },
                    JsonRequestBehavior.AllowGet
                );
            }   
        }

        public ActionResult Delete(int id)
        {
            Subject subject = new Subject();

            subject.DeleteSubject(id);


            return Json(new
            {
                result = "Maklumat mata pelajaran berjaya dipadam"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        


    }
}