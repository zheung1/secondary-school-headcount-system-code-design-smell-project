﻿$(document).ready(function () {

    var examId = $("#examId").val();
    var subjectId = $("#subjectId").val();
    var classId = $("#classId").val();
    
    var pageNumber = $("#pageNumber").val();

    var totalRows = 0;
    var pages = 0;

    $.ajax('/Statistic/GetClassStudentCount/', {
        data: {
            'classId': classId,
            'subjectId': subjectId,
            'examId': examId,

        },
        success: function (data, status, xhr) {

            totalRows = data.result;

            pages = Math.ceil(totalRows / 10);

            //alert(totalRows);


            for (var i = 0; i < pages; i++) {


                $("#pageNavBtn").append("<button type='button' class=\'btn btn-primary page-btn pure-material-button-contained\' style='margin-right: 5px' id='page" + (i + 1) + "'>" + (i + 1) + "</button>");

                var link = "window.location.href=\'/Statistic/ClassStudentIndex/"+classId+"?subjectId=" + subjectId + "&examId=" + examId + "&pageNumber=" + (i) + "\';";

                document.getElementById("page" + (i + 1)).setAttribute("onclick", link);

                if (i == pageNumber) {
                    document.getElementById("page" + (i + 1)).setAttribute("style", "margin-right: 5px; background-color: green");
                } else {
                    document.getElementById("page" + (i + 1)).setAttribute("style", "margin-right: 5px;");
                }

            }


        }
    });

});

/*
function searchStudent() {
   
    var query = $("#search").val();
    var subjectId = $("#subjectId").val();
    var examId = $("#examId").val();
    var classId = $("#classId").val();

    $.post("/Student/SearchStudentStatistic", {
        query: query,
        classId: classId,
        subjectId: subjectId,
        examId: examId,    
    },
        function (data, status) {
            $('#stat-list').empty();
            var students = data;

            //console.log(subjects);

            for (var i = 0; i < students.result.length; i++) {

                //$('#stat-list').append("<a class='dropdown-item' href='/Subject/AddToExam/" + subjects.result[i].SubjectId + "?examId=" + examId + "'>" + subjects.result[i].SubjectName + " (" + subjects.result[i].SubjectCode + ")</a>");

                $('#stat-list').append('<div class="col-md-6" style="margin-top: 10px"> <div id="accordion"> <div class="card graph-card"> <div class="card-header graph-header" id="headingOne" style=\'padding-left: 0px; border-radius: 15px;\'> <h5 class="mb-0"> <button type="button" class="btn btn-link btn-text" data-toggle="collapse" data-target="#' + students.result[i].StudentId + '" aria-expanded="true" aria-controls="' + students.result[i].StudentId + '" onclick="renderChart(' + students.result[i].StudentId + ')"> <i class=\'fas fa-chart-line\' style=\'font-size:24px; float: left; padding-right: 5px; color: black\'></i>' + students.result[i].StudentName + '(' + students.result[i].StudentIc + ') <i class=\'fas fa-angle-down\' style=\'font-size:24px; float: right; padding-left: 5px; color: black\'></i> </button> </h5> </div> <div id="' + students.result[i].StudentId + '" class="collapse" aria-labelledby="headingOne" data-parent="#accordion"> <div class="card-body"> <div class="row"><canvas id="chart-' + students.result[i].StudentId + '"></canvas></div> <div class="row" style="justify-content: flex-end"> <button onclick="window.location.href=\'/Statistic/SubjectClassIndex/?examId=" class="btn btn-primary btn-sm" style="border-radius: 15px"> analisis selanjutnya <i class=\'fas fa-angle-right\' style=\'font-size:24px; float: right; padding-left: 5px; color: white\'></i> </button> </div> </div> </div> </div> </div> </div>');

                //alert(subjects.result[i].SubjectName);

            }
        });
}
*/


function renderChart(id) {
    var subjectId = $("#subjectId").val();
    var examId = $("#examId").val();
    var classId = $("#classId").val();

    var tov = 0,
        oti1 = 0,
        oti2 = 0,
        etr = 0,
        ar1 = 0,
        ar2 = 0,
        finalMarks = 0;

    $.post("/Statistic/StudentMarkTargetStatistic", {
        studentId: id,
        classId: classId,
        subjectId: subjectId,
        examId: examId,
    },
        function (data, status) {
            console.log(data.result);
            //alert("x");
            //alert(data.result.Tov);

            
            if (data.result.StudentTarget.Tov) {
                if (data.result.StudentTarget.Tov == "-") {
                    tov = 0;
                } else {
                    tov = data.result.StudentTarget.Tov;
                }
                
            }
            else {
                tov = 0;
            }

            if (data.result.StudentTarget.Oti1) {

                if (data.result.StudentTarget.Oti1 == "-") {
                    oti1 = 0;
                } else {
                    oti1 = data.result.StudentTarget.Oti1;
                }

            }
            else {
                oti1 = 0;
            }

            if (data.result.StudentTarget.Oti2) {
                if (data.result.StudentTarget.Oti2 == "-") {
                    oti2 = 0;
                } else {
                    oti2 = data.result.StudentTarget.Oti2;
                }
                
            }
            else {
                oti2 = 0;
            }

            if (data.result.StudentTarget.Etr) {

                if (data.result.StudentTarget.Etr == "-") {
                    etr = 0;
                } else {
                    etr = data.result.StudentTarget.Etr;
                }

            }
            else {
                etr = 0;
            }

            if (data.result.StudentResult.Ar1) {

                if (data.result.StudentResult.Ar1 == "-") {
                    ar1 = 0;
                } else {
                    ar1 = data.result.StudentResult.Ar1;
                }

            }
            else {
                ar1 = 0;
            }

            if (data.result.StudentResult.Ar2) {

                if (data.result.StudentResult.Ar2 == "-") {
                    ar2 = 0;
                } else {
                    ar2 = data.result.StudentResult.Ar2;
                }

            }
            else {
                ar2 = 0;
            }

            if (data.result.StudentResult.FinalMarks) {

                if (data.result.StudentResult.FinalMarks == "-") {
                    finalMarks = 0;
                } else {
                    finalMarks = parseFloat(data.result.StudentResult.FinalMarks);
                }

            }
            else {
                finalMarks = 0;
            }
                
            var json = JSON.parse('[{ "markType": "TOV", "Sasaran": ' + tov + ', "Markah": ' + tov + '}, { "markType": "OTI1 vs AR1", "Sasaran": ' + oti1 + ', "Markah": ' + ar1 + '}, { "markType": "OTI2 vs AR2", "Sasaran": ' + oti2 + ', "Markah": ' + ar2 + ' }, { "markType": "Akhir Tahun vs ETR", "Sasaran": ' + etr + ', "Markah": ' + finalMarks + ' }]');


            var labels = json.map(function (e) {
                return e.markType;
            }),
                Sasaran = json.map(function (e) {
                    return e.Sasaran;
                }),
                Markah = json.map(function (e) {
                    return e.Markah;
                });

            var ctx = document.getElementById("chart-" + id).getContext("2d");
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: [{
                        label: "Sasaran",
                        data: Sasaran,
                        borderWidth: 2,
                        backgroundColor: "rgba(6, 167, 125, 0.1)",
                        borderColor: "rgba(6, 167, 125, 1)",
                        pointBackgroundColor: "rgba(225, 225, 225, 1)",
                        pointBorderColor: "rgba(6, 167, 125, 1)",
                        pointHoverBackgroundColor: "rgba(6, 167, 125, 1)",
                        pointHoverBorderColor: "#fff"
                    }, {
                            label: "Markah",
                        data: Markah,
                        borderWidth: 2,
                        backgroundColor: "rgba(26, 143, 227, 0.1)",
                        borderColor: "rgba(26, 143, 227, 1)",
                        pointBackgroundColor: "rgba(225, 225, 225, 1)",
                        pointBorderColor: "rgba(26, 143, 227, 1)",
                        pointHoverBackgroundColor: "rgba(26, 143, 227, 1)",
                        pointHoverBorderColor: "#fff"
                    }, ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                stepSize: 10
                            }
                        }]
                    }
                }
            });
            
        });
}