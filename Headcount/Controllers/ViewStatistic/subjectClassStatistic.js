﻿$(document).ready(function() {

    var examId = $("#examId").val();
    var subjectId = $("#subjectId").val();

    var pageNumber = $("#pageNumber").val();

    var totalRows = 0;
    var pages = 0;

    $.ajax('/Statistic/GetSubjectClassCount/', {
        data: {
            'subjectId': subjectId,
            'examId': examId,

        },
        success: function(data, status, xhr) {

            totalRows = data.result;

            pages = Math.ceil(totalRows / 10);


            for (var i = 0; i < pages; i++) {


                $("#pageNavBtn").append("<button type='button' class=\'btn btn-primary page-btn pure-material-button-contained\' style='margin-right: 5px' id='page" + (i + 1) + "'>" + (i + 1) + "</button>");

                var link = "window.location.href=\'/Statistic/SubjectClassIndex/" + subjectId + "?examId=" + examId + "&pageNumber=" + (i) + "\';";

                document.getElementById("page" + (i + 1)).setAttribute("onclick", link);

                if (i == pageNumber) {
                    document.getElementById("page" + (i + 1)).setAttribute("style", "margin-right: 5px; background-color: green");
                } else {
                    document.getElementById("page" + (i + 1)).setAttribute("style", "margin-right: 5px;");
                }

            }


        }
    });

});

/*
function searchClass() {
    //alert('x');
    var query = $("#search").val();
    var examId = $("#examId").val();
    var subjectId = $("#subjectId").val();

    $.post("/Class/SearchClassStatistic", {
            query: query,
            examId: examId,
            subjectId: subjectId
        },
        function(data, status) {
            $('#stat-list').empty();
            var classes = data;

            //console.log(subjects);

            for (var i = 0; i < classes.result.length; i++) {

                $('#stat-list').append('<div class="col-md-6" style="margin-top: 10px"> <div id="accordion"> <div class="card graph-card"> <div class="card-header graph-header" id="headingOne" style=\'padding-left: 0px; border-radius: 15px;\'> <h5 class="mb-0"> <button type="button" class="btn btn-link btn-text" data-toggle="collapse" data-target="#' + classes.result[i].ClassId + '" aria-expanded="true" aria-controls="' + classes.result[i].ClassId + '" onclick="renderChart(' + classes.result[i].ClassId + ', \'' + classes.result[i].ClassSubject.SubjectLevel + '\')"> <i class=\'fas fa-chart-line\' style=\'font-size:24px; float: left; padding-right: 5px; color: black\'></i>' + classes.result[i].ClassName + ' <i class=\'fas fa-angle-down\' style=\'font-size:24px; float: right; padding-left: 5px; color: black\'></i> </button> </h5> </div> <div id="' + classes.result[i].ClassId + '" class="collapse" aria-labelledby="headingOne" data-parent="#accordion"> <div class="card-body"> <div class="row"><canvas id="myChart-' + classes.result[i].ClassId + '"></canvas></div> <div class="row" style="justify-content: flex-end"> <button onclick="window.location.href=\'/Statistic/SubjectClassIndex/' + classes.result[i].ClassId + '?examId=' + examId + '\'" class="btn btn-primary btn-sm" style="border-radius: 15px"> analisis selanjutnya <i class=\'fas fa-angle-right\' style=\'font-size:24px; float: right; padding-left: 5px; color: white\'></i> </button> </div> </div> </div> </div> </div> </div>');

                //alert(subjects.result[i].SubjectName);

            }
        });
}
*/

function renderChart(id, subjectLevel) { //classId

    //alert(subjectLevel);

    var examId = $("#examId").val();
    var subjectId = $("#subjectId").val();

    $.post("/Statistic/SubjectClassGradeStatistic", {
            classId: id,
            subjectId: subjectId,
            examId: examId
        },
        function(data, status) {
            //console.log(data.result);

            var students = data.result;

            //console.log(students[0].marks);

            if (subjectLevel == "Menengah Bawah") {

                var pt3Grades = [0, 0, 0, 0, 0, 0];

                if (students != null) {
                    for (var i = 0; i < students.length; i++) {

                        var mark = students[i].marks;

                        var gradeIndex = getPt3Grade(mark);

                        //console.log(gradeIndex);

                        pt3Grades[gradeIndex]++;

                    }
                }

                //console.log(pt3Grades);

                var ctx = document.getElementById("myChart-" + id).getContext("2d");
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ["A", "B", "C",
                            "D", "E", "TH"
                        ],
                        datasets: [{
                            label: "Bilangan Pelajar",
                            backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#3e95cd"],
                            data: pt3Grades
                        }]

                    },
                    options: {
                        legend: { display: true },
                        title: {
                            display: true,
                            text: 'Gred',

                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    stepSize: 10
                                },

                            }]
                        }
                    }
                });

            } else if (subjectLevel == "Menengah Atas") {

                var spmGrades = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

                if (students != null) {
                    for (var i = 0; i < students.length; i++) {

                        var mark = students[i].marks;

                        var gradeIndex = getSpmGrade(mark);

                        //console.log(gradeIndex);

                        spmGrades[gradeIndex]++;

                    }
                }

                console.log(spmGrades);

                var ctx = document.getElementById("myChart-" + id).getContext("2d");
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ["A+", "A", "A-",
                            "B+", "B", "C+",
                            "C", "D", "E",
                            "G", "TH"
                        ],
                        datasets: [{
                            label: "Bilangan Pelajar",
                            backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#3e95cd"],
                            data: spmGrades
                        }]

                    },
                    options: {
                        legend: { display: true },
                        title: {
                            display: true,
                            text: 'Gred',

                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    stepSize: 10
                                },

                            }]
                        }
                    }
                });
            }

        });
}

function renderPieChart(id, subjectLevel) { //classId

    //alert(subjectLevel);

    var examId = $("#examId").val();
    var subjectId = $("#subjectId").val();

    $.post("/Statistic/SubjectClassGradeStatistic", {
            classId: id,
            subjectId: subjectId,
            examId: examId
        },
        function(data, status) {
            //console.log(data.result);

            var students = data.result;

            //console.log(students[0].marks);

            if (subjectLevel == "Menengah Bawah") {

                var pt3Grades = [0, 0, 0, 0, 0, 0];

                if (students != null) {
                    for (var i = 0; i < students.length; i++) {

                        var mark = students[i].marks;

                        var gradeIndex = getPt3Grade(mark);

                        //console.log(gradeIndex);

                        pt3Grades[gradeIndex]++;

                    }
                }

                var passCount = 0;

                for (var i = 0; i < 4; i++) {
                    passCount += pt3Grades[i];
                }

                var failCount = pt3Grades[4];

                var passPercent = (passCount / (passCount + failCount)) * 100;

                var failPercent = (failCount / (passCount + failCount)) * 100;

                console.log(passPercent);

                var ctx = document.getElementById("myPieChart-" + id).getContext("2d");
                var passFailData = {
                    labels: [
                        "Peratus Lulus",
                        "Peratus Gagal",
                    ],
                    datasets: [{
                        data: [passPercent, failPercent],
                        backgroundColor: [
                            "#63FF84",
                            "#FF6384",
                        ]
                    }]
                };

                var pieChart = new Chart(ctx, {
                    type: 'pie',
                    data: passFailData
                });

            } else if (subjectLevel == "Menengah Atas") {

                var spmGrades = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

                if (students != null) {
                    for (var i = 0; i < students.length; i++) {

                        var mark = students[i].marks;

                        var gradeIndex = getSpmGrade(mark);

                        //console.log(gradeIndex);

                        spmGrades[gradeIndex]++;

                    }
                }

                var passCount = 0;

                for (var i = 0; i < 9; i++) {
                    passCount += spmGrades[i];
                }

                var failCount = spmGrades[9];

                var passPercent = (passCount / (passCount + failCount)) * 100;

                var failPercent = (failCount / (passCount + failCount)) * 100;

                console.log(passPercent);

                var ctx = document.getElementById("myPieChart-" + id).getContext("2d");
                var passFailData = {
                    labels: [
                        "Peratus Lulus",
                        "Peratus Gagal",
                    ],
                    datasets: [{
                        data: [passPercent.toFixed(2), failPercent.toFixed(2)],
                        backgroundColor: [
                            "#63FF84",
                            "#FF6384",
                        ]
                    }]
                };

                var pieChart = new Chart(ctx, {
                    type: 'pie',
                    data: passFailData
                });
            }

        });
}

function getPt3Grade(studentMark) {

    if (!studentMark) {

        return 6;

    } else {

        if (studentMark == "-") {
            return 6;

        } else {

            if (studentMark >= 85 && studentMark <= 100) {
                return 0;
            } else if (studentMark >= 70 && studentMark <= 84) {
                return 1;
            } else if (studentMark >= 60 && studentMark <= 69) {
                return 2;
            } else if (studentMark >= 50 && studentMark <= 59) {
                return 3;
            } else if (studentMark >= 40 && studentMark <= 49) {
                return 4;
            } else if (studentMark >= 0 && studentMark <= 39) {
                return 5;
            } else {
                return 6;
            }
        }

    }
}

function getSpmGrade(mark) {

    if (!mark) {

        return 10;

    } else {

        if (mark == "-") {
            return 10;

        } else {

            if (mark >= 90 && mark <= 100) {
                return 0;
            } else if (mark >= 80 && mark <= 89) {
                return 1;
            } else if (mark >= 70 && mark <= 79) {
                return 2;
            } else if (mark >= 65 && mark <= 69) {
                return 3;
            } else if (mark >= 60 && mark <= 64) {
                return 4;
            } else if (mark >= 55 && mark <= 59) {
                return 5;
            } else if (mark >= 50 && mark <= 54) {
                return 6;
            } else if (mark >= 45 && mark <= 49) {
                return 7;
            } else if (mark >= 40 && mark <= 44) {
                return 8;
            } else if (mark >= 0 && mark <= 39) {
                return 9;
            } else {
                return 10;
            }
        }

    }
}