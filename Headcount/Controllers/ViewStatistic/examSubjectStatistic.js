﻿$(document).ready(function () {

    var examId = $("#examId").val();
    var pageNumber = $("#pageNumber").val();

    var totalRows = 0;
    var pages = 0;




    $.ajax('/Statistic/GetExamSubjectCount/', {
        data: {
        
            'examId': examId,
        },
        success: function (data, status, xhr) {

            totalRows = data.result;

            pages = Math.ceil(totalRows / 10);


            for (var i = 0; i < pages; i++) {
                

                $("#pageNavBtn").append("<button type='button' class=\'btn btn-primary page-btn pure-material-button-contained\' style='margin-right: 5px' id='page" + (i + 1) + "'>" + (i + 1) + "</button>");

                var link = "window.location.href=\'/Statistic/ExamSubjectIndex/" + examId + "?pageNumber=" + (i) + "\';";

                document.getElementById("page" + (i + 1)).setAttribute("onclick", link);

                if (i == pageNumber) {
                    document.getElementById("page" + (i + 1)).setAttribute("style", "margin-right: 5px; background-color: green");
                } else {
                    document.getElementById("page" + (i + 1)).setAttribute("style", "margin-right: 5px;");
                }

            }


        }
    });

});

/*
function searchSubject() {
    //alert('x');
    var query = $("#search").val();
    var examId = $("#examId").val();

    $.post("/Subject/SearchSubjectStatistic", {
            query: query,
            examId: examId
        },
        function(data, status) {
            $('#stat-list').empty();
            var subjects = data;

            //console.log(subjects);

            for (var i = 0; i < subjects.result.length; i++) {

                var layout = '<div class="col-md-6" style="margin-top: 10px"><div id="accordion-' + i + '"><div class="card graph-card"><div class="card-header graph-header" id="headingOne" style="padding-left: 0px; border-radius: 15px;"><h5 class="mb-0"><button type="button" class="btn btn-link btn-text" data-toggle="collapse" data-target="#' + subjects.result[i].SubjectId + '" aria-expanded="true" aria-controls="' + subjects.result[i].SubjectId + '"><i class="fas fa-chart-line" style="font-size:24px; float: left; padding-right: 5px; color: black"></i> ' + subjects.result[i].SubjectName + '(' + subjects.result[i].SubjectCode + ') <i class="fas fa-angle-down" style="font-size:24px; float: right; padding-left: 5px; color: black"></i></button></h5></div><div id="' + subjects.result[i].SubjectId + '" class="collapse" aria-labelledby="headingOne" data-parent="#accordion-' + i + '"><div class="card-body">';

                for (var j = 0; j < 6; j++) {
                    if (subjects.result[i].SubjectLevel == "Menengah Bawah") {
                        if (j == 4 || j == 5) {
                            continue;
                        } else {
                            var name = "";

                            if (j == 0) {
                                name = "peralihan";
                            } else {
                                name = j;
                            }

                            layout += '<div class="row subject-form-card" id="' + subjects.result[i].SubjectId + '-f-' + j + '"><div class="col-md-12"><div class="card graph-card"><div class="card-header graph-header" style="padding-left: 0px; border-radius: 15px;"><h5 class="mb-0"><button type="button" class="btn btn-link btn-text" data-toggle="collapse" data-target="#collapse-' + subjects.result[i].SubjectId + '-f-' + j + '" aria-expanded="true" aria-controls="' + subjects.result[i].SubjectId + '-f-' + j + '" onclick="renderChart(' + subjects.result[i].SubjectId + ',' + j + ')"><i class="fas fa-chart-line" style="font-size:24px; float: left; padding-right: 5px; color: black"></i>Tingkatan ' + name + '<i class="fas fa-angle-down" style="font-size:24px; float: right; padding-left: 5px; color: black"></i></button></h5></div><div class="card-body collapse" data-parent="#' + subjects.result[i].SubjectId + '-f-' + j + '" id="collapse-' + subjects.result[i].SubjectId + '-f-' + j + '"><canvas id="chart-' + subjects.result[i].SubjectId + '-f-' + j + '"></canvas></div></div></div></div>';
                        }
                    } else if (subjects.result[i].SubjectLevel == "Menengah Atas") {
                        if (j == 0 || j == 1 || j == 2 || j == 3) {
                            continue;
                        } else {

                            layout += '<div class="row subject-form-card" id="' + subjects.result[i].SubjectId + '-f-' + j + '"><div class="col-md-12"><div class="card graph-card"><div class="card-header graph-header" style="padding-left: 0px; border-radius: 15px;"><h5 class="mb-0"><button type="button" class="btn btn-link btn-text" data-toggle="collapse" data-target="#collapse-' + subjects.result[i].SubjectId + '-f-' + j + '" aria-expanded="true" aria-controls="' + subjects.result[i].SubjectId + '-f-' + j + '" onclick="renderChart(' + subjects.result[i].SubjectId + ',' + j + ')"><i class="fas fa-chart-line" style="font-size:24px; float: left; padding-right: 5px; color: black"></i>Tingkatan ' + j + '<i class="fas fa-angle-down" style="font-size:24px; float: right; padding-left: 5px; color: black"></i></button></h5></div><div class="card-body collapse" data-parent="#' + subjects.result[i].SubjectId + '-f-' + j + '" id="collapse-' + subjects.result[i].SubjectId + '-f-' + j + '"><canvas id="chart-' + subjects.result[i].SubjectId + '-f-' + j + '"></canvas></div></div></div></div>';
                        }
                    }
                }

                layout += '<div class="row" style="justify-content: flex-end"><button onclick = "window.location.href=\'/Statistic/SubjectClassIndex/' + subjects.result[i].SubjectId + '?examId=' + examId + '\'" class="btn btn-primary btn-sm" style = "border-radius: 15px" >analisis selanjutnya<i class="fas fa-angle-right" style="font-size:24px; float: right; padding-left: 5px; color: white" ></i></button></div></div></div></div></div></div>';

                $('#stat-list').append(layout);

            }
        });
}
*/

function renderChart(id, form) {

    //alert(id);
    var examId = $("#examId").val();
    var examYear = $("#examYear").val();
    var classId = $("#classId").val();

    var formName = "";

    if (form == 0) {
        formName = "peralihan";
    } else {
        formName = form;
    }

    $.post("/Statistic/ExamSubjectTargetStatistic", {
            subjectId: id,
            examYear: examYear,
            form: formName
        },
        function(data, status) {
            //console.log(data.result);

            var gpmp = data.result;

            var json = JSON.parse('[{ "markType": "TOV", "Sasaran_GPMP": ' + gpmp[0] + ', "GPMP": ' + gpmp[0] + '}, { "markType": "OTI1 vs AR1", "Sasaran_GPMP": ' + gpmp[1] + ', "GPMP": ' + gpmp[2] + '}, { "markType": "OTI2 vs AR2", "Sasaran_GPMP": ' + gpmp[3] + ', "GPMP": ' + gpmp[4] + ' }, { "markType": "Akhir Tahun vs ETR", "Sasaran_GPMP": ' + gpmp[5] + ', "GPMP": ' + gpmp[6] + ' }]');



            var labels = json.map(function(e) {
                    return e.markType;
                }),
                Sasaran = json.map(function(e) {
                    return e.Sasaran_GPMP;
                }),
                Markah = json.map(function(e) {
                    return e.GPMP;
                });

            var ctx = document.getElementById("chart-" + id + "-f-" + form).getContext("2d");
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: [{
                        label: "Sasaran GPMP",
                        data: Sasaran,
                        borderWidth: 2,
                        backgroundColor: "rgba(6, 167, 125, 0.1)",
                        borderColor: "rgba(6, 167, 125, 1)",
                        pointBackgroundColor: "rgba(225, 225, 225, 1)",
                        pointBorderColor: "rgba(6, 167, 125, 1)",
                        pointHoverBackgroundColor: "rgba(6, 167, 125, 1)",
                        pointHoverBorderColor: "#fff"
                    }, {
                        label: "GPMP",
                        data: Markah,
                        borderWidth: 2,
                        backgroundColor: "rgba(26, 143, 227, 0.1)",
                        borderColor: "rgba(26, 143, 227, 1)",
                        pointBackgroundColor: "rgba(225, 225, 225, 1)",
                        pointBorderColor: "rgba(26, 143, 227, 1)",
                        pointHoverBackgroundColor: "rgba(26, 143, 227, 1)",
                        pointHoverBorderColor: "#fff"
                    }, ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                stepSize: 1
                            }
                        }]
                    }
                }
            });

        });
}