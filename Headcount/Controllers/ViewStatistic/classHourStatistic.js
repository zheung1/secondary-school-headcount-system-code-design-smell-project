﻿$(document).ready(function() {

    var examId = $("#examId").val();
    var subjectId = $("#subjectId").val();
    var pageNumber = $("#pageNumber").val();

    var totalRows = 0;
    var pages = 0;




    $.ajax('/SubjectStatistic/GetSubjectYearCount/', {
        data: {

            'subjectId': subjectId,
        },
        success: function(data, status, xhr) {

            totalRows = data.result;

            pages = Math.ceil(totalRows / 10);


            for (var i = 0; i < pages; i++) {


                $("#pageNavBtn").append("<button type='button' class=\'btn btn-primary page-btn pure-material-button-contained\' style='margin-right: 5px' id='page" + (i + 1) + "'>" + (i + 1) + "</button>");

                var link = "window.location.href=\'/SubjectStatistic/ClassHourStatistic/" + subjectId + "?examId=" + examId + "&pageNumber=" + (i) + "\';";

                document.getElementById("page" + (i + 1)).setAttribute("onclick", link);

                if (i == pageNumber) {
                    document.getElementById("page" + (i + 1)).setAttribute("style", "margin-right: 5px; background-color: green");
                } else {
                    document.getElementById("page" + (i + 1)).setAttribute("style", "margin-right: 5px;");
                }

            }


        }
    });

});

/*

function searchClass() {
    //alert('x');
    var query = $("#search").val();
    var examId = $("#examId").val();
    var subjectId = $("#subjectId").val();

    $.post("/Class/SearchClassStatistic", {
            query: query,
            examId: examId,
            subjectId: subjectId
        },
        function(data, status) {
            $('#stat-list').empty();
            var classes = data;

            //console.log(subjects);

            for (var i = 0; i < classes.result.length; i++) {

                $('#stat-list').append('<div class="col-md-6" style="margin-top: 10px"> <div id="accordion"> <div class="card graph-card"> <div class="card-header graph-header" id="headingOne" style=\'padding-left: 0px; border-radius: 15px;\'> <h5 class="mb-0"> <button type="button" class="btn btn-link btn-text" data-toggle="collapse" data-target="#' + classes.result[i].ClassId + '" aria-expanded="true" aria-controls="' + classes.result[i].ClassId + '" onclick="renderChart(' + classes.result[i].ClassId + ', \'' + classes.result[i].ClassSubject.SubjectLevel + '\')"> <i class=\'fas fa-chart-line\' style=\'font-size:24px; float: left; padding-right: 5px; color: black\'></i>' + classes.result[i].ClassName + ' <i class=\'fas fa-angle-down\' style=\'font-size:24px; float: right; padding-left: 5px; color: black\'></i> </button> </h5> </div> <div id="' + classes.result[i].ClassId + '" class="collapse" aria-labelledby="headingOne" data-parent="#accordion"> <div class="card-body"> <div class="row"><canvas id="myChart-' + classes.result[i].ClassId + '"></canvas></div> <div class="row" style="justify-content: flex-end"> <button onclick="window.location.href=\'/Statistic/SubjectClassIndex/' + classes.result[i].ClassId + '?examId=' + examId + '\'" class="btn btn-primary btn-sm" style="border-radius: 15px"> analisis selanjutnya <i class=\'fas fa-angle-right\' style=\'font-size:24px; float: right; padding-left: 5px; color: white\'></i> </button> </div> </div> </div> </div> </div> </div>');

                //alert(subjects.result[i].SubjectName);

            }
        });
}
*/

function renderChart(examYear) {

    var subjectId = $("#subjectId").val();

    $.post("/SubjectStatistic/GetClassHourStatistic", {
            examYear: examYear,
            subjectId: subjectId,

        },
        function(data, status) {
            console.log(data.results);

            var classHours = [];
            var avgs = [];

            for (var i = 0; i < data.results.length; i++) {
                avgs.push(data.results[i][0].toFixed(2));
                classHours.push(data.results[i][1]);

            }
            console.log(classHours);
            console.log(avgs);



            var ctx = document.getElementById("chart-" + examYear).getContext('2d');


            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: classHours,
                    datasets: [{
                        label: 'markah purata pelajar', // Name the series
                        data: avgs, // Specify the data values array

                        backgroundColor: "#3e95cd",

                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,

                            }
                        }]
                    }
                }
            });

        });
}