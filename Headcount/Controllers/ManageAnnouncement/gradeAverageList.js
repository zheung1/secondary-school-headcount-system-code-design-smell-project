﻿$(document).ready(function () {
    var table = $('#data-table').DataTable({
        "language": {
            search: 'Cari',
            searchPlaceholder: 'Kata Kunci',
            zeroRecords: "Tiada Rekod",
            oPaginate: {
                "sNext": "seterus",
                "sPrevious": "sebelum"
            },
            "emptyTable": "Tiada data",
            "info": "Paparan dari _START_ hingga _END_ dari _TOTAL_ rekod",
            "infoEmpty": "Paparan 0 hingga 0 dari 0 rekod",
            "infoFiltered": "(Ditapis dari jumlah _MAX_ rekod)",
            "infoThousands": ",",
            "lengthMenu": "Papar _MENU_ rekod",
            "loadingRecords": "Diproses...",
            "processing": "Sedang diproses...",
            "search": "Carian:",
            "zeroRecords": "Tiada padanan rekod yang dijumpai.",
            "paginate": {
                "first": "Pertama",
                "previous": "Sebelum",
                "next": "Seterusnya",
                "last": "Akhir"
            },
            "aria": {
                "sortAscending": ": diaktifkan kepada susunan lajur menaik",
                "sortDescending": ": diaktifkan kepada susunan lajur menurun"
            }

        }
    });

    $('.dataTables_length').addClass('bs-select');

    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss, .overlay').on('click', function () {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });

    $('#data-table_filter input').on('keyup', function () {

        table
            .columns(parseInt($("#column-criteria").val()))
            .search(this.value)
            .draw();
    });

    $('#data-table_filter').prepend("<label>Cari dalam </label><select onclick='clearQuery()' class='drop' id='column-criteria' style='margin-right: 5px;'><option value='0'>Nama Pelajar</option><option value='1'>No IC</option><option value='2'>Kelas</option><option value='3'>Gred Purata (GP)</option><option value='4'>Jumlah MP Diambil</option></select>");


    $('.gp-btn').on('click', function () {

        var studentId = $(this).parent().find("#studentId").val();
        //alert(id);
        //$(".gp-table").empty();

        $.post("/Student/GetStudentSubjectList", {
            id: studentId
        },
            function (data, status) {
                $(".gp-table").find(".subjectRow").remove();

            var subjects = data;

            for (var i = 0; i < subjects.result.length; i++) {

                $(".gp-table").append("<tr class='subjectRow'><td>" + subjects.result[i].SubjectName + "(" + subjects.result[i].SubjectCode+")</td><td>" + subjects.result[i].Marks +"</td></tr>");
            }
        });

    });


});




function clearQuery() {
    var table = $('#data-table').DataTable();
    table
        .search('')
        .columns().search('')
        .draw();
}



