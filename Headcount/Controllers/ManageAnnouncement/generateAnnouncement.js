﻿$(document).ready(function () {

   
    $('#save').on('click', function () {

        $("#error-box").empty();
        var validForm = true;

        var title = $('#title').val();
        var body = $('#body').val();
        
        //form validation
        //check required fields
        if (title == "" || body == "") {
            validForm = false;
            $("#error-box").removeClass("error-box");
            $("#error-box").append("Sila periksa semula ruangan yang wajib diisi *<br>");
        }
        
        
            
        if (validForm) {
            $("#error-box").addClass("error-box");

            //alert('valid form');
            $.post("/Announcement/Save/", {
                title: title,
                body: body,
                
            },
                function (data, status) {
                    alert(data.result);
                    window.location = "/Status/Index/";
                });
        }
    });
});