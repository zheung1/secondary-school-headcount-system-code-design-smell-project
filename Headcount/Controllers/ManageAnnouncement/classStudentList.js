﻿$(document).ready(function() {
    var table = $('#data-table').DataTable({
        "language": {
            search: 'Cari',
            searchPlaceholder: 'Kata Kunci',
            zeroRecords: "Tiada Rekod",
            oPaginate: {
                "sNext": "seterus",
                "sPrevious": "sebelum"
            },
            "emptyTable": "Tiada data",
            "info": "Paparan dari _START_ hingga _END_ dari _TOTAL_ rekod",
            "infoEmpty": "Paparan 0 hingga 0 dari 0 rekod",
            "infoFiltered": "(Ditapis dari jumlah _MAX_ rekod)",
            "infoThousands": ",",
            "lengthMenu": "Papar _MENU_ rekod",
            "loadingRecords": "Diproses...",
            "processing": "Sedang diproses...",
            "search": "Carian:",
            "zeroRecords": "Tiada padanan rekod yang dijumpai.",
            "paginate": {
                "first": "Pertama",
                "previous": "Sebelum",
                "next": "Seterusnya",
                "last": "Akhir"
            },
            "aria": {
                "sortAscending": ": diaktifkan kepada susunan lajur menaik",
                "sortDescending": ": diaktifkan kepada susunan lajur menurun"
            }

        }
    });

    $('.dataTables_length').addClass('bs-select');

    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss, .overlay').on('click', function() {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function() {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });

    $('#data-table_filter input').on('keyup', function() {

        table
            .columns(parseInt($("#column-criteria").val()))
            .search(this.value)
            .draw();
    });

    $('#data-table_filter').prepend("<label>Cari dalam </label><select onclick='clearQuery()' class='drop' id='column-criteria' style='margin-right: 5px;'><option value='0'>Nama Pelajar</option><option value='1'>No IC</option></select>");

    var examId = $("#examId").val();
    var subjectId = $("#subjectId").val();
    var classId = $("#classId").val();

    $('#data-table_filter').append("<div style='display: inline; '><button class='btn btn-primary btn-add' id='bulkAddBtn' onclick='window.location.href=\"/Result/Index/" + classId + "?subjectId=" + subjectId + "&examId=" + examId + "\"'>Urus Markah</button><button class='btn btn-primary btn-add' id='bulkAddBtn' onclick='window.location.href=\"/Target/Index/" + classId + "?subjectId=" + subjectId + "&examId=" + examId + "\"'>Urus Sasaran</button><button class='btn btn-primary btn-add' id='bulkAddBtn' onclick='window.location.href=\"/Student/BulkAddToClass/" + classId + "?subjectId=" + subjectId + "&examId=" + examId + "\"'>Tambah Secara Pukal</button><div class='dropdown' style='display: inline-block;'><button class='btn btn-primary btn-add' type='button' id = 'dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Tambah Pelajar</button><div class='dropdown-menu dropdown-menu-right' style='width: 300px' id='dropMenu' role='menu' aria-labelledby='dropdownMenuButton'><input type='text' id='search' class='form-control' placeholder = 'taip no. ic untuk mencari pelajar' onKeyUp = 'searchStudent()' /><div id='menuOptions'></div><a class='class-btn dropdown-item subject-btn' href='/Class/Index'>Mengurus Senarai Pelajar</a></div></div></div>");

    $('#dropdownMenuButton').on('click', function() {

        //get exam id
        var examId = $("#examId").val();
        var subjectId = $("#subjectId").val();
        var classId = $("#classId").val();

        $.post("/Student/GetStudentList", {
                //id: classId
            },
            function(data, status) {

                $('#menuOptions').empty();

                var students = data;

                //console.log(subjects);

                for (var i = 0; i < students.result.length; i++) {

                    $('#menuOptions').append("<a class='dropdown-item' onclick='addToClass(" + students.result[i].StudentId + ")'>" + students.result[i].StudentIc + "(" + students.result[i].StudentName + ")</a>");
                }
            });

    });


});

function addToClass(studentId) {

    var subjectId = $("#subjectId").val();
    var examId = $("#examId").val();
    var classId = $("#classId").val();

    $.post("/Student/CheckClassStudentExistence", {
            studentId,
            studentId,
            subjectId: subjectId,
            examId: examId,
            classId: classId,
        },
        function(data, status) {
            if (parseInt(data.result) == 1) {
                validForm = false;
                notExisted = false;

                alert("Pelajar sudah terlibat dalam peperiksaan mata pelajaran ini");
            } else {

                $.post("/Student/AddToClass", {
                        studentId: studentId,
                        subjectId: subjectId,
                        examId: examId,
                        classId: classId
                    },
                    function(data, status) {

                        alert(data.result);
                        window.location = "/Student/ClassStudentIndex/" + classId + "?subjectId=" + subjectId + "&examId=" + examId;

                    });

            }


        });



}

function searchStudent() {

    var query = $("#search").val();
    var subjectId = $("#subjectId").val();
    var examId = $("#examId").val();
    var classId = $("#classId").val();

    $.post("/Student/SearchStudent", {
            query: query,
        },
        function(data, status) {

            $('#menuOptions').empty();

            var students = data;

            //console.log(subjects);

            for (var i = 0; i < students.result.length; i++) {

                $('#menuOptions').append("<a class='dropdown-item' onclick='addToClass(" + students.result[i].StudentId + ")'>" + students.result[i].StudentIc + "(" + students.result[i].StudentName + ")</a>");

                //alert(classes.result[i].ClassName);

            }
        });
}

function clearQuery() {
    var table = $('#data-table').DataTable();
    table
        .search('')
        .columns().search('')
        .draw();
}

function deleteItem(classId, subjectId) {
    //alert(id);
    var examId = $("#examId").val();

    var confirmation = confirm("Adakah anda pasti mengeluarkan kelas ini dari mata pelajaran?");
    if (confirmation) {
        $.post("/SubjectClass/RemoveFromSubject/" + classId + "?subjectId=" + subjectId + "&examId=" + examId, {

            },
            function(data, status) {
                alert("Kelas berjaya dikeluarkan dari mata pelajaran");
                window.location = "/SubjectClass/SubjectClassIndex/" + subjectId + "?examId=" + examId;
            });
    }
}