﻿$(document).ready(function () {

    var examId = $("#examId").val();
    var subjectId = $("#subjectId").val();
    var classId = $("#classId").val();

    var table = $('#data-table').DataTable({

        "ajax": {
            "url": "/Student/GetClassStudentIndex/" + classId + "?subjectId=" + subjectId + "&examId=" + examId,
            "type": "POST",
            "datatype": "json",
            "dataSrc": 'students',

        },
        "serverSide": "true",
        "columns": [
            { "data": "StudentName", "name": "StudentName" },
            { "data": "StudentIc", "name": "StudentIc" },
            {
                "mRender": function (data, type, row) {
                    return '<h6><b>Nama Pelajar</b></h6><h6>' + row.StudentName + '</h6><h6><b>No IC</b></h6><h6>' + row.StudentIc + '</h6>';
                }
            },
            {
                "mRender": function (data, type, row) {
                    return '<button class="btn btn-danger btn-sm pure-material-button-contained" style="background-color: red" onclick="deleteItem(' + row.StudentId + ',' + classId + ')"><i class="fas fa-trash" ></i><span style="margin-left: 5px;" class="btn-text">Padam</span></button>';
                }
            }
        ],
        "columnDefs": [
            {
                targets: 0,
                className: 'desktop-column',
            },
            {
                targets: 1,
                className: 'desktop-column',
            },
            {
                targets: 2,
                className: 'mobile-column',
            },
            
        ],

        "order": [0, "asc"],

        "language": {
            search: 'Cari',
            searchPlaceholder: 'Nama Pelajar',
            zeroRecords: "Tiada Rekod",
            oPaginate: {
                "sNext": "seterus",
                "sPrevious": "sebelum"
            },
            "emptyTable": "Tiada data",
            "info": "Paparan dari _START_ hingga _END_ dari _TOTAL_ rekod",
            "infoEmpty": "Paparan 0 hingga 0 dari 0 rekod",
            "infoFiltered": "(Ditapis dari jumlah _MAX_ rekod)",
            "infoThousands": ",",
            "lengthMenu": "Papar _MENU_ rekod",
            "loadingRecords": "Diproses...",
            "processing": "Sedang diproses...",
            "search": "Carian:",
            "zeroRecords": "Tiada padanan rekod yang dijumpai.",
            "paginate": {
                "first": "Pertama",
                "previous": "Sebelum",
                "next": "Seterusnya",
                "last": "Akhir"
            },
            "aria": {
                "sortAscending": ": diaktifkan kepada susunan lajur menaik",
                "sortDescending": ": diaktifkan kepada susunan lajur menurun"
            }

        }

    });


    $('#data-table_filter').append("<div style='display: inline; '><button class='pure-material-button-contained btn btn-primary btn-add' id='bulkAddBtn' onclick='window.location.href=\"/Result/Index/" + classId + "?subjectId=" + subjectId + "&examId=" + examId + "\"'>Urus Markah</button><button class='pure-material-button-contained btn btn-primary btn-add' id='bulkAddBtn' onclick='window.location.href=\"/Target/Index/" + classId + "?subjectId=" + subjectId + "&examId=" + examId + "\"'>Urus Sasaran</button><button class='pure-material-button-contained btn btn-primary btn-add' id='bulkAddBtn' onclick='window.location.href=\"/Student/BulkAddToClass/" + classId + "?subjectId=" + subjectId + "&examId=" + examId + "\"'>Tambah Secara Pukal</button><div class='dropdown' style='display: inline-block;'><button class='pure-material-button-contained btn btn-primary btn-add' type='button' id = 'dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Tambah Pelajar</button><div class='dropdown-menu dropdown-menu-right' style='width: 300px' id='dropMenu' role='menu' aria-labelledby='dropdownMenuButton'><input type='text' id='search' class='form-control' placeholder = 'taip no. ic untuk mencari pelajar' onKeyUp = 'searchStudent()' /><div id='menuOptions'></div><a class='student-btn dropdown-item subject-btn' href='/Student/Index'>Mengendali Pelajar</a></div></div></div>");

    $('#dropdownMenuButton').on('click', function () {

        //get exam id
        var examId = $("#examId").val();
        var subjectId = $("#subjectId").val();
        var classId = $("#classId").val();

        $.post("/Student/GetStudentList", {
            //id: classId
        },
            function (data, status) {

                $('#menuOptions').empty();

                var students = data;

                //console.log(subjects);

                for (var i = 0; i < students.result.length; i++) {

                    $('#menuOptions').append("<a class='dropdown-item' onclick='addToClass(" + students.result[i].StudentId + ")'>" + students.result[i].StudentIc + "(" + students.result[i].StudentName + ")</a>");
                }
            });

    });
});


function addToClass(studentId) {

    var subjectId = $("#subjectId").val();
    var examId = $("#examId").val();
    var classId = $("#classId").val();

    $.post("/Student/CheckClassStudentExistence", {
        studentId, studentId,
        subjectId: subjectId,
        examId: examId,
        classId: classId,
    },
        function (data, status) {
            if (parseInt(data.result) == 1) {
                validForm = false;
                notExisted = false;

                alert("Pelajar sudah terlibat dalam peperiksaan mata pelajaran ini");
            } else {

                $.post("/Student/AddToClass", {
                    studentId: studentId,
                    subjectId: subjectId,
                    examId: examId,
                    classId: classId
                },
                    function (data, status) {

                        alert(data.result);
                        window.location = "/Student/ClassStudentIndex/" + classId + "?subjectId=" + subjectId + "&examId=" + examId;

                    });

            }


        });



}

function searchStudent() {

    var query = $("#search").val();
    var subjectId = $("#subjectId").val();
    var examId = $("#examId").val();
    var classId = $("#classId").val();

    $.post("/Student/SearchStudent", {
        query: query,
    },
        function (data, status) {

            $('#menuOptions').empty();

            var students = data;

            //console.log(subjects);

            for (var i = 0; i < students.result.length; i++) {

                $('#menuOptions').append("<a class='dropdown-item' onclick='addToClass(" + students.result[i].StudentId + ")'>" + students.result[i].StudentIc + "(" + students.result[i].StudentName + ")</a>");

                //alert(classes.result[i].ClassName);

            }
        });
}

function clearQuery() {
    var table = $('#data-table').DataTable();
    table
        .search('')
        .columns().search('')
        .draw();
}

function deleteItem(studentId, classId) {
    //alert(id);
    var examId = $("#examId").val();
    var subjectId = $("#subjectId").val();

    var confirmation = confirm("Adakah anda pasti mengeluarkan pelajar ini dari kelas?");
    if (confirmation) {
        $.post("/Student/RemoveFromClass/" + classId + "?subjectId=" + subjectId + "&examId=" + examId + "&studentId=" + studentId, {

        },
            function (data, status) {
                alert("Pelajar berjaya dikeluarkan dari kelas");
                window.location = "/Student/ClassStudentIndex/" + classId + "?subjectId=" + subjectId + "&examId=" + examId;
            });
    }
}