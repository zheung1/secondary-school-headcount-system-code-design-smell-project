﻿$(document).ready(function () {

    var examId = $("#examId").val();
    var form = $("#form").val();

    var count = 0;

    var table = $('#data-table').DataTable({

        "ajax": {
            "url": "/Student/GetGradeAverageIndex/" + examId + "?form=" + form,
            "type": "POST",
            "datatype": "json",
            "dataSrc": 'students',

        },
        "serverSide": "true",
        "columns": [
            {
               
            },
            { "data": "StudentName", "name": "StudentName" },
            { "data": "StudentIc", "name": "StudentIc" },
            { "data": "StudentClass.ClassName", "name": "ClassName" },
            { "data": "RealMarkGpmp", "name": "RealMarkGpmp" },
            { "data": "EtrMarkGpmp", "name": "EtrMarkGpmp" },
            { "data": "SubjectCount", "name": "SubjectCount" },
            {
                "mRender": function (data, type, row) {
                    return '<h6><b>Nama Pelajar</b></h6><h6>' + row.StudentName + '</h6><h6><b>No IC</b></h6><h6>' + row.StudentIc + '</h6><b>Kelas</b></h6><h6>' + row.StudentClass.ClassName + '</h6><h6><b>Gred Purata (Markah Sebenar)</b></h6><h6>' + row.RealMarkGpmp + '</h6><h6><b>Gred Purata (ETR)</b></h6><h6>' + row.EtrMarkGpmp + '</h6><h6><b>Jumlah MP Diambil</b></h6><h6>' + row.SubjectCount + '</h6>';
                }
            },
            {
                "mRender": function (data, type, row) {
                    //return '<div class="dropdown"><button onclick = "showMarkList(' + row.StudentId + ')" class="btn btn-sm btn-secondary gp-btn" type = "button" id = "gpButton" data-toggle="dropdown" aria- haspopup="true" aria - expanded="false"><i class="fas fa-eye"></i> Info GP Markah</button ><button onclick="showTargetList(' + row.StudentId + ')" class="btn btn-sm btn-secondary gp-btn" type="button" id="gpButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-eye"></i> Info GP ETR</button><div style="max-height: 300px; overflow-y: auto" class="dropdown-menu dropdown-menu-right" aria-labelledby="gpButton"><table class="table gp-table"><tr><th>Mata Pelajaran</th><th>Markah</th></tr></table></div></div>';

                    return '<div class="dropdown"><button class="btn btn-sm btn-secondary btn-drop pure-material-button-contained" type = "button" id = "dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><i class="fas fa-ellipsis-v"></i></button><div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"><button onclick = "showMarkList(' + row.StudentId + ')" class="btn btn-sm btn-secondary gp-btn" type = "button" id = "gpButton" data-toggle="dropdown" aria- haspopup="true" aria - expanded="false"><i class="fas fa-eye"></i> Info GP Markah</button ><button onclick="showTargetList(' + row.StudentId + ')" class="btn btn-sm btn-secondary gp-btn" type="button" id="gpButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-eye"></i> Info GP ETR</button><div style="max-height: 300px; overflow-y: auto" class="dropdown-menu dropdown-menu-right" aria-labelledby="gpButton"><table class="table gp-table"><tr><th>Mata Pelajaran</th><th>Markah</th></tr></table></div></div></div>';
                }
            }
        ],
        "columnDefs": [
            {
                "targets": 0,
                "searchable": false,
                "orderable": false,
                "data": null,
                "title": 'No.',
                "render": function (data, type, full, meta) {
                    return meta.settings._iDisplayStart + meta.row + 1;
                }
            },
            {
                targets: 1,
                className: 'desktop-column',
            },
            {
                targets: 2,
                className: 'desktop-column',
            },
            {
                targets: 3,
                className: 'desktop-column',
            },
            {
                targets: 4,
                className: 'desktop-column',
            },
            {
                targets: 5,
                className: 'desktop-column',
            },
            {
                targets: 6,
                className: 'desktop-column',
            },
            {
                targets: 7,
                className: 'mobile-column',
            },
        ],

        "order": [1, "asc"],

        "language": {
            search: 'Cari',
            searchPlaceholder: 'Nama Pelajar',
            zeroRecords: "Tiada Rekod",
            oPaginate: {
                "sNext": "seterus",
                "sPrevious": "sebelum"
            },
            "emptyTable": "Tiada data",
            "info": "Paparan dari _START_ hingga _END_ dari _TOTAL_ rekod",
            "infoEmpty": "Paparan 0 hingga 0 dari 0 rekod",
            "infoFiltered": "(Ditapis dari jumlah _MAX_ rekod)",
            "infoThousands": ",",
            "lengthMenu": "Papar _MENU_ rekod",
            "loadingRecords": "Diproses...",
            "processing": "Sedang diproses...",
            "search": "Carian:",
            "zeroRecords": "Tiada padanan rekod yang dijumpai.",
            "paginate": {
                "first": "Pertama",
                "previous": "Sebelum",
                "next": "Seterusnya",
                "last": "Akhir"
            },
            "aria": {
                "sortAscending": ": diaktifkan kepada susunan lajur menaik",
                "sortDescending": ": diaktifkan kepada susunan lajur menurun"
            }

        }

    });
});


function showMarkList(studentId) {

    var examId = $("#examId").val();


    $.post("/Student/GetStudentSubjectMarkList", {
        id: studentId,
        examId: examId
    },
        function (data, status) {
            $(".gp-table").find(".subjectRow").remove();

            var subjects = data;

            for (var i = 0; i < subjects.result.length; i++) {

                $(".gp-table").append("<tr class='subjectRow'><td>" + subjects.result[i].SubjectName + "(" + subjects.result[i].SubjectCode + ")</td><td>" + subjects.result[i].Marks + "</td></tr>");
            }
        });

}

function showTargetList(studentId) {

    var examYear = $("#examYear").val();


    $.post("/Student/GetStudentSubjectTargetList", {
        id: studentId,
        examYear: examYear
    },
        function (data, status) {
            $(".gp-table").find(".subjectRow").remove();

            var subjects = data;

            for (var i = 0; i < subjects.result.length; i++) {

                $(".gp-table").append("<tr class='subjectRow'><td>" + subjects.result[i].SubjectName + "(" + subjects.result[i].SubjectCode + ")</td><td>" + subjects.result[i].Marks + "</td></tr>");
            }
        });

}

function clearQuery() {
    var table = $('#data-table').DataTable();
    table
        .search('')
        .columns().search('')
        .draw();
}



