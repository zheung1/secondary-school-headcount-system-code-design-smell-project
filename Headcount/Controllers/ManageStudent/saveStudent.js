﻿$(document).ready(function () {

   
    $('#save').on('click', function () {

        $("#error-box").empty();
        var validForm = true;

        var name = $('#name').val();
        var ic = $('#ic').val();
        
        //form validation
        //check required fields
        if (name == "" || ic == "") {
            validForm = false;
            $("#error-box").removeClass("error-box");
            $("#error-box").append("Sila periksa semula ruangan yang wajib diisi *<br>");
        }
        
        $.post("/Student/CheckStudentExistence", {
            studentIc: ic,
            
        },
        function (data, status) {

            if (parseInt(data.result) == 1) {
                 validForm = false;
                 $("#error-box").removeClass("error-box");
                 $("#error-box").append("Pelajar ini sudah wujud dalam sistem");
            }
            
        if (validForm) {
            $("#error-box").addClass("error-box");

            //alert('valid form');
            $.post("/Student/Save/", {
                studentName: name,
                studentIc: ic,
                
            },
                function (data, status) {
                    alert(data.result);
                    window.location = "/Student/Index/";
                });
        }
        });
         
    });
    
});