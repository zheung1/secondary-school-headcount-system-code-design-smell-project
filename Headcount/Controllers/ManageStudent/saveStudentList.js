﻿$(document).ready(function() {


    var hotElement = document.querySelector('#hot');
    var hotElementContainer = hotElement.parentNode;

    var hotSettings = {
        data: [],
        columns: [
            {
                data: 'name',
                type: 'text'
            },
            {
                data: 'ic',
                type: 'text'
            },
            
        ],
        stretchH: 'all',
        autoWrapRow: true,
        rowHeaders: true,
        colHeaders: [
            'Nama',
            'No. IC',
            
        ],
        contextMenu: true,
        manualRowResize: true,
        manualColumnResize: true,
        language: 'en-US',
        outsideClickDeselects: false 
    }

    var hot = new Handsontable(hotElement, hotSettings);

    hot.alter('insert_row', hot.countRows(), 50);

    

 
    $("#removeRowBtn").click(function () {
        //console.log(hot.getSelected());
        
        var deleteTargets = [];

        for (var i = 0; i < hot.getSelected().length; i++) {

            var startIndex = hot.getSelected()[i][0];
            var numberOfRows = hot.getSelected()[i][2] - hot.getSelected()[i][0] + 1;

            var deleteTarget = [startIndex, numberOfRows];
            deleteTargets.push(deleteTarget);
        }

        hot.alter('remove_row', deleteTargets);
    });

    $("#addRowBtn").click(function () {
        var numRow = $("#rowNumber").val();
        var validForm = true;

        if (isNaN(numRow) || numRow == "") {
            validForm = false;
            $("#error-box").removeClass("error-box");
            $("#error-box").append("Sila masukkan nombor dalam ruangan jumlah pelajar ");
        }

        if (validForm) {
            hot.alter('insert_row', hot.countRows(), numRow);
        }

    });

    $("#save").click(function () {

        //alert("save");

        var students = hot.getData();

        var validList = true;
        //console.log(students);
        var invalidRows = [];

        var validStudents = [];

        for (var i = 0; i < students.length; i++) {
            if ((students[i][0] == null || students[i][0] == "") && (students[i][1] == null || students[i][1] == "")) {

                
                continue;

            }

            if ((students[i][0] == null || students[i][0] == "") && (students[i][1] != null || students[i][1] != "")) {

                validList = false;
                invalidRows.push(i + 1);

            }

            if ((students[i][0] != null || students[i][0] != "") && (students[i][1] == null || students[i][1] == "")) {

                validList = false;
                invalidRows.push(i + 1);

            }

            if ((students[i][0] != null || students[i][0] != "") && (students[i][1] != null || students[i][1] != "")) {

                validStudents.push(students[i]);

            }
        }

        if (validList) {

            //console.log(validStudents);
            
            var studentArray = JSON.stringify({ 'students': validStudents });

            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: '/Student/SaveBulkInsert/',
                data: studentArray,
                success: function (data) {
                    alert(data.result);
                    window.location = "/Student/Index/" 

                    
                }
            })
            
            

            
        } else {
            
            var message = "Sila periksa semula baris ";

            for (var i = 0; i < invalidRows.length; i++) {
                message += invalidRows[i] + ", ";
            }

            message += "Input di baris-baris tersebut mesti lengkap atau dipadam sebelum senarai ini dapat disimpan ke pangkalan data";

            $("#error-box").removeClass("error-box");
            $("#error-box").append(message);
        }

    });

  
});


