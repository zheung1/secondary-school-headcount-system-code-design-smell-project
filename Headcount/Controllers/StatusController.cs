﻿using Headcount.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
    public class StatusController : Controller
    {
        // GET: Status
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult GetExamList()
        {
            Exam exam = new Exam();

            exam.Uuid = (string)Session["uuid"];

            int start = Convert.ToInt32(Request["start"]);

            int length = Convert.ToInt32(Request["length"]);

            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];

            string sortDirection = Request["order[0][dir]"];

            List<Exam> exams = exam.GetExamList(start, length, searchValue, sortColumnName, sortDirection);

            int filteredCount = exam.GetFilteredCount(searchValue);

            int totalCount = exam.GetExamCount();

            return Json(new
            {
                exams,
                recordsTotal = totalCount,
                draw = Request["draw"],
                recordsFiltered = filteredCount
            },
            JsonRequestBehavior.AllowGet
            );

        }

        public ActionResult GetMarkStatusList(int id)
        {
            Exam exam = new Exam();

            exam.Uuid = (string)Session["uuid"];

            int start = Convert.ToInt32(Request["start"]);

            int length = Convert.ToInt32(Request["length"]);

            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];

            string sortDirection = Request["order[0][dir]"];

            List<Class> statuses = exam.GetMarkStatusList(start, length, searchValue, sortColumnName, sortDirection, id);

            Session["statuses"] = statuses;

            int filteredCount = exam.GetFilteredMarkStatusCount(searchValue, id);

            int totalCount = exam.GetMarkStatusCount(id);

            return Json(new
            {
                statuses,
                recordsTotal = totalCount,
                draw = Request["draw"],
                recordsFiltered = filteredCount
            },
            JsonRequestBehavior.AllowGet
            );

        }

        public ActionResult GetTargetStatusList(int id)
        {
            Exam exam = new Exam();

            exam.Uuid = (string)Session["uuid"];

            int start = Convert.ToInt32(Request["start"]);

            int length = Convert.ToInt32(Request["length"]);

            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];

            string sortDirection = Request["order[0][dir]"];

            List<Class> statuses = exam.GetTargetStatusList(start, length, searchValue, sortColumnName, sortDirection, id);

            Session["statuses"] = statuses;

            int filteredCount = exam.GetFilteredTargetStatusCount(searchValue, id);

            int totalCount = exam.GetTargetStatusCount(id);

            return Json(new
            {
                statuses,
                recordsTotal = totalCount,
                draw = Request["draw"],
                recordsFiltered = filteredCount
            },
            JsonRequestBehavior.AllowGet
            );

        }

        public ActionResult Show(int id)
        {
            ViewBag.examId = id;
            
            return View();
        }

        public ActionResult ShowMarks(int id)
        {
            
            ViewBag.examId = id;

            return View();
        }

        public ActionResult ShowTargets(int id)
        {
           

            ViewBag.examId = id;

            return View();
        }

        public ActionResult MakeAnnouncement()
        {
            List<Class> statuses = (List<Class>)Session["statuses"];

            //Session["statuses"] = null;

            ArrayList admins = new ArrayList();
            ArrayList teachers = new ArrayList();

            string admin = "";
            string teacher = "";

            foreach(Class status in statuses)
            {
                admin = status.ClassAdmin.AdminName;
                
                if (!admins.Contains(admin))
                    admins.Add(admin);

                teacher = status.ClassTeacher.TeacherName;

                if (!teachers.Contains(teacher))
                    teachers.Add(teacher);
            }

            ViewBag.admins = admins;
            ViewBag.teachers = teachers;


            return View();
        }

    }
}