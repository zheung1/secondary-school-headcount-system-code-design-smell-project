﻿$(document).ready(function () {

    $('#login-btn').on('click', function () {

        $("#error-box").empty();

        var validForm = true;

        var username = $('#username').val();
        var password = $('#password').val();
        
        //form validation
        //check required fields
        if (username == "" || password == "") {
            validForm = false;
            $("#error-box").removeClass("error-box");
            $("#error-box").append("Sila periksa semula ruangan yang wajib diisi *<br>");
        }

        $.post("/Login/Validate", {
            username: username,
            password: password
        },
            function (data, status) {

                if (parseInt(data.result) == "") {
                    validForm = false;
                    $("#error-box").removeClass("error-box");
                    $("#error-box").append("Nama atau kata laluan tidak betul");
                }

                if (validForm) {
                    $("#error-box").addClass("error-box");

                    var userType = data.result;

                    //alert('valid form');
                    $.post("/Login/StartUserSession/", {
                        username: username,
                        password: password,
                        userType: userType
                    },
                        function (data, status) {
                            //alert(data.udid);
                            //window.location = "/Class/Index/";

                            var type = data.result;

                            if (type == "admin" || type == "teacher") {

                                window.location = "/Home/HomePage/";

                            } else if (type == "guest") {

                                window.location = "/Home/HomePage/";

                            }
                        });
                }
            });
            
    });
    
});