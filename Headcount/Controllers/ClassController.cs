﻿using Headcount.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
    public class ClassController : Controller
    {
        // GET: Class
        public ActionResult Index()
        {
            Class kelas = new Class();

            ArrayList classes = new ArrayList();

            kelas.Uuid = (string)Session["uuid"];

            classes = kelas.GetClassList();

            ViewBag.classes = classes;
            
            return View();
        }

        public ActionResult GetClassIndex()
        {
            Class kelas = new Class();

            kelas.Uuid = (string)Session["uuid"];

            int start = Convert.ToInt32(Request["start"]);

            int length = Convert.ToInt32(Request["length"]);

            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];

            string sortDirection = Request["order[0][dir]"];

            List<Class> classes = kelas.GetClassList(start, length, searchValue, sortColumnName, sortDirection);

            int filteredCount = kelas.GetFilteredCount(searchValue);

            int totalCount = kelas.GetClassCount();

            return Json(new
            {
                classes,
                recordsTotal = totalCount,
                draw = Request["draw"],
                recordsFiltered = filteredCount
            },
            JsonRequestBehavior.AllowGet
            );

        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {

            Class kelas = new Class();

            kelas = kelas.GetClass(id);

            ViewBag.kelas = kelas;

            return View();
        }

        public ActionResult ShowInfo(int id)
        {

            Class kelas = new Class();

            kelas = kelas.GetClass(id);

            ViewBag.kelas = kelas;

            return View();
        }

        public ActionResult SaveEdit(int classId, string className, string classForm, string classType, string classRemarks)
        {
            Class kelas = new Class();

            kelas.ClassId = classId;

            kelas.ClassName = DbConfig.EscapeString(className);

            kelas.ClassForm = DbConfig.EscapeString(classForm);

            kelas.ClassType = DbConfig.EscapeString(classType);

            kelas.ClassRemarks = DbConfig.EscapeString(classRemarks);

            kelas.UpdateClass();

            return Json(new
            {
                result = "Maklumat Kelas berjaya disunting"
            },
            JsonRequestBehavior.AllowGet
            );

        }

        public ActionResult Save(string className, string classType, string classForm, string classRemarks)
        {
            Class kelas = new Class();

            kelas.Uuid = (string)Session["uuid"];

            kelas.ClassName = DbConfig.EscapeString(className);

            kelas.ClassType = DbConfig.EscapeString(classType);

            kelas.ClassForm = DbConfig.EscapeString(classForm);

            kelas.ClassRemarks = DbConfig.EscapeString(classRemarks);

            kelas.SaveClass();

            return Json(new
            {
                result = "Maklumat kelas berjaya disimpan"
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult CheckClassExistence(string className)
        {
            Class kelas = new Class();

            kelas.ClassName = className;

            kelas.Uuid = (string)Session["uuid"];
            
            bool exist = kelas.CheckExistence();

            if (exist)
            {
                return Json(new
                {
                    result = 1
                },
                    JsonRequestBehavior.AllowGet
                );
            }
            else
            {
                return Json(new
                {
                    result = 0
                },
                    JsonRequestBehavior.AllowGet
                );
            }


        }

        public ActionResult SearchClass(string query, int subjectId)
        {
            //get subject level
            Subject subject = new Subject();

            subject = subject.GetSubject(subjectId);

            string subjectLevel = subject.SubjectLevel;

            ArrayList classes = new ArrayList();

            Class kelas = new Class();

            kelas.Uuid = (string)Session["uuid"];

            classes = kelas.SearchClass(subjectLevel, query);

            return Json(new
            {
                result = classes
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult Delete(int id)
        {
            Class kelas = new Class();

            kelas.DeleteClass(id);


            return Json(new
            {
                result = "Maklumat kelas berjaya dipadam"
            },
            JsonRequestBehavior.AllowGet
            );
        }

    }
}