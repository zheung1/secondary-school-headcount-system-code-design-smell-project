﻿using Headcount.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Headcount.Controllers
{
    public class StatisticController : Controller
    {
        // GET: Statistic
        public ActionResult Index()
        {
            /*
            Exam exam = new Exam();

            exam.Uuid = (string)Session["uuid"];
            
            ArrayList exams = exam.GetExamList();

            ViewBag.exams = exams;
            */

            return View();
        }

        public ActionResult GetExamList()
        {
            Exam exam = new Exam();

            exam.Uuid = (string)Session["uuid"];

            int start = Convert.ToInt32(Request["start"]);

            int length = Convert.ToInt32(Request["length"]);

            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];

            string sortDirection = Request["order[0][dir]"];

            List<Exam> exams = exam.GetExamList(start, length, searchValue, sortColumnName, sortDirection);

            int filteredCount = exam.GetFilteredCount(searchValue);

            int totalCount = exam.GetExamCount();

            return Json(new
            {
                exams,
                recordsTotal = totalCount,
                draw = Request["draw"],
                recordsFiltered = filteredCount
            },
            JsonRequestBehavior.AllowGet
            );

        }

        public ActionResult GetExamSubjectCount(int examId)
        {
            ExamSubject examSubject = new ExamSubject();

            examSubject.Uuid = (string)Session["uuid"];

            int examSubjectCount = examSubject.GetExamSubjectCount(examId);

            return Json(new
            {
                result = examSubjectCount
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult ExamSubjectIndex(int id, int pageNumber = 0) //examId
        {
            //get exam info
            Exam exam = new Exam();


            exam = exam.GetExam(id);

            ExamSubject examSubject = new ExamSubject();

            ArrayList subjects = new ArrayList();

            examSubject.Uuid = (string)Session["uuid"];

            //get subject list related to the examination
            subjects = examSubject.GetExamSubjectList(id, pageNumber);

            ViewBag.exam = exam;
            ViewBag.pageNumber = pageNumber;
            ViewBag.subjects = subjects;

            return View();
        }

        public ActionResult GetSubjectClassCount(int subjectId, int examId) //subjectId, examId
        {
            SubjectClass subjectClass = new SubjectClass();

            subjectClass.Uuid = (string)Session["uuid"];

            int subjectClassCount = subjectClass.GetSubjectClassCount(subjectId, examId);

            return Json(new
            {
                result = subjectClassCount
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult SubjectClassIndex(int id, int examId, int pageNumber = 0) //subjectId, examId
        {
            //get subject info
            Subject subject = new Subject();

            subject = subject.GetSubject(id);

            SubjectClass subjectClass = new SubjectClass();

            subjectClass.Uuid = (string)Session["uuid"];

            ArrayList classes = new ArrayList();

            //get class list related to the subject
            classes = subjectClass.GetSubjectClassList(id, examId, pageNumber);

            ViewBag.subject = subject;
            ViewBag.classes = classes;
            ViewBag.examId = examId;
            ViewBag.pageNumber = pageNumber;

            return View();
        }

        public ActionResult GetClassStudentCount(int classId, int subjectId, int examId) //subjectId, examId
        {
            Student student = new Student();

            student.Uuid = (string)Session["uuid"];

            int classStudentCount = student.GetClassStudentCount(classId, subjectId, examId);

            return Json(new
            {
                result = classStudentCount
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult ClassStudentIndex(int id, int subjectId, int examId, int pageNumber = 0) //classId, subjectId, examId
        {
            //get class info
            Class kelas = new Class();

            kelas = kelas.GetClass(id);

            Student student = new Student();

            ArrayList students = new ArrayList();

            Exam exam = new Exam();
            exam = exam.GetExam(examId);
            exam.ExamId = examId;

            Subject subject = new Subject();
            subject = subject.GetSubject(subjectId);

            subject.SubjectId = subjectId;
            subject.SubjectExam = exam;

            kelas.ClassId = id;
            kelas.ClassSubject = subject;

            student.StudentClass = kelas;

            //get class list related to the subject
            students = student.GetClassStudentList(pageNumber);

            foreach (Student studentObj in students)
            {
                Target target = new Target();

                target.TargetClass = kelas;

                string tovYear = "";
                int tovForm = 0;
                string tovMarkType = "";

                if (kelas.ClassForm == "2" || kelas.ClassForm == "3" || kelas.ClassForm == "5")
                {
                    //get previous exam year
                    tovYear = (Convert.ToInt32(exam.ExamYear) - 1).ToString();

                    tovForm = Convert.ToInt32(kelas.ClassForm) - 1;

                    tovMarkType = "Markah Akhir Tahun";
                }
                else if (kelas.ClassForm == "peralihan" || kelas.ClassForm == "1" || kelas.ClassForm == "4")
                {
                    tovYear = exam.ExamYear;
                    tovForm = Convert.ToInt32(kelas.ClassForm);
                    tovMarkType = "AR2";
                }

                target.TovYear = tovYear;
                target.TovForm = tovForm;
                target.TovMarkType = tovMarkType;

                target.ExamYear = exam.ExamYear;
                target.StudentForm = kelas.ClassForm;


                target.StudentId = studentObj.StudentId;
                target = target.GetTarget();

                studentObj.StudentTarget = target;
                student = student.GetStudentMarks(studentObj, target.StudentId, subjectId, examId, kelas.ClassForm);

                int failCounter = 0;

                if (Double.TryParse(student.StudentResult.Ar1, out double ar1) && Double.TryParse(student.StudentTarget.Oti1, out double oti1))
                {
                    if (ar1 <= oti1 && ar1 != 0 && oti1 != 0)
                    {
                        failCounter++;
                    }
                }

                if (Double.TryParse(student.StudentResult.Ar2, out double ar2) && Double.TryParse(student.StudentTarget.Oti2, out double oti2))
                {
                    if (ar2 <= oti2 && ar2 != 0 && oti2 != 0)
                    {
                        failCounter++;
                    }
                }

                if (Double.TryParse(student.StudentResult.FinalMarks, out double finalMarks) && Double.TryParse(student.StudentTarget.Etr, out double etr))
                {
                    if (finalMarks <= etr && finalMarks != 0 && etr != 0)
                    {
                        failCounter++;
                    }
                }

                if (failCounter >= 2)
                {
                    student.NeedAttention = true;
                }
                else
                {
                    student.NeedAttention = false;
                }

            }



            ViewBag.kelas = kelas;
            ViewBag.students = students;
            ViewBag.subject = subject;
            ViewBag.examId = examId;
            ViewBag.pageNumber = pageNumber;

            return View();
        }

        public ActionResult ExamSubjectTargetStatistic(int subjectId, string examYear, string form)
        {
            Subject subject = new Subject();

            subject = subject.GetSubject(subjectId);

            double[] gpmp = new double[7] { 0, 0, 0, 0, 0, 0, 0 };

            string subjectLevel = "";

            if (form == "peralihan" || form == "1" || form == "2" || form == "3")
            {
                subjectLevel = "Menengah Bawah";
            }
            else if (form == "4" || form == "5")
            {
                subjectLevel = "Menengah Atas";
            }

            if (subjectLevel == "Menengah Bawah")
            {
                int[,] gradeCounts = new int[7, 7] {
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                };

                double[,] gradePercentage = new double[7, 7] {
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                };

                int[] total = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

                int[] passes = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

                int[] fails = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

                double[] passPercent = new double[7] { 0, 0, 0, 0, 0, 0, 0 };

                double[] failPercent = new double[7] { 0, 0, 0, 0, 0, 0, 0 };

                string[] grades = { "A", "B", "C",
                                    "D", "E", "F",
                                    "TH"};

                SubjectTarget subjectTarget = new SubjectTarget();
                
                gradeCounts = subjectTarget.GetGradeCounts(subjectId, examYear, form, "Menengah Bawah");

                gradePercentage = subjectTarget.GetGradePercentage(gradeCounts, "Menengah Bawah");

                gpmp = subjectTarget.GetGpmp(gradeCounts, subjectLevel);

                total = subjectTarget.GetTotalStudent(gradeCounts);

                passes = subjectTarget.GetPassNumber(total, gradeCounts, subjectLevel);

                fails = subjectTarget.GetFailNumber(gradeCounts, subjectLevel);

                passPercent = subjectTarget.GetPassPercent(passes, fails);

                failPercent = subjectTarget.GetFailPercent(passes, fails);

                ViewBag.grades = grades;

                ViewBag.gradeCounts = gradeCounts;

                ViewBag.gradePercentage = gradePercentage;

                ViewBag.gpmp = gpmp;

                ViewBag.total = total;

                ViewBag.passes = passes;

                ViewBag.fails = fails;

                ViewBag.passPercent = passPercent;

                ViewBag.failPercent = failPercent;

            }
            else if (subjectLevel == "Menengah Atas")
            {
                int[,] gradeCounts = new int[7, 11] {
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                };

                double[,] gradePercentage = new double[7, 11] {
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                };



                int[] total = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

                int[] passes = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

                int[] fails = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

                double[] passPercent = new double[7] { 0, 0, 0, 0, 0, 0, 0 };

                double[] failPercent = new double[7] { 0, 0, 0, 0, 0, 0, 0 };

                string[] grades = { "A+", "A", "A-",
                                    "B+", "B", "C+",
                                    "C", "D",
                                    "E", "G", "TH"};

                SubjectTarget subjectTarget = new SubjectTarget();

                gradeCounts = subjectTarget.GetGradeCounts(subjectId, examYear, form, "Menengah Atas");

                //gradePercentage = subjectTarget.GetSpmGradePercentage(gradeCounts);

                gradePercentage = subjectTarget.GetGradePercentage(gradeCounts, "Menengah Atas");

                gpmp = subjectTarget.GetGpmp(gradeCounts, subjectLevel);

                total = subjectTarget.GetTotalStudent(gradeCounts);

                passes = subjectTarget.GetPassNumber(total, gradeCounts, subjectLevel);

                fails = subjectTarget.GetFailNumber(gradeCounts, subjectLevel);

                passPercent = subjectTarget.GetPassPercent(passes, fails);

                failPercent = subjectTarget.GetFailPercent(passes, fails);

                ViewBag.grades = grades;

                ViewBag.gradeCounts = gradeCounts;

                ViewBag.gradePercentage = gradePercentage;

                ViewBag.gpmp = gpmp;

                ViewBag.total = total;

                ViewBag.passes = passes;

                ViewBag.fails = fails;

                ViewBag.passPercent = passPercent;

                ViewBag.failPercent = failPercent;

            }

            ViewBag.subject = subject;

            ViewBag.examYear = examYear;

            return Json(new
            {
                result = gpmp
            },
            JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult SubjectClassGradeStatistic(int classId, int subjectId, int examId)
        {
            Class kelas = new Class();

            kelas = kelas.GetClass(classId);

            Result result = new Result();

            ArrayList results = new ArrayList();

            Exam exam = new Exam();
            exam.ExamId = examId;

            Subject subject = new Subject();
            subject.SubjectId = subjectId;
            subject.SubjectExam = exam;

            kelas.ClassId = classId;
            kelas.ClassSubject = subject;

            result.StudentClass = kelas;

            //get class list related to the subject
            results = result.GetClassStudentList();



            return Json(new
            {
                result = results
            },
            JsonRequestBehavior.AllowGet
            );
        }



        public ActionResult StudentMarkTargetStatistic(int studentId, int classId, int subjectId, int examId)
        {

            //get etr, tov, oti1, oti2, etr
            Class kelas = new Class();

            kelas = kelas.GetClass(classId);

            Target target = new Target();

            Exam exam = new Exam();
            exam = exam.GetExam(examId);

            Subject subject = new Subject();
            subject.SubjectId = subjectId;
            subject.SubjectExam = exam;

            kelas = kelas.GetClass(classId);
            kelas.ClassSubject = subject;

            target.TargetClass = kelas;

            string tovYear = "";
            int tovForm = 0;
            string tovMarkType = "";

            //get previous form
            if (kelas.ClassForm == "2" || kelas.ClassForm == "3" || kelas.ClassForm == "5")
            {
                //get previous exam year
                tovYear = (Convert.ToInt32(exam.ExamYear) - 1).ToString();

                tovForm = Convert.ToInt32(kelas.ClassForm) - 1;

                tovMarkType = "Markah Akhir Tahun";
            }
            else if (kelas.ClassForm == "peralihan" || kelas.ClassForm == "1" || kelas.ClassForm == "4")
            {
                tovYear = exam.ExamYear;
                tovForm = Convert.ToInt32(kelas.ClassForm);
                tovMarkType = "AR2";
            }

            target.TovYear = tovYear;
            target.TovForm = tovForm;
            target.TovMarkType = tovMarkType;

            target.ExamYear = exam.ExamYear;
            target.StudentForm = kelas.ClassForm;
            target.StudentId = studentId;

            //get class list related to the subject
            target = target.GetTarget();

            Student student = new Student();

            student.StudentTarget = target;

            student = student.GetStudentMarks(student, studentId, subjectId, examId, kelas.ClassForm);


            return Json(new
            {
                result = student
            },
            JsonRequestBehavior.AllowGet
            );


        }

        public ActionResult StudentGp(int id, int classId, int examId, int subjectId)
        {
            //param: studentId

            //get exam years of student
            Student student = new Student();

            student = student.GetStudent(id);

            student.StudentId = id;

            ArrayList examYears = student.GetExamYears();

            ViewBag.examYears = examYears;

            ViewBag.student = student;

            ViewBag.classId = classId;

            ViewBag.examId = examId;

            ViewBag.subjectId = subjectId;

            return View();
        }


        public ActionResult StudentGpStatistic(string examYear, int studentId)
        {
            Student student = new Student();

            ArrayList gps = new ArrayList();

            gps = student.StudentGpStatistic(examYear, studentId);

            return Json(new
            {
                ar1 = gps[0],
                ar2 = gps[1],
                final = gps[2]
            },
            JsonRequestBehavior.AllowGet
            );
        }

        


    }
        
}