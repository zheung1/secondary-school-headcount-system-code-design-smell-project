using Headcount.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Headcount.Models
{
    public class SubjectClass: Class
    {
        public List<Class> GetSubjectClassIndex(int start, int length, string searchValue, string sortColumnName, string sortDirection, int subjectId, int examId)
        {

            string sql = "SELECT * FROM SubjectClasses sc, ExamSubjects es, Subject s, Class c, Admin a, Teacher t WHERE sc.examSubjectId = es.examSubjectId AND es.subjectId = s.subjectId AND sc.classId = c.classId AND sc.adminId = a.adminId AND sc.teacherId = t.teacherId AND es.subjectId = " + subjectId + " AND examId = '" + examId + "' AND sc.uuid = '" + this.Uuid + "' AND c.className LIKE '%" + searchValue + "%' ORDER BY " + sortColumnName + " " + sortDirection + " OFFSET " + start + " ROWS FETCH NEXT " + length + " ROWS ONLY";

            List<Class> classes = new List<Class>();

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    
                    Class kelas = new Class();

                    kelas.ClassId = Convert.ToInt32(col["classId"]);

                    kelas.ClassName = col["className"].ToString();

                    kelas.ClassType = col["classType"].ToString();

                    kelas.ClassForm = col["classForm"].ToString();

                    Admin admin = new Admin();

                    admin.AdminName = col["adminName"].ToString();

                    kelas.ClassAdmin = admin;

                    Teacher teacher = new Teacher();

                    teacher.TeacherName = col["teacherName"].ToString();

                    kelas.ClassTeacher = teacher;

                    Subject subject = new Subject();

                    subject.SubjectId = subjectId;

                    subject.SubjectName = col["subjectName"].ToString();

                    kelas.ClassSubject = subject;

                    classes.Add(kelas);

                }
            }

            return classes;
        }

        public int GetSubjectClassCount(int subjectId, int examId)
        {
            string sql = "SELECT count(subjectClassId) as classCount FROM SubjectClasses sc, ExamSubjects es, Subject s, Class c, Admin a, Teacher t WHERE sc.examSubjectId = es.examSubjectId AND es.subjectId = s.subjectId AND sc.classId = c.classId AND sc.adminId = a.adminId AND sc.teacherId = t.teacherId AND es.subjectId = " + subjectId + " AND examId = '" + examId + "' AND sc.uuid = '" + this.Uuid + "'";

            int classCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    classCount = Convert.ToInt32(col["classCount"]);
                }
            }

            return classCount;
        }

        public int GetFilteredSubjectClassCount(string searchValue, int subjectId, int examId)
        {
            string sql = "SELECT count(subjectClassId) as classCount FROM SubjectClasses sc, ExamSubjects es, Subject s, Class c, Admin a, Teacher t WHERE sc.examSubjectId = es.examSubjectId AND es.subjectId = s.subjectId AND sc.classId = c.classId AND sc.adminId = a.adminId AND sc.teacherId = t.teacherId AND es.subjectId = " + subjectId + " AND examId = '" + examId + "' AND sc.uuid = '" + this.Uuid + "' AND c.className LIKE '%" + searchValue + "%'";

            int classCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    classCount = Convert.ToInt32(col["classCount"]);
                }
            }

            return classCount;
        }

        public ArrayList GetSubjectClassList(int subjectId, int examId, int pageNumber)
        {
            int offset = pageNumber * 10;

            string sql = "SELECT * FROM SubjectClasses sc, ExamSubjects es, Subject s, Class c, Admin a, Teacher t WHERE sc.examSubjectId = es.examSubjectId AND es.subjectId = s.subjectId AND sc.classId = c.classId AND sc.adminId = a.adminId AND sc.teacherId = t.teacherId AND es.subjectId = " + subjectId + " AND examId = '" + examId + "' AND sc.uuid = '" + this.Uuid + "' ORDER BY c.className OFFSET " + offset + " ROWS FETCH NEXT " + 10 + " ROWS ONLY";

            ArrayList rows = DbConfig.Retrieve(sql);

            ArrayList classes = new ArrayList();

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    Class kelas = new Class();

                    kelas.ClassId = Convert.ToInt32(cols["classId"]);

                    kelas.ClassName = cols["className"].ToString();

                    kelas.ClassType = cols["classType"].ToString();

                    kelas.ClassForm = cols["classForm"].ToString();

                    Admin admin = new Admin();

                    admin.AdminName = cols["adminName"].ToString();

                    kelas.ClassAdmin = admin;

                    Teacher teacher = new Teacher();

                    teacher.TeacherName = cols["teacherName"].ToString();

                    kelas.ClassTeacher = teacher;

                    Subject subject = new Subject();

                    subject.SubjectId = subjectId;

                    subject.SubjectName = cols["subjectName"].ToString();

                    kelas.ClassSubject = subject;

                    classes.Add(kelas);
                }
            }

            return classes;
        }

        public bool CheckSubjectClassExistence()
        {
            string sql = "SELECT * FROM SubjectClasses sc, ExamSubjects es where sc.examSubjectId = es.examSubjectId AND es.subjectId = '" + this.ClassSubject.SubjectId + "' AND classId = '" + this.ClassId + "' AND examId = '" + this.ClassSubject.SubjectExam.ExamId + "' AND sc.uuid = '" + this.Uuid + "'";

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void AddToSubject()
        {
            // get examSubjectId
            string getExamSubjectIdSql = "SELECT * FROM ExamSubjects WHERE examId = '" + this.ClassSubject.SubjectExam.ExamId + "' AND subjectId = '" + this.ClassSubject.SubjectId + "'";

            ArrayList rows = DbConfig.Retrieve(getExamSubjectIdSql);

            int examSubjectId = 0;

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);
                }
            }

            string sql = "";

            if (this.ClassMethod.MethodId == 0)
            {
                sql = "INSERT INTO SubjectClasses (examSubjectId, classId, teacherId, adminId, classHour, methodId, uuid) VALUES ('" + examSubjectId + "', '" + this.ClassId + "', '" + this.ClassTeacher.TeacherId + "' , '" + this.ClassAdmin.AdminId + "', '" + this.ClassHour + "', null, '" + this.Uuid + "')";
            }
            else
            {
                sql = "INSERT INTO SubjectClasses (examSubjectId, classId, teacherId, adminId, classHour, methodId, uuid) VALUES ('" + examSubjectId + "', '" + this.ClassId + "', '" + this.ClassTeacher.TeacherId + "' , '" + this.ClassAdmin.AdminId + "', '" + this.ClassHour + "', '" + this.ClassMethod.MethodId + "', '" + this.Uuid + "')";
            }

            DbConfig.InsertData(sql);

        }

        public SubjectClass GetSubjectClass(int id, int subjectId, int examId)
        {
            // get examSubjectId
            string getExamSubjectIdSql = "SELECT * FROM ExamSubjects WHERE examId = '" + examId + "' AND subjectId = '" + subjectId + "'";

            ArrayList rows = DbConfig.Retrieve(getExamSubjectIdSql);

            int examSubjectId = 0;

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);
                }
            }

            //get subjectClass
            string getSubjectClassSql = "SELECT t.teacherId, t.teacherName, ut.username as teacherUsername, ua.username as adminUsername, a.adminId, a.adminName, sc.subjectClassId, sc.classHour, sc.methodId FROM SubjectClasses sc, Teacher t, Admin a, Users ua, Users ut WHERE sc.teacherId = t.teacherId and sc.adminId = a.adminId and ut.userId = t.userId and ua.userId = a.userId and examSubjectId = '" + examSubjectId + "' and sc.classId = '" + id + "'";

            ArrayList getSubjectClassRows = DbConfig.Retrieve(getSubjectClassSql);

            SubjectClass kelas = new SubjectClass();

            if (getSubjectClassRows != null)
            {
                foreach (IDictionary subjectClassCols in getSubjectClassRows)
                {

                    Teacher teacher = new Teacher();
                    teacher.TeacherId = Convert.ToInt32(subjectClassCols["teacherId"]);
                    teacher.TeacherName = subjectClassCols["teacherName"].ToString();
                    teacher.Username = subjectClassCols["teacherUsername"].ToString();

                    Admin admin = new Admin();
                    admin.AdminId = Convert.ToInt32(subjectClassCols["adminId"]);
                    admin.AdminName = subjectClassCols["adminName"].ToString();
                    admin.Username = subjectClassCols["adminUsername"].ToString();

                    Method method = new Method();


                    if (subjectClassCols["methodId"] is DBNull)
                    {
                        method.MethodId = 0;
                        method.MethodName = "";
                    }
                    else
                    {
                        method.MethodId = Convert.ToInt32(subjectClassCols["methodId"]);

                        string sql = "SELECT * FROM TeachingMethod WHERE methodId = " + method.MethodId;

                        ArrayList methodRows = DbConfig.Retrieve(sql);

                        foreach (IDictionary methodCols in methodRows)
                        {
                            method.MethodName = methodCols["methodName"].ToString();
                        }

                    }


                    Subject subject = new Subject();
                    subject.SubjectId = Convert.ToInt32(subjectClassCols["subjectClassId"]);

                    kelas.ClassTeacher = teacher;
                    kelas.ClassAdmin = admin;
                    kelas.ClassMethod = method;
                    kelas.ClassSubject = subject;

                    if (!(subjectClassCols["classHour"] is DBNull))
                    {
                        kelas.ClassHour = Convert.ToDouble(subjectClassCols["classHour"]);
                    }

                }
            }

            return kelas;
        }

        public void RemoveFromSubject()
        {
            // get examSubjectId
            string getExamSubjectIdSql = "SELECT * FROM ExamSubjects WHERE examId = '" + this.ClassSubject.SubjectExam.ExamId + "' AND subjectId = '" + this.ClassSubject.SubjectId + "'";

            ArrayList rows = DbConfig.Retrieve(getExamSubjectIdSql);

            int examSubjectId = 0;

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);
                }
            }

            //get subjectClassId
            string getSubjectClassId = "SELECT * FROM SubjectClasses where examSubjectId = '" + examSubjectId + "' AND classId = '" + this.ClassId + "'";

            ArrayList subjectClassRows = DbConfig.Retrieve(getSubjectClassId);

            int subjectClassId = 0;

            if (subjectClassRows != null)
            {
                foreach (IDictionary cols in subjectClassRows)
                {
                    subjectClassId = Convert.ToInt32(cols["subjectClassId"]);
                }
            }


            //delete from result
            string deleteResultSql = "DELETE FROM Result WHERE subjectClassId = " + subjectClassId;

            DbConfig.DeleteData(deleteResultSql);

            //delete from subjectClasses
            string deleteSubjectClassSql = "DELETE FROM SubjectClasses where subjectClassId = '" + subjectClassId + "'";

            DbConfig.DeleteData(deleteSubjectClassSql);
        }

        public void SaveChangeAdmin()
        {
            string sql = "";

            if (this.ClassMethod.MethodId == 0)
            {
                sql = "UPDATE SubjectClasses SET adminId = '" + this.ClassAdmin.AdminId + "', teacherId = '" + this.ClassTeacher.TeacherId + "', classHour = '" + this.ClassHour + "', methodId = null WHERE subjectClassId = '" + this.ClassSubject.SubjectId + "'";
            }
            else
            {
                sql = "UPDATE SubjectClasses SET adminId = '" + this.ClassAdmin.AdminId + "', teacherId = '" + this.ClassTeacher.TeacherId + "', classHour = '" + this.ClassHour + "', methodId = '" + this.ClassMethod.MethodId + "' WHERE subjectClassId = '" + this.ClassSubject.SubjectId + "'";
            }

            DbConfig.UpdateData(sql);

        }
    }
}