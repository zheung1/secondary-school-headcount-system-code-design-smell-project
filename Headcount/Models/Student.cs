using Headcount.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Headcount.Models
{
    public class Student
    {
        private int studentId;

        private string studentIc,
            studentName;

        private string realMarkGpmp;

        private string etrGpmp;

        private int subjectCount;

        private Class studentClass;

        private Target studentTarget;

        private Result studentResult;

        private List<Subject>subjects;

        private string uuid;

        private Boolean needAttention;

        public int StudentId { get; set; }
        public string StudentIc { get; set; }
        public string StudentName { get; set; }
        public string RealMarkGpmp { get; set; }
        public string EtrMarkGpmp { get; set; }
        public int SubjectCount { get; set; }
        public Class StudentClass { get; set; }
        public Target StudentTarget { get; set; }
        public Result StudentResult { get; set; }
        public List<Subject> Subjects { get; set; }

        public string Uuid { get; set; }

        public Boolean NeedAttention { get; set; }

        public List<Student> GetStudentList(int start, int length, string searchValue, string sortColumnName, string sortDirection)
        {

            string sql = "SELECT * FROM Student s WHERE s.uuid = '" + this.Uuid + "' AND s.studentName LIKE '%" + searchValue + "%' ORDER BY " + sortColumnName + " " + sortDirection + " OFFSET " + start + " ROWS FETCH NEXT " + length + " ROWS ONLY";

            List<Student> students = new List<Student>();

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {

                    Student student = new Student();

                    student.StudentId = Convert.ToInt32(col["studentId"]);
                    student.StudentName = col["studentName"].ToString();
                    student.StudentIc = col["studentIc"].ToString();

                    students.Add(student);

                }
            }

            return students;
        }

        public int GetStudentCount()
        {
            string sql = "SELECT count(studentId) as studentCount FROM Student s WHERE s.uuid = '" + this.Uuid + "'";

            int studentCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    studentCount = Convert.ToInt32(col["studentCount"]);
                }
            }

            return studentCount;
        }

        public int GetFilteredCount(string searchValue)
        {
            string sql = "SELECT count(studentId) as studentCount FROM Student s WHERE s.uuid = '" + this.Uuid + "' AND s.studentName LIKE '%" + searchValue + "%'";

            int studentCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    studentCount = Convert.ToInt32(col["studentCount"]);
                }
            }

            return studentCount;
        }

        public bool CheckExistence()
        {
            string sql = "SELECT * FROM Student where studentIc = '" + this.StudentIc + "' and uuid = '"+this.Uuid+"'";

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int SaveStudent()
        {
            string sql = "INSERT INTO Student (studentIc, studentName, uuid) Output Inserted.studentId VALUES ('" + this.StudentIc + "','" + this.StudentName + "','" + this.Uuid + "')";

            int studentId = DbConfig.InsertData(sql);

            return studentId;
        }

        public void UpdateStudent()
        {
            string sql = "UPDATE Student SET studentName = '"+ this.StudentName + "' WHERE studentIc = '"+ this.StudentIc + "'";

            DbConfig.UpdateData(sql);
        }

        public ArrayList GetClassStudentList(int pageNumber)
        {
            // get examSubjectId
            string getExamSubjectIdSql = "SELECT * FROM ExamSubjects WHERE examId = '" + this.StudentClass.ClassSubject.SubjectExam.ExamId + "' AND subjectId = '" + this.StudentClass.ClassSubject.SubjectId + "'";

            ArrayList examSubjectRows = DbConfig.Retrieve(getExamSubjectIdSql);

            int examSubjectId = 0;

            if (examSubjectRows != null)
            {
                foreach (IDictionary cols in examSubjectRows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);
                }
            }

            string getSubjectClassIdSql = "SELECT * FROM SubjectClasses WHERE examSubjectId = '" + examSubjectId + "' AND classId = '" + this.StudentClass.ClassId + "'";

            ArrayList subjectClassRows = DbConfig.Retrieve(getSubjectClassIdSql);

            int subjectClassId = 0;

            if (subjectClassRows != null)
            {
                foreach (IDictionary cols in subjectClassRows)
                {
                    subjectClassId = Convert.ToInt32(cols["subjectClassId"]);
                }
            }

            int offset = pageNumber * 10;

            string sql = "SELECT * FROM Result r, Student s where r.studentId = s.studentId AND r.subjectClassId = '" + subjectClassId + "' ORDER BY s.studentName OFFSET " + offset + " ROWS FETCH NEXT " + 10 + " ROWS ONLY";

            ArrayList studentRows = DbConfig.Retrieve(sql);

            ArrayList students = new ArrayList();

            if (studentRows != null)
            {
                foreach (IDictionary cols in studentRows)
                {
                    Student student = new Student();

                    student.StudentId = Convert.ToInt32(cols["studentId"]);
                    student.StudentName = cols["studentName"].ToString();
                    student.StudentIc = cols["studentIc"].ToString();

                    students.Add(student);
                }
            }

            return students;

        }

        public List<Student> GetClassStudentList(int start, int length, string searchValue, string sortColumnName, string sortDirection, int classId, int subjectId, int examId)
        {

            // get examSubjectId
            string getExamSubjectIdSql = "SELECT * FROM ExamSubjects WHERE examId = '" + examId + "' AND subjectId = '" + subjectId + "'";

            ArrayList examSubjectRows = DbConfig.Retrieve(getExamSubjectIdSql);

            int examSubjectId = 0;

            if (examSubjectRows != null)
            {
                foreach (IDictionary cols in examSubjectRows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);
                }
            }

            string getSubjectClassIdSql = "SELECT * FROM SubjectClasses WHERE examSubjectId = '" + examSubjectId + "' AND classId = '" + classId + "'";

            ArrayList subjectClassRows = DbConfig.Retrieve(getSubjectClassIdSql);

            int subjectClassId = 0;

            if (subjectClassRows != null)
            {
                foreach (IDictionary cols in subjectClassRows)
                {
                    subjectClassId = Convert.ToInt32(cols["subjectClassId"]);
                }
            }

            string sql = "SELECT * FROM Result r, Student s where r.studentId = s.studentId AND r.subjectClassId = '" + subjectClassId + "' AND s.uuid = '" + this.Uuid + "' AND s.studentName LIKE '%" + searchValue + "%' ORDER BY " + sortColumnName + " " + sortDirection + " OFFSET " + start + " ROWS FETCH NEXT " + length + " ROWS ONLY";

            ArrayList studentRows = DbConfig.Retrieve(sql);

            List<Student> students = new List<Student>();

            if (studentRows != null)
            {
                foreach (IDictionary cols in studentRows)
                {
                    Student student = new Student();

                    student.StudentId = Convert.ToInt32(cols["studentId"]);
                    student.StudentName = cols["studentName"].ToString();
                    student.StudentIc = cols["studentIc"].ToString();

                    students.Add(student);
                }
            }

            return students;
        }

        public int GetClassStudentCount(int classId, int subjectId, int examId)
        {
            // get examSubjectId
            string getExamSubjectIdSql = "SELECT * FROM ExamSubjects WHERE examId = '" + examId + "' AND subjectId = '" + subjectId + "'";

            ArrayList examSubjectRows = DbConfig.Retrieve(getExamSubjectIdSql);

            int examSubjectId = 0;

            if (examSubjectRows != null)
            {
                foreach (IDictionary cols in examSubjectRows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);
                }
            }

            string getSubjectClassIdSql = "SELECT * FROM SubjectClasses WHERE examSubjectId = '" + examSubjectId + "' AND classId = '" + classId + "'";

            ArrayList subjectClassRows = DbConfig.Retrieve(getSubjectClassIdSql);

            int subjectClassId = 0;

            if (subjectClassRows != null)
            {
                foreach (IDictionary cols in subjectClassRows)
                {
                    subjectClassId = Convert.ToInt32(cols["subjectClassId"]);
                }
            }

            string sql = "SELECT count(s.studentId) as studentCount FROM Result r, Student s where r.studentId = s.studentId AND r.subjectClassId = '" + subjectClassId + "' AND s.uuid = '" + this.Uuid + "'";

            ArrayList studentRows = DbConfig.Retrieve(sql);

            int studentCount = 0;

            if (studentRows != null)
            {
                foreach (IDictionary cols in studentRows)
                {
                    studentCount = Convert.ToInt32(cols["studentCount"]);
                }
            }

            return studentCount;
        }

        public int GetFilteredClassStudentCount(string searchValue, int classId, int subjectId, int examId)
        {
            // get examSubjectId
            string getExamSubjectIdSql = "SELECT * FROM ExamSubjects WHERE examId = '" + examId + "' AND subjectId = '" + subjectId + "'";

            ArrayList examSubjectRows = DbConfig.Retrieve(getExamSubjectIdSql);

            int examSubjectId = 0;

            if (examSubjectRows != null)
            {
                foreach (IDictionary cols in examSubjectRows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);
                }
            }

            string getSubjectClassIdSql = "SELECT * FROM SubjectClasses WHERE examSubjectId = '" + examSubjectId + "' AND classId = '" + classId + "'";

            ArrayList subjectClassRows = DbConfig.Retrieve(getSubjectClassIdSql);

            int subjectClassId = 0;

            if (subjectClassRows != null)
            {
                foreach (IDictionary cols in subjectClassRows)
                {
                    subjectClassId = Convert.ToInt32(cols["subjectClassId"]);
                }
            }

            string sql = "SELECT count(s.studentId) as studentCount FROM Result r, Student s where r.studentId = s.studentId AND r.subjectClassId = '" + subjectClassId + "' AND s.uuid = '" + this.Uuid + "'";

            ArrayList studentRows = DbConfig.Retrieve(sql);

            int studentCount = 0;

            if (studentRows != null)
            {
                foreach (IDictionary cols in studentRows)
                {
                    studentCount = Convert.ToInt32(cols["studentCount"]);
                }
            }

            return studentCount;
        }

        public ArrayList GetStudentList()
        {
            string sql = "SELECT * FROM Student s WHERE s.uuid = '"+this.Uuid+"' ORDER BY s.studentName ASC OFFSET 0 ROWS FETCH NEXT 5 ROWS ONLY";

            ArrayList rows = DbConfig.Retrieve(sql);

            ArrayList students = new ArrayList();

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    Student student = new Student();

                    student.StudentId = Convert.ToInt32(cols["studentId"]);
                    student.StudentIc = cols["studentIc"].ToString();
                    student.StudentName = cols["studentName"].ToString();

                    students.Add(student);
                }
            }

            return students;
        }

        public Student GetStudent(int id)
        {
            string sql = "SELECT * FROM Student WHERE studentId = " + id;

            ArrayList rows = DbConfig.Retrieve(sql);

            Student student = new Student();

            foreach (IDictionary cols in rows)
            {
                student.StudentId = Convert.ToInt32(cols["studentId"]);
                student.StudentName = cols["studentName"].ToString();
                student.StudentIc = cols["studentIc"].ToString();
            }

            return student;
        }



        public ArrayList SearchStudent(string query)
        {
            ArrayList students = new ArrayList();

            string sql = "SELECT * FROM Student WHERE studentIc LIKE '%" + query + "%' AND uuid = '"+this.Uuid+"'";
            

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                int counter = 0;

                foreach (IDictionary cols in rows)
                {
                    //limit 5
                    if (counter > 4)
                    {
                        break;
                    }
                    else
                    {
                        Student student = new Student();

                        student.StudentId = Convert.ToInt32(cols["studentId"]);
                        student.StudentIc = cols["studentIc"].ToString();
                        student.StudentName = cols["studentName"].ToString();

                        students.Add(student);

                        counter++;
                    }

                }
            }

            return students;
        }

        public ArrayList SearchStudentStatistic(string query)
        {
            // get examSubjectId
            string getExamSubjectIdSql = "SELECT * FROM ExamSubjects WHERE examId = '" + this.StudentClass.ClassSubject.SubjectExam.ExamId + "' AND subjectId = '" + this.StudentClass.ClassSubject.SubjectId + "'";

            ArrayList examSubjectRows = DbConfig.Retrieve(getExamSubjectIdSql);

            int examSubjectId = 0;

            if (examSubjectRows != null)
            {
                foreach (IDictionary cols in examSubjectRows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);
                }
            }

            string getSubjectClassIdSql = "SELECT * FROM SubjectClasses WHERE examSubjectId = '" + examSubjectId + "' AND classId = '" + this.StudentClass.ClassId + "'";

            ArrayList subjectClassRows = DbConfig.Retrieve(getSubjectClassIdSql);

            int subjectClassId = 0;

            if (subjectClassRows != null)
            {
                foreach (IDictionary cols in subjectClassRows)
                {
                    subjectClassId = Convert.ToInt32(cols["subjectClassId"]);
                }
            }

            string sql = "SELECT * FROM Result r, Student s where r.studentId = s.studentId AND r.subjectClassId = '" + subjectClassId + "' and s.studentIc LIKE '%" + query + "%'";

            ArrayList studentRows = DbConfig.Retrieve(sql);

            ArrayList students = new ArrayList();

            if (studentRows != null)
            {
                foreach (IDictionary cols in studentRows)
                {
                    Student student = new Student();

                    student.StudentId = Convert.ToInt32(cols["studentId"]);
                    student.StudentName = cols["studentName"].ToString();
                    student.StudentIc = cols["studentIc"].ToString();

                    students.Add(student);
                }
            }

            return students;

        }

        public void AddToClass()
        {
            // get examSubjectId
            string getExamSubjectIdSql = "SELECT * FROM ExamSubjects WHERE examId = '" + this.StudentClass.ClassSubject.SubjectExam.ExamId + "' AND subjectId = '" + this.StudentClass.ClassSubject.SubjectId + "'";

            ArrayList examSubjectRows = DbConfig.Retrieve(getExamSubjectIdSql);

            int examSubjectId = 0;

            if (examSubjectRows != null)
            {
                foreach (IDictionary cols in examSubjectRows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);
                }
            }

            string getSubjectClassIdSql = "SELECT * FROM SubjectClasses WHERE examSubjectId = '" + examSubjectId + "' AND classId = '" +this.StudentClass.ClassId +"'";

            ArrayList subjectClassRows = DbConfig.Retrieve(getSubjectClassIdSql);

            int subjectClassId = 0;

            if (subjectClassRows != null)
            {
                foreach (IDictionary cols in subjectClassRows)
                {
                    subjectClassId = Convert.ToInt32(cols["subjectClassId"]);
                }
            }

            string insertResultSql = "INSERT INTO Result (subjectClassId, studentId) values ('"+subjectClassId+"', '" + this.StudentId + "')";

            DbConfig.InsertData(insertResultSql);

            //studentId, examYear, studentForm, subjectId, tov, oti1, oti2, etr

            //get examYear, studentForm, subjectId

            string sql = "SELECT distinct(examYear), classForm, subjectId FROM Result r, SubjectClasses sc, ExamSubjects es, Exam e, Class c where r.subjectClassId = sc.subjectClassId AND sc.examSubjectId = es.examSubjectId AND e.examId = es.examId and sc.classId = c.classId and e.examId = '" + this.StudentClass.ClassSubject.SubjectExam.ExamId + "' and es.subjectId = '"+ this.StudentClass.ClassSubject.SubjectId + "'";

            ArrayList rows = DbConfig.Retrieve(sql);

            string examYear = "";
            string studentForm = "";
            int subjectId = 0;

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    examYear = cols["examYear"].ToString();
                    studentForm = cols["classForm"].ToString();
                    subjectId = Convert.ToInt32(cols["subjectId"]);
                }
            }

            //insert target if not exist

            string checkTargetExistenceSql = "SELECT * FROM Target WHERE examYear = '"+ examYear + "' AND studentForm = '" + studentForm + "' AND subjectId = '"+ subjectId + "' AND studentId = '"+ this.StudentId + "'";

            ArrayList targetRows = DbConfig.Retrieve(checkTargetExistenceSql);

            if (targetRows == null)
            {
                string insertTargetSql = "INSERT INTO Target (examYear, studentForm, subjectId, studentId) values ('" + examYear + "', '" + studentForm + "', '" + subjectId + "', '" + this.StudentId + "')";

                DbConfig.InsertData(insertTargetSql);
            }


            /*
            string insertTargetSql = "INSERT INTO Target (subjectClassId, studentId) values ('" + subjectClassId + "', '" + this.StudentId + "')";

            DbConfig.InsertData(insertTargetSql);
            */
        }

        public bool CheckClassStudentExistence()
        {

            string sql = "SELECT * FROM Result r, SubjectClasses sc, ExamSubjects es where r.subjectClassId = sc.subjectClassId AND sc.examSubjectId = es.examSubjectId and examId = '" + this.StudentClass.ClassSubject.SubjectExam.ExamId + "' and subjectId = '" + this.StudentClass.ClassSubject.SubjectId + "' and studentId = '" + this.StudentId + "'";

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int GetIdWithIc()
        {
            string sql = "SELECT * FROM Student where studentIc = '"+this.StudentIc+"'";

            ArrayList rows = DbConfig.Retrieve(sql);

            int studentId = 0;

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    studentId = Convert.ToInt32(cols["studentId"]);
                }
            }

            return studentId;
        }

        public void DeleteStudent(int id)
        {
            //delete from target
            string deleteTargetSql = "DELETE FROM Target WHERE studentId = " + id;

            DbConfig.DeleteData(deleteTargetSql);

            //delete from result
            string deleteResultSql = "DELETE FROM Result WHERE studentId = " + id;

            DbConfig.DeleteData(deleteResultSql);

            string sql = "DELETE FROM Student WHERE studentId = " + id;

            DbConfig.DeleteData(sql);
        }

        public ArrayList GetGradeAverageList(int examId)
        {
            //get exam level
            Exam exam = new Exam();

            exam = exam.GetExam(examId);

            string examLevel = exam.ExamLevel;

            string sql = "Select distinct(s.studentId) from Result r, Student s, SubjectClasses sc, Class c, ExamSubjects es, Exam e where r.studentId = s.studentId and r.subjectClassId = sc.subjectClassId and sc.classId = c.classId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and e.examId = '"+examId+"' ORDER BY s.studentId";

            ArrayList rows = DbConfig.Retrieve(sql);

            ArrayList students = new ArrayList();

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    Student student = new Student();

                    student.StudentId = Convert.ToInt32(cols["studentId"]);

                    string studentInfoSql = "Select distinct(s.studentId), s.studentName, s.studentIc, c.className from Result r, Student s, SubjectClasses sc, Class c, ExamSubjects es, Exam e where r.studentId = s.studentId and r.subjectClassId = sc.subjectClassId and sc.classId = c.classId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and e.examId = '"+examId+"' and s.studentId = '"+ student.StudentId + "'";

                    ArrayList studentInfoRows = DbConfig.Retrieve(studentInfoSql);

                    if (studentInfoRows != null)
                    {
                        foreach (IDictionary studentInfoCols in studentInfoRows)
                        {
                            student.StudentIc = studentInfoCols["studentIc"].ToString();
                            student.StudentName = studentInfoCols["studentName"].ToString();

                            Class kelas = new Class();
                            kelas.ClassName = studentInfoCols["className"].ToString();

                            student.StudentClass = kelas;
                        }
                    }
                    

                    List<Subject> subjects = new List<Subject>();

                    //get subject taken by the student from result table
                    string getSubjectsSql = "select * from Result r, SubjectClasses sc, ExamSubjects es, Subject s where r.subjectClassId = sc.subjectClassId and sc.examSubjectId = es.examSubjectId and es.subjectId = s.subjectId and studentId = '" + student.StudentId + "' and examId = '" + exam.ExamId + "'";

                    ArrayList subjectRows = DbConfig.Retrieve(getSubjectsSql);

                    if (subjectRows != null)
                    {
                        foreach (IDictionary subjectCols in subjectRows)
                        {
                            Subject subject = new Subject();
                            subject.SubjectId = Convert.ToInt32(subjectCols["subjectId"]);
                            subject.SubjectName = subjectCols["subjectName"].ToString();
                            subject.Marks = subjectCols["marks"].ToString();

                            string getStudentSubjectEtrSql = "SELECT etr FROM Target WHERE examYear = '" + exam.ExamYear + "' and subjectId = '" + subject.SubjectId + "' and studentId = '" + student.StudentId + "'";

                            ArrayList etrRows = DbConfig.Retrieve(getStudentSubjectEtrSql);

                            if (etrRows != null)
                            {
                                foreach (IDictionary etrCols in etrRows)
                                {
                                    subject.EtrMarks = etrCols["etr"].ToString();
                                }
                            }

                            subjects.Add(subject);
                        }
                    }

                    student.Subjects = subjects;

                    student.SubjectCount = subjects.Count;

                    string actualGpmp = "";

                    actualGpmp = student.GetStudentActualGpmp(subjects, examLevel);

                    if(double.TryParse(actualGpmp, out double realMarkGpmp))
                    {
                        student.RealMarkGpmp = Math.Round(realMarkGpmp, 2).ToString();
                    }
                    else
                    {
                        student.RealMarkGpmp = actualGpmp;
                    }

                    string etrGpmp = "";

                    etrGpmp = student.GetStudentEtrGpmp(subjects, examLevel);

                    if (double.TryParse(etrGpmp, out double targetGpmp))
                    {
                        student.EtrMarkGpmp = Math.Round(targetGpmp, 2).ToString();
                    }
                    else
                    {
                        student.EtrMarkGpmp = etrGpmp;
                    }

                    students.Add(student);

                }
            }

            return students;
        }

        public List<Student> GetGradeAverageList(int start, int length, string searchValue, string sortColumnName, string sortDirection, int examId, string form)
        {

            //get exam level
            Exam exam = new Exam();

            exam = exam.GetExam(examId);

            string examLevel = exam.ExamLevel;

    

            string sql = "Select distinct(s.studentId), s.studentName, s.studentIc, c.className, c.classForm, s.etrMarkGpmp, s.realMarkGpmp, s.subjectCount from Result r, Student s, SubjectClasses sc, Class c, ExamSubjects es, Exam e where r.studentId = s.studentId and r.subjectClassId = sc.subjectClassId and sc.classId = c.classId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and e.examId = '" + examId + "'  and c.classForm = '" + form + "' AND s.etrMarkGpmp != '-' AND s.realMarkGpmp != '-' AND s.studentName LIKE '%" + searchValue + "%' ORDER BY " + sortColumnName + " " + sortDirection + " OFFSET " + start + " ROWS FETCH NEXT " + length + " ROWS ONLY";

            ArrayList rows = DbConfig.Retrieve(sql);

            List<Student> students = new List<Student>();

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    Student student = new Student();

                    student.StudentId = Convert.ToInt32(cols["studentId"]);
                    student.RealMarkGpmp = cols["realMarkGpmp"].ToString();
                    student.EtrMarkGpmp = cols["etrMarkGpmp"].ToString();

                    string studentInfoSql = "Select distinct(s.studentId), s.studentName, s.studentIc, c.className, c.classForm from Result r, Student s, SubjectClasses sc, Class c, ExamSubjects es, Exam e where r.studentId = s.studentId and r.subjectClassId = sc.subjectClassId and sc.classId = c.classId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and e.examId = '" + examId + "' and c.classForm = '" + form + "'  and s.studentId = '" + student.StudentId + "' order by s.studentId asc";

                    ArrayList studentInfoRows = DbConfig.Retrieve(studentInfoSql);

                    if (studentInfoRows != null)
                    {
                        foreach (IDictionary studentInfoCols in studentInfoRows)
                        {
                            student.StudentIc = studentInfoCols["studentIc"].ToString();
                            student.StudentName = studentInfoCols["studentName"].ToString();
                            

                            Class kelas = new Class();
                            kelas.ClassName = studentInfoCols["className"].ToString();

                            student.StudentClass = kelas;
                        }
                    }


                    List<Subject> subjects = new List<Subject>();

                    //get subject taken by the student from result table
                    string getSubjectsSql = "select * from Result r, SubjectClasses sc, ExamSubjects es, Subject s where r.subjectClassId = sc.subjectClassId and sc.examSubjectId = es.examSubjectId and es.subjectId = s.subjectId and studentId = '" + student.StudentId + "' and examId = '" + exam.ExamId + "'";

                    ArrayList subjectRows = DbConfig.Retrieve(getSubjectsSql);

                    if (subjectRows != null)
                    {
                        foreach (IDictionary subjectCols in subjectRows)
                        {
                            Subject subject = new Subject();
                            subject.SubjectId = Convert.ToInt32(subjectCols["subjectId"]);
                            subject.SubjectName = subjectCols["subjectName"].ToString();
                            subject.Marks = subjectCols["marks"].ToString();

                            string getStudentSubjectEtrSql = "SELECT etr FROM Target WHERE examYear = '" + exam.ExamYear + "' and subjectId = '" + subject.SubjectId + "' and studentId = '" + student.StudentId + "'";

                            ArrayList etrRows = DbConfig.Retrieve(getStudentSubjectEtrSql);

                            if (etrRows != null)
                            {
                                foreach (IDictionary etrCols in etrRows)
                                {
                                    subject.EtrMarks = etrCols["etr"].ToString();
                                }
                            }

                            subjects.Add(subject);
                        }
                    }

                    student.Subjects = subjects;

                    student.SubjectCount = subjects.Count;

                   
                    string actualGpmp = "";

                    Boolean addToList = true;

                    actualGpmp = student.GetStudentActualGpmp(subjects, examLevel);

                    if (double.TryParse(actualGpmp, out double realMarkGpmp))
                    {
                        student.RealMarkGpmp = Math.Round(realMarkGpmp, 2).ToString();

                        string updateRealMarkGpmpSql = "UPDATE Student SET realMarkGpmp = '" + student.RealMarkGpmp + "' WHERE studentId = '" + student.StudentId + "'";

                        DbConfig.UpdateData(updateRealMarkGpmpSql);
                    }
                    else
                    {
                        student.RealMarkGpmp = actualGpmp;

                        addToList = false;
                    }

                    string etrGpmp = "";

                    etrGpmp = student.GetStudentEtrGpmp(subjects, examLevel);

                    if (double.TryParse(etrGpmp, out double targetGpmp))
                    {
                        student.EtrMarkGpmp = Math.Round(targetGpmp, 2).ToString();

                        string updateEtrGpmpSql = "UPDATE Student SET etrMarkGpmp = '"+ student.EtrMarkGpmp + "' WHERE studentId = '"+student.StudentId+"'";

                        DbConfig.UpdateData(updateEtrGpmpSql);
                    }
                    else
                    {
                        student.EtrMarkGpmp = etrGpmp;

                        addToList = false;

                    }

                    if (addToList)
                    {
                        students.Add(student);
                    }
                    

                }
            }

            return students;
        }

        public int GetGradeAverageCount(int examId, string form)
        {
            //get exam level
            Exam exam = new Exam();

            exam = exam.GetExam(examId);

            string examLevel = exam.ExamLevel;



            string sql = "Select distinct(s.studentId) from Result r, Student s, SubjectClasses sc, Class c, ExamSubjects es, Exam e where r.studentId = s.studentId and r.subjectClassId = sc.subjectClassId and sc.classId = c.classId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and e.examId = '" + examId + "'  and c.classForm = '" + form + "' AND s.etrMarkGpmp != '-' AND s.realMarkGpmp != '-'";

            ArrayList rows = DbConfig.Retrieve(sql);

            List<Student> students = new List<Student>();

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    Student student = new Student();

                    student.StudentId = Convert.ToInt32(cols["studentId"]);
                    

                    List<Subject> subjects = new List<Subject>();

                    //get subject taken by the student from result table
                    string getSubjectsSql = "select * from Result r, SubjectClasses sc, ExamSubjects es, Subject s where r.subjectClassId = sc.subjectClassId and sc.examSubjectId = es.examSubjectId and es.subjectId = s.subjectId and studentId = '" + student.StudentId + "' and examId = '" + exam.ExamId + "'";

                    ArrayList subjectRows = DbConfig.Retrieve(getSubjectsSql);

                    if (subjectRows != null)
                    {
                        foreach (IDictionary subjectCols in subjectRows)
                        {
                            Subject subject = new Subject();
                            subject.SubjectId = Convert.ToInt32(subjectCols["subjectId"]);
                            subject.SubjectName = subjectCols["subjectName"].ToString();
                            subject.Marks = subjectCols["marks"].ToString();

                            string getStudentSubjectEtrSql = "SELECT etr FROM Target WHERE examYear = '" + exam.ExamYear + "' and subjectId = '" + subject.SubjectId + "' and studentId = '" + student.StudentId + "'";

                            ArrayList etrRows = DbConfig.Retrieve(getStudentSubjectEtrSql);

                            if (etrRows != null)
                            {
                                foreach (IDictionary etrCols in etrRows)
                                {
                                    subject.EtrMarks = etrCols["etr"].ToString();
                                }
                            }

                            subjects.Add(subject);
                        }
                    }

                    student.Subjects = subjects;

                    student.SubjectCount = subjects.Count;


                    string actualGpmp = "";

                    Boolean addToList = true;

                    actualGpmp = student.GetStudentActualGpmp(subjects, examLevel);

                    if (double.TryParse(actualGpmp, out double realMarkGpmp))
                    {
                        student.RealMarkGpmp = Math.Round(realMarkGpmp, 2).ToString();

                        string updateRealMarkGpmpSql = "UPDATE Student SET realMarkGpmp = '" + student.RealMarkGpmp + "' WHERE studentId = '" + student.StudentId + "'";

                        DbConfig.UpdateData(updateRealMarkGpmpSql);
                    }
                    else
                    {
                        student.RealMarkGpmp = actualGpmp;

                        addToList = false;
                    }

                    string etrGpmp = "";

                    etrGpmp = student.GetStudentEtrGpmp(subjects, examLevel);

                    if (double.TryParse(etrGpmp, out double targetGpmp))
                    {
                        student.EtrMarkGpmp = Math.Round(targetGpmp, 2).ToString();

                        string updateEtrGpmpSql = "UPDATE Student SET etrMarkGpmp = '" + student.EtrMarkGpmp + "' WHERE studentId = '" + student.StudentId + "'";

                        DbConfig.UpdateData(updateEtrGpmpSql);
                    }
                    else
                    {
                        student.EtrMarkGpmp = etrGpmp;

                        addToList = false;

                    }

                    if (addToList)
                    {
                        students.Add(student);
                    }


                }
            }

            return students.Count;
        }

        public int GetFilteredGradeAverageCount(string searchValue, int examId, string form)
        {
            //get exam level
            Exam exam = new Exam();

            exam = exam.GetExam(examId);

            string examLevel = exam.ExamLevel;



            string sql = "Select distinct(s.studentId), s.studentName, s.studentIc, c.className, c.classForm, s.etrMarkGpmp, s.realMarkGpmp, s.subjectCount from Result r, Student s, SubjectClasses sc, Class c, ExamSubjects es, Exam e where r.studentId = s.studentId and r.subjectClassId = sc.subjectClassId and sc.classId = c.classId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and e.examId = '" + examId + "'  and c.classForm = '" + form + "' AND s.etrMarkGpmp != '-' AND s.realMarkGpmp != '-' AND s.studentName LIKE '%" + searchValue + "%'";

            ArrayList rows = DbConfig.Retrieve(sql);

            List<Student> students = new List<Student>();

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    Student student = new Student();

                    student.StudentId = Convert.ToInt32(cols["studentId"]);
                    student.RealMarkGpmp = cols["realMarkGpmp"].ToString();
                    student.EtrMarkGpmp = cols["etrMarkGpmp"].ToString();

                    string studentInfoSql = "Select distinct(s.studentId), s.studentName, s.studentIc, c.className, c.classForm from Result r, Student s, SubjectClasses sc, Class c, ExamSubjects es, Exam e where r.studentId = s.studentId and r.subjectClassId = sc.subjectClassId and sc.classId = c.classId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and e.examId = '" + examId + "' and c.classForm = '" + form + "'  and s.studentId = '" + student.StudentId + "' order by s.studentId asc";

                    ArrayList studentInfoRows = DbConfig.Retrieve(studentInfoSql);

                    if (studentInfoRows != null)
                    {
                        foreach (IDictionary studentInfoCols in studentInfoRows)
                        {
                            student.StudentIc = studentInfoCols["studentIc"].ToString();
                            student.StudentName = studentInfoCols["studentName"].ToString();


                            Class kelas = new Class();
                            kelas.ClassName = studentInfoCols["className"].ToString();

                            student.StudentClass = kelas;
                        }
                    }


                    List<Subject> subjects = new List<Subject>();

                    //get subject taken by the student from result table
                    string getSubjectsSql = "select * from Result r, SubjectClasses sc, ExamSubjects es, Subject s where r.subjectClassId = sc.subjectClassId and sc.examSubjectId = es.examSubjectId and es.subjectId = s.subjectId and studentId = '" + student.StudentId + "' and examId = '" + exam.ExamId + "'";

                    ArrayList subjectRows = DbConfig.Retrieve(getSubjectsSql);

                    if (subjectRows != null)
                    {
                        foreach (IDictionary subjectCols in subjectRows)
                        {
                            Subject subject = new Subject();
                            subject.SubjectId = Convert.ToInt32(subjectCols["subjectId"]);
                            subject.SubjectName = subjectCols["subjectName"].ToString();
                            subject.Marks = subjectCols["marks"].ToString();

                            string getStudentSubjectEtrSql = "SELECT etr FROM Target WHERE examYear = '" + exam.ExamYear + "' and subjectId = '" + subject.SubjectId + "' and studentId = '" + student.StudentId + "'";

                            ArrayList etrRows = DbConfig.Retrieve(getStudentSubjectEtrSql);

                            if (etrRows != null)
                            {
                                foreach (IDictionary etrCols in etrRows)
                                {
                                    subject.EtrMarks = etrCols["etr"].ToString();
                                }
                            }

                            subjects.Add(subject);
                        }
                    }

                    student.Subjects = subjects;

                    student.SubjectCount = subjects.Count;


                    string actualGpmp = "";

                    Boolean addToList = true;

                    actualGpmp = student.GetStudentActualGpmp(subjects, examLevel);

                    if (double.TryParse(actualGpmp, out double realMarkGpmp))
                    {
                        student.RealMarkGpmp = Math.Round(realMarkGpmp, 2).ToString();

                        string updateRealMarkGpmpSql = "UPDATE Student SET realMarkGpmp = '" + student.RealMarkGpmp + "' WHERE studentId = '" + student.StudentId + "'";

                        DbConfig.UpdateData(updateRealMarkGpmpSql);
                    }
                    else
                    {
                        student.RealMarkGpmp = actualGpmp;

                        addToList = false;
                    }

                    string etrGpmp = "";

                    etrGpmp = student.GetStudentEtrGpmp(subjects, examLevel);

                    if (double.TryParse(etrGpmp, out double targetGpmp))
                    {
                        student.EtrMarkGpmp = Math.Round(targetGpmp, 2).ToString();

                        string updateEtrGpmpSql = "UPDATE Student SET etrMarkGpmp = '" + student.EtrMarkGpmp + "' WHERE studentId = '" + student.StudentId + "'";

                        DbConfig.UpdateData(updateEtrGpmpSql);
                    }
                    else
                    {
                        student.EtrMarkGpmp = etrGpmp;

                        addToList = false;

                    }

                    if (addToList)
                    {
                        students.Add(student);
                    }


                }
            }

            return students.Count;
        }

        public string GetStudentActualGpmp(List <Subject> subjects, string examLevel)
        {
            string gpmp = "-";

            if (examLevel == "Menengah Bawah")
            {
                double[] gradeCounts = new double[7] { 0, 0, 0, 0, 0, 0, 0 };

                foreach (var subject in subjects)
                {
                    int gradeIndex = Result.GetGrade(subject.Marks, examLevel);

                    gradeCounts[gradeIndex]++;
                }

                double subjectCount = 0;

                
                for(int i=0; i < gradeCounts.Length - 1; i++)
                {
                    subjectCount += gradeCounts[i];
                }

                double sum = 0;

                for(int i=0; i<gradeCounts.Length - 1; i++)
                {
                    sum += gradeCounts[i] * (i + 1);
                }

                if (subjectCount != 0)
                {
                    gpmp = (sum / subjectCount).ToString();
                }
                
                
                    


            }
            else if (examLevel == "Menengah Atas")
            {
                double[] gradeCounts = new double[11] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

                foreach (var subject in subjects)
                {
                    int gradeIndex = Result.GetGrade(subject.Marks, examLevel);

                    gradeCounts[gradeIndex]++;
                }

                double subjectCount = 0;

                for (int i = 0; i < gradeCounts.Length - 1; i++)
                {
                    subjectCount += gradeCounts[i];
                }

                double sum = 0;

                for (int i = 0; i < gradeCounts.Length - 1; i++)
                {
                    sum += gradeCounts[i] * i;
                }

                Console.WriteLine(sum);

                if (subjectCount != 0)
                {
                    gpmp = (sum / subjectCount).ToString();
                }
                
            }
            
            return gpmp;
        }

        public string GetStudentEtrGpmp(List<Subject> subjects, string examLevel)
        {
            string gpmp = "-";

            if (examLevel == "Menengah Bawah")
            {
                double[] gradeCounts = new double[7] { 0, 0, 0, 0, 0, 0, 0};

                foreach (var subject in subjects)
                {
                    int gradeIndex = Result.GetGrade(subject.EtrMarks, examLevel);

                    gradeCounts[gradeIndex]++;
                }

                double subjectCount = 0;


                for (int i = 0; i < gradeCounts.Length - 1; i++)
                {
                    subjectCount += gradeCounts[i];
                }

                double sum = 0;

                for (int i = 0; i < gradeCounts.Length - 1; i++)
                {
                    sum += gradeCounts[i] * (i + 1);
                }

                if (subjectCount != 0)
                {
                    gpmp = (sum / subjectCount).ToString();
                }





            }
            else if (examLevel == "Menengah Atas")
            {
                double[] gradeCounts = new double[11] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

                foreach (var subject in subjects)
                {
                    int gradeIndex = Result.GetGrade(subject.EtrMarks, examLevel);

                    gradeCounts[gradeIndex]++;
                }

                double subjectCount = 0;

                for (int i = 0; i < gradeCounts.Length - 1; i++)
                {
                    subjectCount += gradeCounts[i];
                }

                double sum = 0;

                for (int i = 0; i < gradeCounts.Length - 1; i++)
                {
                    sum += gradeCounts[i] * i;
                }

                if (subjectCount != 0)
                {
                    gpmp = (sum / subjectCount).ToString();
                }

            }

            return gpmp;
        }

        public Student GetStudentMarks(Student student, int studentId, int subjectId, int examId, string form)
        {
            //get exam year
            Exam exam = new Exam();
            exam = exam.GetExam(examId);
            
            //using exam year, subjectId, studentId, markType = ar1
            string ar1Sql = "select * from Result r, SubjectClasses sc, Class c, ExamSubjects es left join Exam e on es.examId = e.examId where r.subjectClassId = sc.subjectClassId and sc.classId = c.classID and sc.examSubjectId = es.examSubjectId and studentId = '" + studentId + "' and subjectId = '" + subjectId + "' and e.examYear = '" + exam.ExamYear + "' and examMarkType = 'AR1' and c.classForm = '" + form + "'";

            ArrayList ar1Rows = DbConfig.Retrieve(ar1Sql);

            Result result = new Result();

            if (ar1Rows != null)
            {
                foreach (IDictionary ar1Cols in ar1Rows)
                {
                    result.Ar1 = ar1Cols["marks"].ToString();
                }
            }

            //using exam year, subjectId, studentId, markType = ar2
            string ar2Sql = "select * from Result r, SubjectClasses sc, Class c, ExamSubjects es left join Exam e on es.examId = e.examId where r.subjectClassId = sc.subjectClassId and sc.classId = c.classID and sc.examSubjectId = es.examSubjectId and studentId = '" + studentId + "' and subjectId = '" + subjectId + "' and e.examYear = '" + exam.ExamYear + "' and examMarkType = 'AR2' and c.classForm = '" + form + "'";

            ArrayList ar2Rows = DbConfig.Retrieve(ar2Sql);

            if (ar2Rows != null)
            {
                foreach (IDictionary ar2Cols in ar2Rows)
                {
                    result.Ar2 = ar2Cols["marks"].ToString();
                }
            }

            //using exam year, subjectId, studentId, markType = final
            
            string finalSql = "select * from Result r, SubjectClasses sc, Class c, ExamSubjects es left join Exam e on es.examId = e.examId where r.subjectClassId = sc.subjectClassId and sc.classId = c.classID and sc.examSubjectId = es.examSubjectId and studentId = '" + studentId + "' and subjectId = '" + subjectId + "' and e.examYear = '" + exam.ExamYear + "' and examMarkType = 'Markah Akhir Tahun' and c.classForm = '"+form+"'";

            ArrayList finalRows = DbConfig.Retrieve(finalSql);

            if (finalRows != null)
            {
                foreach (IDictionary finalCols in finalRows)
                {
                    result.FinalMarks = finalCols["marks"].ToString();
                }
            }

            student.StudentResult = result;

            return student;
        }

        public void RemoveFromClass(Class kelas)
        {
            // get examSubjectId
            string getExamSubjectIdSql = "SELECT * FROM ExamSubjects WHERE examId = '" + kelas.ClassSubject.SubjectExam.ExamId + "' AND subjectId = '" + kelas.ClassSubject.SubjectId + "'";

            ArrayList rows = DbConfig.Retrieve(getExamSubjectIdSql);

            int examSubjectId = 0;

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);
                }
            }

            //get subjectClassId
            string getSubjectClassId = "SELECT * FROM SubjectClasses where examSubjectId = '" + examSubjectId + "' AND classId = '" + kelas.ClassId + "'";

            ArrayList subjectClassRows = DbConfig.Retrieve(getSubjectClassId);

            int subjectClassId = 0;

            if (subjectClassRows != null)
            {
                foreach (IDictionary cols in subjectClassRows)
                {
                    subjectClassId = Convert.ToInt32(cols["subjectClassId"]);
                }
            }


            //delete from result
            string deleteResultSql = "DELETE FROM Result WHERE subjectClassId = '" + subjectClassId + "' and studentId = " + kelas.ClassStudent.StudentId;

            DbConfig.DeleteData(deleteResultSql);

            //delete from target

        }

        public ArrayList GetExamYears()
        {
            string sql = "Select distinct(e.examYear) from Result r, SubjectClasses sc, ExamSubjects es, Exam e where r.subjectClassId = sc.subjectClassId and es.examSubjectId = sc.examSubjectId and es.examId = e.examId and r.studentId = '"+this.StudentId+"' ORDER by e.examYear";

            ArrayList examYears = new ArrayList();            
            
            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    string examYear = cols["examYear"].ToString();

                    examYears.Add(examYear);
                }
            }

            return examYears;
        }

        public ArrayList StudentGpStatistic(string examYear, int studentId)
        {
            string sql = "select * from Result r, SubjectClasses sc, ExamSubjects es, Exam e where r.subjectClassId = sc.subjectClassId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and studentId = '" + studentId + "' and e.examYear = '" + examYear + "'";

            ArrayList rows = DbConfig.Retrieve(sql);

            /*
            double ar1Sum = 0;
            double ar1SubjectCount = 0;
            

            double ar2Sum = 0;
            double ar2SubjectCount = 0;
            double ar2Gp = 0;

            double finalSum = 0;
            double finalSubjectCount = 0;
            double finalGp = 0;
            */

            int[] pt3Ar1GradeCounts = new int[6] { 0, 0, 0, 0, 0, 0};

            int[] pt3Ar2GradeCounts = new int[6] { 0, 0, 0, 0, 0, 0 };

            int[] pt3FinalGradeCounts = new int[6] { 0, 0, 0, 0, 0, 0 };

            int[] spmAr1GradeCounts = new int[11] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            int[] spmAr2GradeCounts = new int[11] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            int[] spmFinalGradeCounts = new int[11] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            string ar1Gp = null;;

            string ar2Gp = null;

            string finalGp = null;

            ArrayList gps = new ArrayList();

            string examLevel = "";

            if (rows != null)
            {

                foreach (IDictionary cols in rows)
                {
                    examLevel = cols["examLevel"].ToString();

                    string marks = cols["marks"].ToString();
                    
                    int gradeIndex = Result.GetGrade(marks, examLevel);

                    string examMarkType = cols["examMarkType"].ToString();

                    if (examLevel == "Menengah Atas")
                    {
                        if (examMarkType == "AR1")
                        {
                            spmAr1GradeCounts[gradeIndex]++;
                        }
                        else if (examMarkType == "AR2")
                        {
                            spmAr2GradeCounts[gradeIndex]++;
                        }
                        else if (examMarkType == "Markah Akhir Tahun")
                        {
                            spmFinalGradeCounts[gradeIndex]++;
                        }
                    }
                    else if (examLevel == "Menengah Bawah")
                    {
                        if (examMarkType == "AR1")
                        {
                            pt3Ar1GradeCounts[gradeIndex]++;
                        }
                        else if (examMarkType == "AR2")
                        {
                            pt3Ar2GradeCounts[gradeIndex]++;
                        }
                        else if (examMarkType == "Markah Akhir Tahun")
                        {
                            pt3FinalGradeCounts[gradeIndex]++;
                        }
                    }
                }

                if(examLevel == "Menengah Bawah")
                {
                    ar1Gp = CalcGradeAverage(pt3Ar1GradeCounts, examLevel);
                    ar2Gp = CalcGradeAverage(pt3Ar2GradeCounts, examLevel);
                    finalGp = CalcGradeAverage(pt3FinalGradeCounts, examLevel);

                }
                else if (examLevel == "Menengah Atas")
                {
                    ar1Gp = CalcGradeAverage(spmAr1GradeCounts, examLevel);
                    ar2Gp = CalcGradeAverage(spmAr2GradeCounts, examLevel);
                    finalGp = CalcGradeAverage(spmFinalGradeCounts, examLevel);
                }

            }

            gps.Add(ar1Gp);
            gps.Add(ar2Gp);
            gps.Add(finalGp);

            return gps;
        }

        public string CalcGradeAverage(int[] gradeCounts, string examLevel)
        {
            string gpmp = null;
            
            if (examLevel == "Menengah Bawah")
            {
                int subjectCount = 0;

                for (int i = 0; i < gradeCounts.Length - 1; i++)
                {
                    subjectCount += gradeCounts[i];
                }

                int sum = 0;

                for (int i = 0; i < gradeCounts.Length - 1; i++)
                {
                    sum += gradeCounts[i] * (i + 1);
                }

                if (subjectCount != 0)
                {
                    gpmp = (sum / subjectCount).ToString();
                }
            }
            else if (examLevel == "Menengah Atas")
            {
                int subjectCount = 0;

                for (int i = 0; i < gradeCounts.Length - 1; i++)
                {
                    subjectCount += gradeCounts[i];
                }

                double sum = 0;

                for (int i = 0; i < gradeCounts.Length - 1; i++)
                {
                    sum += gradeCounts[i] * i;
                }

                if (subjectCount != 0)
                {
                    gpmp = (sum / subjectCount).ToString();
                }
            }

            return gpmp;
        }
    }
}