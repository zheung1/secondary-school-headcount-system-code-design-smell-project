using Headcount.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Headcount.Models
{
    public class Guest : User
    {
        private int guestId;

        private string guestName,
            guestHomeTel,
            guestOfficeTel,
            guestHp,
            guestOccupation,
            guestAddress,
            guestEmail,
            guestRemarks,
            uuid;

        public int GuestId { get; set; }
        public int IsSystemOwner { get; set; }
        public string GuestName { get; set; }

        public string GuestHomeTel { get; set; }
        public string GuestOfficeTel { get; set; }
        public string GuestOccupation { get; set; }
        public string GuestHp { get; set; }
        public string GuestAddress { get; set; }
        public string GuestEmail { get; set; }
        public string GuestRemarks { get; set; }
        public string Uuid { get; set; }

        public List<Guest> GetGuestList(int start, int length, string searchValue, string sortColumnName, string sortDirection)
        {

            string sql = "SELECT * FROM Guest g, Users u WHERE g.userId = u.userId AND g.uuid = '"+this.Uuid+ "' AND guestName LIKE '%" + searchValue + "%' ORDER BY " + sortColumnName + " " + sortDirection + " OFFSET " + start + " ROWS FETCH NEXT " + length + " ROWS ONLY";

            List<Guest> guests = new List<Guest>();

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {

                    Guest guest = new Guest();

                    guest.UserId = Convert.ToInt32(col["userId"]);
                    guest.Username = col["username"].ToString();
                    guest.GuestEmail = col["guestEmail"].ToString();
                    guest.GuestName = col["guestName"].ToString();
                    guest.GuestOfficeTel = col["guestOfficeTel"].ToString();
                    guest.GuestHp = col["guestHp"].ToString();

                    guests.Add(guest);

                }
            }

            return guests;
        }

        public int GetGuestCount()
        {
            string sql = "SELECT count(guestId) as guestCount FROM Users u, Guest g WHERE u.userId = g.userId and g.uuid = '"+this.Uuid+"'";

            int guestCount = 0;
            
            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    guestCount = Convert.ToInt32(col["guestCount"]);
                }
            }

            return guestCount;
        }

        public int GetFilteredCount(string searchValue)
        {
            string sql = "SELECT count(guestId) as guestCount FROM Guest WHERE uuid = '" + this.Uuid + "' AND guestName LIKE '%" + searchValue + "%'";

            int guestCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    guestCount = Convert.ToInt32(col["guestCount"]);
                }
            }

            return guestCount;
        }

        public ArrayList GetGuestList()
        {
            string sql = "SELECT * FROM Users u, Guest g WHERE u.userId = g.userId and g.uuid = '"+this.Uuid+"'";

            ArrayList guests = new ArrayList();

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {

                    Guest guest = new Guest();

                    guest.UserId = Convert.ToInt32(col["userId"]);
                    guest.Username = col["username"].ToString();
                    guest.GuestEmail = col["guestEmail"].ToString();
                    guest.GuestName = col["guestName"].ToString();
                    guest.GuestOfficeTel = col["guestOfficeTel"].ToString();
                    guest.GuestHp = col["guestHp"].ToString();

                    guests.Add(guest);

                }
            }

            return guests;
        }

        public Guest GetGuest(int id)
        {
            string sql = "SELECT * FROM Users u, Guest a WHERE u.userId = a.userId AND u.userId = " + id;

            ArrayList rows = DbConfig.Retrieve(sql);
            
            Guest guest = new Guest();

            foreach (IDictionary cols in rows)
            {
                guest.UserId = Convert.ToInt32(cols["userId"]);
                guest.Username = cols["username"].ToString();
                guest.Password = cols["password"].ToString();
                guest.GuestEmail = cols["guestEmail"].ToString();
                guest.GuestId = Convert.ToInt32(cols["guestId"]);
                guest.GuestName = cols["guestName"].ToString();
                guest.GuestHomeTel = cols["guestHomeTel"].ToString();
                guest.GuestOfficeTel = cols["guestOfficeTel"].ToString();
                guest.GuestHp = cols["guestHp"].ToString();
                guest.GuestOccupation = cols["guestOccupation"].ToString();
                guest.GuestRemarks = cols["guestRemarks"].ToString();
                guest.GuestAddress = cols["guestAddress"].ToString();
            }

            return guest;
        }

        public void UpdateGuest()
        {
            string updateUserSql = "UPDATE Users SET username = '" + this.Username + "', password = '" + this.Password + "' WHERE userId = " + this.UserId;

            DbConfig.UpdateData(updateUserSql);

            string sql = "UPDATE Guest SET guestName = '" + this.GuestName + "', guestEmail = '" + this.GuestEmail + "', guestHomeTel = '" + this.GuestHomeTel + "', guestOfficeTel = '" + this.GuestOfficeTel + "', guestHp = '" + this.GuestHp + "',  guestOccupation = '" + this.guestOccupation + "', guestRemarks = '" + this.GuestRemarks + "', guestAddress = '" + this.GuestAddress + "' WHERE userId = " + this.UserId;

            DbConfig.UpdateData(sql);
        }

        public void SaveGuest()
        {
            string insertUserSql = "INSERT INTO Users (username, password, type, uuid) Output Inserted.userId VALUES ('" + this.Username + "', '" + this.Password + "','" + this.Type + "', '" + this.Uuid + "')";

            int id = DbConfig.InsertData(insertUserSql);

            if (id != 0)
            {
                string insertGuestSql = "INSERT INTO Guest (guestName, guestHomeTel, guestOfficeTel, guestHp, guestOccupation, guestEmail, guestRemarks, guestAddress, userId, uuid) Output Inserted.guestId VALUES ('" + this.GuestName + "', '" + this.GuestHomeTel + "', '" + this.GuestOfficeTel + "', '" + this.GuestHp + "' , '" + this.GuestOccupation + "' , '" + this.GuestEmail + "', '" + this.GuestRemarks + "', '" + this.GuestAddress + "','" + id + "', '" + this.Uuid + "')";

                DbConfig.InsertData(insertGuestSql);
            }
        }

        public void DeleteGuest(int id)
        {
            string deleteGuestSql = "DELETE FROM Guest WHERE userId = " + id;

            DbConfig.DeleteData(deleteGuestSql);

            string deleteUserSql = "DELETE FROM Users WHERE userId = " + id;

            DbConfig.DeleteData(deleteUserSql);
        }

        public ArrayList GetGuestDropdownList()
        {
            ArrayList dropdownOptions = new ArrayList();

            string sql = "SELECT * FROM Users u, Guest a WHERE u.userId = a.userId";

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {

                    DropdownOption dropdownOption = new DropdownOption();

                    dropdownOption.id = Convert.ToInt32(cols["guestId"]);
                    dropdownOption.username = cols["userName"].ToString();
                    dropdownOption.text = cols["fullName"].ToString() + " (" + dropdownOption.username + ")";

                    dropdownOptions.Add(dropdownOption);

                }
            }

            return dropdownOptions;
        }
    }
}