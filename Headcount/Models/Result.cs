using Headcount.Controllers;
using System;
using System.Collections;

namespace Headcount.Models
{
    public class Result
    {
        private int resultId, studentId, subjectClassId;

        private string studentIc,
            studentName;

        private string marks;

        private string ar1, ar2, finalMarks;

        private Class studentClass;

        public int ResultId { get; set; }

        public int StudentId { get; set; }

        public int SubjectClassId { get; set; }
        public string StudentIc { get; set; }
        public string StudentName { get; set; }

        public string Marks { get; set; }
        public string Ar1 { get; set; }
        public string Ar2 { get; set; }
        public string FinalMarks { get; set; }
        public Class StudentClass { get; set; }

        public int GetClassStudentCount()
        {
            // get examSubjectId
            string getExamSubjectIdSql = "SELECT * FROM ExamSubjects WHERE examId = '" + this.StudentClass.ClassSubject.SubjectExam.ExamId + "' AND subjectId = '" + this.StudentClass.ClassSubject.SubjectId + "'";

            ArrayList examSubjectRows = DbConfig.Retrieve(getExamSubjectIdSql);

            int examSubjectId = 0;

            if (examSubjectRows != null)
            {
                foreach (IDictionary cols in examSubjectRows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);
                }
            }

            string getSubjectClassIdSql = "SELECT * FROM SubjectClasses WHERE examSubjectId = '" + examSubjectId + "' AND classId = '" + this.StudentClass.ClassId + "'";

            ArrayList subjectClassRows = DbConfig.Retrieve(getSubjectClassIdSql);

            int subjectClassId = 0;

            if (subjectClassRows != null)
            {
                foreach (IDictionary cols in subjectClassRows)
                {
                    subjectClassId = Convert.ToInt32(cols["subjectClassId"]);
                }
            }

            string sql = "SELECT count(r.studentId) as studentCount FROM Result r, Student s where r.studentId = s.studentId AND r.subjectClassId = '" + subjectClassId + "'";

            ArrayList resultRows = DbConfig.Retrieve(sql);

            int studentCount = 0;

            if (resultRows != null)
            {
                foreach (IDictionary cols in resultRows)
                {
                    studentCount = Convert.ToInt32(cols["studentCount"]);
                }
            }


            return studentCount;

        }


        public ArrayList GetClassStudentList(int pageNumber = 0)
        {
            // get examSubjectId
            string getExamSubjectIdSql = "SELECT * FROM ExamSubjects WHERE examId = '" + this.StudentClass.ClassSubject.SubjectExam.ExamId + "' AND subjectId = '" + this.StudentClass.ClassSubject.SubjectId + "'";

            ArrayList examSubjectRows = DbConfig.Retrieve(getExamSubjectIdSql);

            int examSubjectId = 0;

            if (examSubjectRows != null)
            {
                foreach (IDictionary cols in examSubjectRows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);
                }
            }

            string getSubjectClassIdSql = "SELECT * FROM SubjectClasses WHERE examSubjectId = '" + examSubjectId + "' AND classId = '" + this.StudentClass.ClassId + "'";

            ArrayList subjectClassRows = DbConfig.Retrieve(getSubjectClassIdSql);

            int subjectClassId = 0;

            if (subjectClassRows != null)
            {
                foreach (IDictionary cols in subjectClassRows)
                {
                    subjectClassId = Convert.ToInt32(cols["subjectClassId"]);
                }
            }

            int offset = (pageNumber - 1) * 10;

            string sql = "";
            
            if(pageNumber == 0)
            {
                sql = "SELECT r.subjectClassId, r.studentId, s.studentName, s.studentIc, r.marks FROM Result r, Student s where r.studentId = s.studentId AND r.subjectClassId = '" + subjectClassId + "'";
            }
            else
            {
                sql = "SELECT r.subjectClassId, r.studentId, s.studentName, s.studentIc, r.marks FROM Result r, Student s where r.studentId = s.studentId AND r.subjectClassId = '" + subjectClassId + "' ORDER BY s.studentName ASC OFFSET " + offset + " ROWS FETCH NEXT 10 ROWS ONLY";
            }
            
            

            ArrayList results = DbConfig.Retrieve(sql);

           
            return results;

        }

        public void SaveResultList()
        {
            string sql = "UPDATE Result SET marks = '" + this.Marks + "' WHERE studentId = '" + this.StudentId + "' AND subjectClassId = '"+this.SubjectClassId + "'";

            DbConfig.UpdateData(sql);
        }


        public ArrayList GetTov(string tovMarkType, string tovYear, int tovForm)
        {
            string sql = "select * from result r, student s, SubjectClasses sc, class c, ExamSubjects es, Exam e where r.studentId = s.studentId and r.subjectClassId = sc.subjectClassId and sc.classId = c.classId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and e.examYear = '"+ tovYear + "' and c.classForm = '"+ tovForm + "' and  e.examMarkType = '"+ tovMarkType + "'";

            ArrayList tovs = DbConfig.Retrieve(sql);


            return tovs;
        }

        public static int GetGrade(string mark, string examLevel)
        {

            if (examLevel == "Menengah Bawah")
            {

                if (mark == "" || mark == null)
                {
                    return 6;
                }
                else
                {
                    if (mark == "-")
                    {
                        return 6;
                    }
                    else
                    {
                        try { 
                        double studentMark = Convert.ToDouble(mark);

                            if (studentMark >= 85 && studentMark <= 100)
                            {
                                return 0;
                            }
                            else if (studentMark >= 70 && studentMark <= 84)
                            {
                                return 1;
                            }
                            else if (studentMark >= 60 && studentMark <= 69)
                            {
                                return 2;
                            }
                            else if (studentMark >= 50 && studentMark <= 59)
                            {
                                return 3;
                            }
                            else if (studentMark >= 40 && studentMark <= 49)
                            {
                                return 4;
                            }
                            else if (studentMark >= 0 && studentMark <= 39)
                            {
                                return 5;
                            }
                        }
                        catch(FormatException e)
                        {
                            return 6;
                        }

                        return 6;
                    }
                }
            }
            else if (examLevel == "Menengah Atas")
            {
                if (mark == "" || mark == null)
                {
                    return 10;
                }
                else
                {
                    if (mark == "-")
                    {
                        return 10;
                    }
                    else
                    {
                        try { 
                        double studentMark = Convert.ToDouble(mark);

                        if (studentMark >= 90 && studentMark <= 100)
                        {
                            return 0;
                        }
                        else if (studentMark >= 80 && studentMark <= 89)
                        {
                            return 1;
                        }
                        else if (studentMark >= 70 && studentMark <= 79)
                        {
                            return 2;
                        }
                        else if (studentMark >= 65 && studentMark <= 69)
                        {
                            return 3;
                        }
                        else if (studentMark >= 60 && studentMark <= 64)
                        {
                            return 4;
                        }
                        else if (studentMark >= 55 && studentMark <= 59)
                        {
                            return 5;
                        }
                        else if (studentMark >= 50 && studentMark <= 54)
                        {
                            return 6;
                        }
                        else if (studentMark >= 45 && studentMark <= 49)
                        {
                            return 7;
                        }
                        else if (studentMark >= 40 && studentMark <= 44)
                        {
                            return 8;
                        }
                        else if (studentMark >= 0 && studentMark <= 39)
                        {
                            return 9;
                        }
                        }catch(FormatException e)
                        {
                            return 10;
                           
                        }

                        return 10;
                    }
                }
                
                
            }

            return 10;
        }

        
      

    }


}