using Headcount.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Headcount.Models
{
    public class Class
    {
        private int classId;

        private string className,
            classType,
            classForm,
            classRemarks,
            uuid;

        private double classHour;

        private Admin classAdmin;
        private Teacher classTeacher;
        private Student classStudent;
        private Subject classSubject;
        private Method classMethod;

        public int ClassId { get; set; }
        public double ClassHour { get; set; }
        public string ClassName { get; set; }
        public string ClassType { get; set; }
        public string ClassForm { get; set; }
        public string ClassRemarks { get; set; }
        public string Uuid { get; set; }

        public Admin ClassAdmin { get; set; }
        public Teacher ClassTeacher { get; set; }
        public Student ClassStudent { get; set; }

        public Method ClassMethod { get; set; }
        public Subject ClassSubject { get; set; }

        public List<Class> GetClassList(int start, int length, string searchValue, string sortColumnName, string sortDirection)
        {

            string sql = "SELECT * FROM Class c WHERE c.uuid = '" + this.Uuid + "' AND c.className LIKE '%" + searchValue + "%' ORDER BY " + sortColumnName + " " + sortDirection + " OFFSET " + start + " ROWS FETCH NEXT " + length + " ROWS ONLY";

            List<Class> classes = new List<Class>();

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {

                    Class kelas = new Class();

                    kelas.ClassId = Convert.ToInt32(col["classId"]);
                    kelas.ClassName = col["className"].ToString();
                    kelas.ClassForm = col["classForm"].ToString();
                    kelas.ClassType = col["classType"].ToString();

                    classes.Add(kelas);

                }
            }

            return classes;
        }

        public int GetClassCount()
        {
            string sql = "SELECT count(classId) as classCount FROM Class c WHERE c.uuid = '" + this.Uuid + "'";

            int classCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    classCount = Convert.ToInt32(col["classCount"]);
                }
            }

            return classCount;
        }

        public int GetFilteredCount(string searchValue)
        {
            string sql = "SELECT count(classId) as classCount FROM Class c WHERE c.uuid = '" + this.Uuid + "' AND c.className LIKE '%" + searchValue + "%'";

            int classCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    classCount = Convert.ToInt32(col["classCount"]);
                }
            }

            return classCount;
        }

        public bool CheckExistence()
        {
            string sql = "SELECT * FROM Class where className = '" + this.ClassName + "' AND uuid = '"+this.Uuid+"'";
            
            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SaveClass()
        {
            string sql = "INSERT INTO Class (className, classType, classForm, classRemarks, uuid) Output Inserted.classId VALUES ('" + this.ClassName + "', '" + this.ClassType + "','" + this.ClassForm + "','" + this.ClassRemarks + "', '" + this.Uuid + "')";

            DbConfig.InsertData(sql);

        }

        public Class GetClass(int id)
        {

            string sql = "SELECT * FROM Class WHERE classId = " + id;

            ArrayList rows = DbConfig.Retrieve(sql);

            Class kelas = new Class();

            foreach (IDictionary cols in rows)
            {
                kelas.ClassId = Convert.ToInt32(cols["classId"]);
                kelas.ClassName = cols["className"].ToString();
                kelas.ClassForm = cols["classForm"].ToString();
                kelas.ClassType = cols["classType"].ToString();
                kelas.ClassRemarks = cols["classRemarks"].ToString();
            }

            return kelas;

        }

        public void UpdateClass()
        {
            string sql = "UPDATE Class SET className = '" + this.ClassName + "', classForm = '" + this.ClassForm + "', classType = '" + this.ClassType + "', classRemarks = '" + this.ClassRemarks + "' WHERE classId = " + this.ClassId;

            DbConfig.UpdateData(sql);
        }


        public ArrayList GetClassList()
        {
            string sql = "SELECT * FROM Class where uuid = '"+this.Uuid+"'";

            ArrayList rows = DbConfig.Retrieve(sql);

            ArrayList classes = new ArrayList();

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    Class kelas = new Class();

                    kelas.ClassId = Convert.ToInt32(cols["classId"]);
                    kelas.ClassName = cols["className"].ToString();
                    kelas.ClassForm = cols["classForm"].ToString();
                    kelas.ClassType = cols["classType"].ToString();

                    classes.Add(kelas);
                }
            }

            return classes;
        }

        public ArrayList GetClassList(string examLevel)
        {
            string sql = "";
            
            if(examLevel == "Menengah Bawah")
            {
                sql = "SELECT * FROM Class WHERE classForm = 'peralihan' OR classForm = '1' OR classForm = '2' OR classForm = '3' AND uuid = '"+this.Uuid+"'";
            }
            else
            {
                sql = "SELECT * FROM Class WHERE classForm = '4' OR classForm = '5' AND uuid = '" + this.Uuid + "'";
            }
            
            ArrayList classes = new ArrayList();

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                int counter = 0;

                foreach (IDictionary cols in rows)
                {

                    Class kelas = new Class();

                    kelas.ClassId = Convert.ToInt32(cols["classId"]);
                    kelas.ClassName = cols["className"].ToString();

                    classes.Add(kelas);

                    counter++;

                    //limit 5
                    if (counter > 4)
                    {
                        break;
                    }
                }
            }

            return classes;
        }

        public ArrayList SearchClass(string subjectLevel, string query)
        {
            ArrayList classes = new ArrayList();

            string sql = "";

            if(subjectLevel == "Menengah Bawah")
            {
                sql = "SELECT * FROM Class WHERE className LIKE '%" + query + "%' AND uuid = '"+this.Uuid+"' AND (classForm = 'peralihan' OR classForm = '1' OR classForm = '2' OR classForm = '3')";
            }
            else
            {
                sql = "SELECT * FROM Class WHERE className LIKE '%" + query + "%' AND uuid = '" + this.Uuid + "' AND (classForm = '4' OR classForm = '5')";
            }

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {

                    Class kelas = new Class();

                    kelas.ClassId = Convert.ToInt32(cols["classId"]);
                    kelas.ClassName = cols["className"].ToString();

                    classes.Add(kelas);
                }
            }

            return classes;
        }

        

        public void DeleteClass(int id)
        {
            //get subjectClassId
            string getSubjectClassIdSql = "SELECT * FROM SubjectClasses where classId = '" + id + "'";

            ArrayList subjectClassRows = DbConfig.Retrieve(getSubjectClassIdSql);

            int subjectClassId = 0;

            if (subjectClassRows != null)
            {
                foreach (IDictionary cols in subjectClassRows)
                {
                    subjectClassId = Convert.ToInt32(cols["subjectClassId"]);

                    //delete from result
                    string deleteResultSql = "DELETE FROM Result WHERE subjectClassId = " + subjectClassId;

                    DbConfig.DeleteData(deleteResultSql);

                    //delete from subjectClasses
                    string deleteSubjectClassSql = "DELETE FROM SubjectClasses where subjectClassId = '" + subjectClassId + "'";

                    DbConfig.DeleteData(deleteSubjectClassSql);


                }
            }

           
            //delete class

            string deleteClassSql = "DELETE FROM Class where classId = '"+id+"'";

            DbConfig.DeleteData(deleteClassSql);
        }

    }
}