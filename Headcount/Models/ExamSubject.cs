using Headcount.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Headcount.Models
{
    public class ExamSubject : Subject
    {
        public List<Subject> GetExamSubjectIndex(int start, int length, string searchValue, string sortColumnName, string sortDirection, int examId)
        {

            string sql = "SELECT * FROM ExamSubjects es, Exam e, Subject s, Admin a WHERE es.examId = e.examId AND es.subjectId = s.subjectId AND es.adminId = a.adminId AND es.examId = '" + examId + "' AND es.uuid = '" + this.Uuid + "' AND s.subjectName LIKE '%" + searchValue + "%' ORDER BY " + sortColumnName + " " + sortDirection + " OFFSET " + start + " ROWS FETCH NEXT " + length + " ROWS ONLY";

            List<Subject> subjects = new List<Subject>();

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {

                    Subject subject = new Subject();

                    subject.SubjectId = Convert.ToInt32(col["subjectId"]);
                    subject.SubjectCode = col["subjectCode"].ToString();
                    subject.SubjectName = col["subjectName"].ToString();
                    subject.SubjectType = col["subjectType"].ToString();
                    subject.SubjectLevel = col["subjectLevel"].ToString();

                    Admin admin = new Admin();
                    admin.AdminName = col["adminName"].ToString();

                    subject.SubjectAdmin = admin;

                    Exam exam = new Exam();
                    exam.ExamId = Convert.ToInt32(col["examId"]);
                    exam.ExamYear = col["examYear"].ToString();

                    subject.SubjectExam = exam;

                    subjects.Add(subject);

                }
            }

            return subjects;
        }

        public ArrayList GetExamSubjectList(int examId, int pageNumber)
        {
            int offset = pageNumber * 10;

            string sql = "SELECT * FROM ExamSubjects es, Exam e, Subject s, Admin a WHERE es.examId = e.examId AND es.subjectId = s.subjectId AND es.adminId = a.adminId AND es.examId = '" + examId + "' AND es.uuid = '" + this.Uuid + "' ORDER BY s.subjectName OFFSET " + offset + " ROWS FETCH NEXT " + 10 + " ROWS ONLY";

            ArrayList rows = DbConfig.Retrieve(sql);

            ArrayList subjects = new ArrayList();

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    Subject subject = new Subject();

                    subject.SubjectId = Convert.ToInt32(cols["subjectId"]);
                    subject.SubjectCode = cols["subjectCode"].ToString();
                    subject.SubjectName = cols["subjectName"].ToString();
                    subject.SubjectType = cols["subjectType"].ToString();
                    subject.SubjectLevel = cols["subjectLevel"].ToString();

                    Admin admin = new Admin();
                    admin.AdminName = cols["adminName"].ToString();

                    subject.SubjectAdmin = admin;

                    Exam exam = new Exam();
                    exam.ExamId = Convert.ToInt32(cols["examId"]);
                    exam.ExamYear = cols["examYear"].ToString();

                    subject.SubjectExam = exam;

                    subjects.Add(subject);
                }
            }

            return subjects;
        }

        public int GetExamSubjectCount(int examId)
        {
            string sql = "SELECT count(es.examSubjectId) as subjectCount FROM ExamSubjects es, Exam e, Subject s, Admin a WHERE es.examId = e.examId AND es.subjectId = s.subjectId AND es.adminId = a.adminId AND es.examId = '" + examId + "' AND es.uuid = '" + this.Uuid + "'";

            int subjectCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    subjectCount = Convert.ToInt32(col["subjectCount"]);
                }
            }

            return subjectCount;
        }

        public int GetFilteredExamSubjectCount(string searchValue, int examId)
        {
            string sql = "SELECT count(es.examSubjectId) as subjectCount FROM ExamSubjects es, Exam e, Subject s, Admin a WHERE es.examId = e.examId AND es.subjectId = s.subjectId AND es.adminId = a.adminId AND es.examId = '" + examId + "' AND es.uuid = '" + this.Uuid + "' AND s.subjectName LIKE '%" + searchValue + "%'";

            int subjectCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    subjectCount = Convert.ToInt32(col["subjectCount"]);
                }
            }

            return subjectCount;
        }

        public bool CheckExamSubjectExistence()
        {
            string sql = "SELECT * FROM ExamSubjects where subjectId = '" + this.SubjectId + "' AND examId = '" + this.SubjectExam.ExamId + "' AND uuid = '" + this.Uuid + "'";

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Admin GetSubjectAdmin(int subjectId, int examId)
        {
            string sql = "SELECT * FROM ExamSubjects e, Admin a, Users u where e.adminId = a.adminId AND a.userId = u.userId AND subjectId = '" + subjectId + "' AND examId = '" + examId + "'";

            ArrayList rows = DbConfig.Retrieve(sql);

            Admin admin = new Admin();

            if (rows != null)
            {

                foreach (IDictionary cols in rows)
                {
                    admin.AdminId = Convert.ToInt32(cols["adminId"]);

                    admin.AdminName = cols["adminName"].ToString();

                    admin.Username = cols["username"].ToString();


                }
            }

            return admin;
        }

        public void SaveChangeAdmin()
        {
            string sql = "UPDATE ExamSubjects SET adminId = '" + this.SubjectAdmin.AdminId + "' WHERE examId = '" + this.SubjectExam.ExamId + "' AND subjectId = '" + this.SubjectId + "'";

            DbConfig.UpdateData(sql);
        }

        public void AddToExam()
        {
            string sql = "INSERT INTO ExamSubjects (examId, subjectId, adminId, uuid) VALUES ('" + this.SubjectExam.ExamId + "', '" + this.SubjectId + "', '" + this.SubjectAdmin.AdminId + "', '" + this.Uuid + "')";

            DbConfig.InsertData(sql);

        }

        public void RemoveFromExam()
        {
            //get examSubjectId
            string getExamSubjectId = "SELECT * FROM ExamSubjects where examId = '" + this.SubjectExam.ExamId + "' AND subjectId = '" + this.SubjectId + "'";

            ArrayList examSubjectRows = DbConfig.Retrieve(getExamSubjectId);

            int examSubjectId = 0;

            if (examSubjectRows != null)
            {
                foreach (IDictionary cols in examSubjectRows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);

                }
            }

            //get subjectClassIds with examSubjectId
            string getSubjectClassId = "SELECT * FROM SubjectClasses where examSubjectId = '" + examSubjectId + "'";

            ArrayList subjectClassRows = DbConfig.Retrieve(getSubjectClassId);

            int subjectClassId = 0;

            if (subjectClassRows != null)
            {
                foreach (IDictionary cols in subjectClassRows)
                {
                    subjectClassId = Convert.ToInt32(cols["subjectClassId"]);

                    //delete from result
                    string deleteResultSql = "DELETE FROM Result WHERE subjectClassId = " + subjectClassId;

                    DbConfig.DeleteData(deleteResultSql);

                    //delete from subjectClasses
                    string deleteSubjectClassSql = "DELETE FROM SubjectClasses where subjectClassId = '" + subjectClassId + "'";

                    DbConfig.DeleteData(deleteSubjectClassSql);
                }
            }

            //remove examSubjectId

            string sql = "DELETE FROM ExamSubjects where examSubjectId = '" + examSubjectId + "'";

            DbConfig.DeleteData(sql);
        }

    }
}