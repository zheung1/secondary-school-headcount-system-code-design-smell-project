using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Headcount.Models
{
    public class Constants
    {
        public static string[] months = { "Januari", "Februari", "Mac", "April", "Mei", "Jun",
                            "Julai", "Ogos", "September", "Oktober", "November", "Disember"};

    }

    public class DropdownOption
    {
        public int id;
        public string text;
        public string username;
    }
}