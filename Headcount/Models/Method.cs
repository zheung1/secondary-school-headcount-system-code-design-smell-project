using Headcount.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Headcount.Models
{
    public class Method
    {
        private int methodId;
        private string methodName;
        private string uuid;

        public int MethodId { get; set; }
        public string MethodName { get; set; }
        public string Uuid { get; set; }

        public List<Method> GetMethodList(int start, int length, string searchValue, string sortColumnName, string sortDirection)
        {

            string sql = "SELECT * FROM TeachingMethod m WHERE m.uuid = '" + this.Uuid + "' AND m.methodName LIKE '%" + searchValue + "%' ORDER BY " + sortColumnName + " " + sortDirection + " OFFSET " + start + " ROWS FETCH NEXT " + length + " ROWS ONLY";

            List<Method> methods = new List<Method>();

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {

                    Method method = new Method();

                    method.MethodId = Convert.ToInt32(col["methodId"]);
                    method.MethodName = col["methodName"].ToString();


                    methods.Add(method);

                }
            }

            return methods;
        }
        public int GetMethodCount()
        {
            string sql = "SELECT count(methodId) as methodCount FROM TeachingMethod m WHERE m.uuid = '" + this.Uuid + "'";

            int methodCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    methodCount = Convert.ToInt32(col["methodCount"]);
                }
            }

            return methodCount;
        }
        public int GetFilteredCount(string searchValue)
        {
            string sql = "SELECT count(methodId) as methodCount FROM TeachingMethod m WHERE m.uuid = '" + this.Uuid + "' AND m.methodName LIKE '%" + searchValue + "%'";

            int methodCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    methodCount = Convert.ToInt32(col["methodCount"]);
                }
            }

            return methodCount;
        }
        public void SaveMethod()
        {
            string insertMethodSql = "INSERT INTO TeachingMethod (methodName, uuid) Output Inserted.methodId VALUES ('" + this.MethodName + "', '" + this.Uuid + "')";

            DbConfig.InsertData(insertMethodSql);
        }
        public bool CheckMethod()
        {
            string sql = "SELECT * FROM TeachingMethod where methodName = '" + DbConfig.EscapeString(this.MethodName) + "' AND uuid = '"+this.Uuid+"'";

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void DeleteMethod(int id)
        {
            string changeMethodSql = "Update SubjectClasses SET methodId = NULL where methodId = " + id;

            DbConfig.UpdateData(changeMethodSql);

            string deleteMethodSql = "DELETE FROM TeachingMethod WHERE methodId = " + id;

            DbConfig.DeleteData(deleteMethodSql);
        }
        public ArrayList GetMethodDropdownList()
        {
            ArrayList dropdownOptions = new ArrayList();

            string sql = "SELECT * FROM TeachingMethod WHERE uuid = '"+this.Uuid+"'";

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {

                    DropdownOption dropdownOption = new DropdownOption();

                    dropdownOption.id = Convert.ToInt32(cols["methodId"]);
                    dropdownOption.text = cols["methodName"].ToString();

                    dropdownOptions.Add(dropdownOption);

                }
            }

            return dropdownOptions;
        }
    }
}