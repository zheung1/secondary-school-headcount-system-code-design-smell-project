using Headcount.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Headcount.Models
{
    public class Subject
    {
        protected int subjectId;

        protected string subjectName,
            subjectCode,
            subjectType,
            subjectLevel,
            subjectRemarks,
            marks,
            etrMarks;
        
        protected string uuid;

       private Admin subjectAdmin;
       private Exam subjectExam;

        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string SubjectCode { get; set; }
        public string SubjectType { get; set; }
        public string SubjectLevel { get; set; }
        public string SubjectRemarks { get; set; }
        public string Marks { get; set; }
        public string EtrMarks { get; set; }
        public string Uuid { get; set; }

        public Admin SubjectAdmin { get; set; }

        public Exam SubjectExam { get; set; }

       public Exam GetRelatedExam(int examId)
        {
            Exam exam = new Exam();
            exam = exam.GetExam(examId);
            return exam;
        }

        public List<Subject> GetSubjectList(int start, int length, string searchValue, string sortColumnName, string sortDirection)
        {
            string sql = "SELECT * FROM Subject s WHERE s.uuid = '" + this.Uuid + "' AND s.subjectName LIKE '%" + searchValue + "%' ORDER BY " + sortColumnName + " " + sortDirection + " OFFSET " + start + " ROWS FETCH NEXT " + length + " ROWS ONLY";

            List<Subject> subjects = new List<Subject>();

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {

                    Subject subject = new Subject();

                    subject.SubjectId = Convert.ToInt32(col["subjectId"]);
                    subject.SubjectCode = col["subjectCode"].ToString();
                    subject.SubjectName = col["subjectName"].ToString();
                    subject.SubjectType = col["subjectType"].ToString();
                    subject.SubjectLevel = col["subjectLevel"].ToString();

                    subjects.Add(subject);

                }
            }

            return subjects;
        }

        public int GetSubjectCount()
        {
            string sql = "SELECT count(subjectId) as subjectCount FROM Subject s WHERE s.uuid = '" + this.Uuid + "'";

            int subjectCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    subjectCount = Convert.ToInt32(col["subjectCount"]);
                }
            }

            return subjectCount;
        }

        public int GetFilteredCount(string searchValue)
        {
            string sql = "SELECT count(subjectId) as subjectCount FROM Subject s WHERE s.uuid = '" + this.Uuid + "' AND s.subjectName LIKE '%" + searchValue + "%'";

            int subjectCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    subjectCount = Convert.ToInt32(col["subjectCount"]);
                }
            }

            return subjectCount;
        }

        public Subject GetSubject(int id)
        {
            
            string sql = "SELECT * FROM Subject WHERE subjectId = " + id;

            ArrayList rows = DbConfig.Retrieve(sql);

            Subject subject = new Subject();

            foreach (IDictionary cols in rows)
            {
                subject.SubjectId = Convert.ToInt32(cols["subjectId"]);
                subject.SubjectCode = cols["subjectCode"].ToString();
                subject.SubjectName = cols["subjectName"].ToString();
                subject.SubjectRemarks = cols["subjectRemarks"].ToString();
                subject.SubjectType = cols["subjectType"].ToString();
                subject.SubjectLevel = cols["subjectLevel"].ToString();

            }

            return subject;
            
        }

        public ArrayList GetSubjectList(string examLevel)
        {
            ArrayList subjects = new ArrayList();

            string sql = "SELECT * FROM Subject WHERE subjectLevel = '"+ examLevel +"' AND uuid = '"+this.Uuid+"'";
            

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                int counter = 0;
                
                foreach (IDictionary cols in rows)
                {

                    Subject subject = new Subject();
                    
                    subject.SubjectId = Convert.ToInt32(cols["subjectId"]);
                    subject.SubjectCode = cols["subjectCode"].ToString();
                    subject.SubjectName = cols["subjectName"].ToString();
                    subject.SubjectType = cols["subjectType"].ToString();
                    subject.SubjectLevel = cols["subjectLevel"].ToString();

                    subjects.Add(subject);
                    
                    counter++;

                    //limit 5
                    if(counter > 4)
                    {
                        break;
                    }
                }
            }

            return subjects;
        }

        public void SaveSubject()
        {
            string sql = "INSERT INTO Subject (subjectCode, subjectName, subjectType, subjectLevel, subjectRemarks, uuid) Output Inserted.subjectId VALUES ('" + this.SubjectCode + "', '" + this.SubjectName + "','" + this.SubjectType + "','" + this.SubjectLevel + "', '" + this.SubjectRemarks + "' , '" + this.Uuid + "')";

            DbConfig.InsertData(sql);

        }

        public void UpdateSubject()
        {
            string sql = "UPDATE Subject SET subjectCode = '" + this.SubjectCode + "', subjectName = '" + this.SubjectName + "', subjectType = '" + this.SubjectType + "', subjectLevel = '" + this.SubjectLevel + "' , subjectRemarks = '" + this.SubjectRemarks + "' WHERE subjectId = " + this.SubjectId;

            DbConfig.UpdateData(sql);
        }

        public bool CheckExistence()
        {
            string sql = "SELECT * FROM Subject where subjectCode = '" + this.SubjectCode + "' AND subjectName = '" + this.SubjectName + "' AND uuid = '"+this.Uuid+"'";

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void DeleteSubject(int subjectId)
        {
            //get examSubjectIds

            string examSubjectSql = "SELECT * FROM ExamSubjects WHERE subjectId = '"+subjectId+"'";

            ArrayList examSubjectRows = DbConfig.Retrieve(examSubjectSql);

            int examSubjectId = 0;

            if (examSubjectRows != null)
            {
                foreach (IDictionary cols in examSubjectRows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);

                    //get subjectClassId
                    string getSubjectClassIdSql = "SELECT * FROM SubjectClasses where examSubjectId = '" + examSubjectId + "'";

                    ArrayList subjectClassRows = DbConfig.Retrieve(getSubjectClassIdSql);

                    int subjectClassId = 0;

                    if (subjectClassRows != null)
                    {
                        foreach (IDictionary subjectClassCols in subjectClassRows)
                        {
                            subjectClassId = Convert.ToInt32(subjectClassCols["subjectClassId"]);

                            //delete from result
                            string deleteResultSql = "DELETE FROM Result WHERE subjectClassId = " + subjectClassId;

                            DbConfig.DeleteData(deleteResultSql);

                            //delete from subjectClasses
                            string deleteSubjectClassSql = "DELETE FROM SubjectClasses where subjectClassId = '" + subjectClassId + "'";

                            DbConfig.DeleteData(deleteSubjectClassSql);
                        }
                    }

                    string deleteExamSubjectSql = "DELETE FROM ExamSubjects where examSubjectId = '"+ examSubjectId + "'";

                    DbConfig.DeleteData(deleteExamSubjectSql);
                    
                }
                    
            }

            string deleteTargetSql = "Delete from Target where subjectId = '"+subjectId+"'";

            DbConfig.DeleteData(deleteTargetSql);

            string deleteSubjectSql = "Delete from Subject where subjectId = '" + subjectId + "'";

            DbConfig.DeleteData(deleteSubjectSql);
        }

        public ArrayList GetStudentSubjectMarkList(int id, int examId)
        {
            ArrayList subjects = new ArrayList();

            string getSubjectsSql = "select * from Result r, SubjectClasses sc, ExamSubjects es, Subject s where r.subjectClassId = sc.subjectClassId and sc.examSubjectId = es.examSubjectId and es.subjectId = s.subjectId and studentId = '" + id + "' and es.examId = '" + examId + "'";

            ArrayList subjectRows = DbConfig.Retrieve(getSubjectsSql);

            if (subjectRows != null)
            {
                foreach (IDictionary subjectCols in subjectRows)
                {
                    Subject subject = new Subject();
                    subject.SubjectId = Convert.ToInt32(subjectCols["subjectId"]);
                    subject.SubjectName = subjectCols["subjectName"].ToString();
                    subject.Marks = subjectCols["marks"].ToString();
                    subject.SubjectCode = subjectCols["subjectCode"].ToString();

                    subjects.Add(subject);
                }
            }

            return subjects;
        }

        public ArrayList GetStudentSubjectTargetList(int id, string examYear)
        {
            ArrayList subjects = new ArrayList();

            string getSubjectsSql = "SELECT * FROM Target t, Subject s WHERE t.subjectId = s.subjectId and t.examYear = '" + examYear + "' and t.studentId = '" + id + "'";

            ArrayList subjectRows = DbConfig.Retrieve(getSubjectsSql);

            if (subjectRows != null)
            {
                foreach (IDictionary subjectCols in subjectRows)
                {
                    Subject subject = new Subject();
                    subject.SubjectId = Convert.ToInt32(subjectCols["subjectId"]);
                    subject.SubjectName = subjectCols["subjectName"].ToString();
                    subject.Marks = subjectCols["etr"].ToString();
                    subject.SubjectCode = subjectCols["subjectCode"].ToString();

                    subjects.Add(subject);
                }
            }

            return subjects;
        }



    }




}