using Headcount.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Headcount.Models
{
    public class Exam
    {
        private int examId;

        private string examName,
            examStartDate,
            examEndDate,
            examLevel,
            examRemarks,
            examYear,
            examMarkType,
            uuid;
        
        private Admin examAdmin;

        public int ExamId { get; set; }
        public string ExamName { get; set; }
        public string ExamStartDate { get; set; }
        public string ExamEndDate { get; set; }
        public string ExamLevel { get; set; }
        public string ExamRemarks { get; set; }
        public string ExamYear { get; set; }
        public string ExamMarkType { get; set; }
        public string Uuid { get; set; }

        public Admin ExamAdmin { get; set; }

        public List<Exam> GetExamList(int start, int length, string searchValue, string sortColumnName, string sortDirection)
        {

            string sql = "SELECT * FROM Exam e, Admin a WHERE e.adminId = a.adminId AND e.uuid = '" + this.Uuid + "' AND e.examName LIKE '%" + searchValue + "%' ORDER BY " + sortColumnName + " " + sortDirection + " OFFSET " + start + " ROWS FETCH NEXT " + length + " ROWS ONLY";

            List<Exam> exams = new List<Exam>();

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {

                    Exam exam = new Exam();

                    exam.ExamId = Convert.ToInt32(col["examId"]);
                    exam.ExamName = col["examName"].ToString();
                    exam.ExamYear = col["examYear"].ToString();
                    exam.ExamLevel = col["examLevel"].ToString();
                    exam.ExamMarkType = col["examMarkType"].ToString();

                    Admin admin = new Admin();
                    admin.AdminId = Convert.ToInt32(col["adminId"]);
                    admin.AdminName = col["adminName"].ToString();

                    exam.ExamAdmin = admin;

                    exams.Add(exam);

                }
            }

            return exams;
        }

        public int GetExamCount()
        {
            string sql = "SELECT count(examId) as examCount FROM Exam e, Admin a WHERE e.adminId = a.adminId AND e.uuid = '" + this.Uuid + "'";

            int examCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    examCount = Convert.ToInt32(col["examCount"]);
                }
            }

            return examCount;
        }

        public int GetFilteredCount(string searchValue)
        {
            string sql = "SELECT count(examId) as examCount FROM Exam e, Admin a WHERE e.adminId = a.adminId AND e.uuid = '" + this.Uuid + "' AND examName LIKE '%" + searchValue + "%'";

            int examCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    examCount = Convert.ToInt32(col["examCount"]);
                }
            }

            return examCount;
        }

        

        public ArrayList GetExamList()
        {
            string sql = "SELECT * FROM Exam e, Admin a WHERE e.adminId = a.adminId AND e.uuid = '"+this.Uuid+"'";

            ArrayList exams = new ArrayList();

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {

                    Exam exam = new Exam();

                    exam.ExamId = Convert.ToInt32(cols["examId"]);
                    exam.ExamName = cols["examName"].ToString();
                    exam.ExamYear = cols["examYear"].ToString();
                    exam.ExamLevel = cols["examLevel"].ToString();
                    exam.ExamMarkType = cols["examMarkType"].ToString();

                    Admin admin = new Admin();
                    admin.AdminId = Convert.ToInt32(cols["adminId"]);
                    admin.AdminName = cols["adminName"].ToString();

                    exam.ExamAdmin = admin;

                    exams.Add(exam);

                }
            }

            return exams;
        }

        public Exam GetExam(int id)
        {
            string sql = "SELECT * FROM Exam e, Admin a, Users u WHERE e.adminId = a.adminId AND a.userId = u.userId AND examId = " + id;

            ArrayList rows = DbConfig.Retrieve(sql);

            Exam exam = new Exam();
            Admin admin = new Admin();

            foreach (IDictionary cols in rows)
            {
                exam.ExamId = Convert.ToInt32(cols["examId"]);
                exam.ExamName = cols["examName"].ToString();
                exam.ExamStartDate = cols["examStartDate"].ToString();
                exam.ExamEndDate = cols["examEndDate"].ToString();
                exam.ExamLevel = cols["examLevel"].ToString();
                exam.ExamRemarks = cols["examRemarks"].ToString();
                exam.ExamYear = cols["examYear"].ToString();
                exam.ExamMarkType = cols["examMarkType"].ToString();

                admin.AdminId = Convert.ToInt32(cols["adminId"]);
                admin.AdminName = cols["adminName"].ToString();
                admin.Username = cols["username"].ToString();

                exam.ExamAdmin = admin;

            }

            return exam;
        }

        public void UpdateExam()
        {
            string sql = "UPDATE Exam SET examName = '" + this.ExamName + "', examStartDate = '" + this.ExamStartDate + "', examEndDate = '" + this.ExamEndDate + "' , examLevel = '" + this.ExamLevel + "' , examRemarks = '" + this.ExamRemarks + "', adminId = '" + this.ExamAdmin.AdminId + "' , examYear = '" + this.ExamYear + "' , examMarkType = '" + this.ExamMarkType + "' WHERE examId = " + this.ExamId;

            DbConfig.UpdateData(sql);
        }

        public void SaveExam()
        {
            string sql = "INSERT INTO Exam (examName, examStartDate, examEndDate, examLevel, examRemarks, adminId, examYear, examMarkType, uuid) Output Inserted.examId VALUES ('" + this.ExamName + "', '" + this.ExamStartDate + "','" + this.ExamEndDate + "','" + this.ExamLevel + "','" + this.ExamRemarks + "', '" + this.ExamAdmin.AdminId + "', '" + this.ExamYear + "', '" + this.ExamMarkType + "', '" + this.Uuid + "')";

            DbConfig.InsertData(sql);
        }

        public void DeleteExam(int examId)
        {
            string examSubjectSql = "SELECT * FROM ExamSubjects WHERE examId = '" + examId + "'";

            ArrayList examSubjectRows = DbConfig.Retrieve(examSubjectSql);

            int examSubjectId = 0;

            if (examSubjectRows != null)
            {
                foreach (IDictionary cols in examSubjectRows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);

                    string subjectClassIdSql = "SELECT * FROM SubjectClasses WHERE examSubjectId = '" + examSubjectId + "'";

                    ArrayList subjectClassRows = DbConfig.Retrieve(subjectClassIdSql);

                    int subjectClassId = 0;

                    if (subjectClassRows != null)
                    {
                        foreach (IDictionary subjectClassCols in subjectClassRows)
                        {
                            subjectClassId = Convert.ToInt32(subjectClassCols["subjectClassId"]);

                            string deleteResultSql = "DELETE FROM Result WHERE subjectClassId = " + subjectClassId;

                            DbConfig.DeleteData(deleteResultSql);

                            string deleteSubjectClassSql = "DELETE FROM SubjectClasses where subjectClassId = '" + subjectClassId + "'";

                            DbConfig.DeleteData(deleteSubjectClassSql);
                        }
                    }

                    string deleteExamSubjectSql = "DELETE FROM ExamSubjects where examSubjectId = '" + examSubjectId + "'";

                    DbConfig.DeleteData(deleteExamSubjectSql);

                }
            }

            string deleteExamSql = "DELETE FROM Exam where examId = '" + examId + "'";

            DbConfig.DeleteData(deleteExamSql);
        }

        public ArrayList GetMarkList(int id)
        {
            string sql = "select * from result r, SubjectClasses sc, ExamSubjects es, Exam e where r.subjectClassId = sc.subjectClassId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and e.examId = '"+id+"' and r.marks = '' or r.marks = null";

            ArrayList exams = new ArrayList();

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {

                    Exam exam = new Exam();

                    exam.ExamId = Convert.ToInt32(cols["examId"]);
                    exam.ExamName = cols["examName"].ToString();
                    exam.ExamStartDate = cols["examStartDate"].ToString();
                    exam.ExamEndDate = cols["examEndDate"].ToString();
                    exam.ExamLevel = cols["examLevel"].ToString();
                    exam.ExamRemarks = cols["examRemarks"].ToString();
                    exam.ExamMarkType = cols["examMarkType"].ToString();

                    Admin admin = new Admin();
                    admin.AdminId = Convert.ToInt32(cols["adminId"]);
                    admin.AdminName = cols["adminName"].ToString();

                    exam.ExamAdmin = admin;

                    exams.Add(exam);

                }
            }

            return exams;
        }

        

        public List<Class> GetMarkStatusList(int start, int length, string searchValue, string sortColumnName, string sortDirection, int examId)
        {
            string sql = "select distinct(subjectClassId) from result where marks is null or marks = ''";

            ArrayList rows = DbConfig.Retrieve(sql);

            List<Class> statuses = new List<Class>();

            if (rows != null)
            {

                foreach (IDictionary cols in rows)
                {
                    int subjectClassId = Convert.ToInt32(cols["subjectClassId"]);

                    string subjectClassInfoSql = "select * from SubjectClasses sc, ExamSubjects es, Exam e, Subject s, Class c, Teacher t, Admin a where sc.examSubjectId = es.examSubjectId and es.examId = e.examId and es.subjectId = s.subjectId and sc.classId = c.classId and sc.teacherId = t.teacherId and sc.adminId = a.adminId and e.examId = '"+examId+"' and sc.subjectClassId = '" + subjectClassId + "' AND s.subjectName LIKE '%" + searchValue + "%' ORDER BY " + sortColumnName + " " + sortDirection + " OFFSET " + start + " ROWS FETCH NEXT " + length + " ROWS ONLY";

                    ArrayList subjectClassInfoRows = DbConfig.Retrieve(subjectClassInfoSql);

                    if (subjectClassInfoRows != null)
                    {
                        foreach (IDictionary subjectClassInfoCols in subjectClassInfoRows)
                        {
                            Exam exam = new Exam();

                            exam.ExamName = subjectClassInfoCols["examName"].ToString();
                            exam.ExamLevel = subjectClassInfoCols["examLevel"].ToString();
                            exam.ExamYear = subjectClassInfoCols["examYear"].ToString();
                            exam.ExamMarkType = subjectClassInfoCols["examMarkType"].ToString();

                            Admin admin = new Admin();
                            admin.AdminName = subjectClassInfoCols["adminName"].ToString();

                            Teacher teacher = new Teacher();
                            teacher.TeacherName = subjectClassInfoCols["teacherName"].ToString();

                            Subject subject = new Subject();
                            subject.SubjectName = subjectClassInfoCols["subjectName"].ToString();
                            subject.SubjectExam = exam;

                            Class kelas = new Class();

                            kelas.ClassName = subjectClassInfoCols["className"].ToString();
                            kelas.ClassAdmin = admin;
                            kelas.ClassTeacher = teacher;
                            kelas.ClassSubject = subject;

                            statuses.Add(kelas);
                        }
                    }
                }
            }

            return statuses;
        }

        public int GetMarkStatusCount(int examId)
        {
            string sql = "select distinct(subjectClassId) from result where marks is null or marks = ''";

            ArrayList rows = DbConfig.Retrieve(sql);

            List<Class> statuses = new List<Class>();

            int statusCount = 0;

            if (rows != null)
            {

                foreach (IDictionary cols in rows)
                {
                    int subjectClassId = Convert.ToInt32(cols["subjectClassId"]);

                    string subjectClassInfoSql = "select count(e.examId) as examCount from SubjectClasses sc, ExamSubjects es, Exam e, Subject s, Class c, Teacher t, Admin a where sc.examSubjectId = es.examSubjectId and es.examId = e.examId and es.subjectId = s.subjectId and sc.classId = c.classId and sc.teacherId = t.teacherId and sc.adminId = a.adminId and e.examId = '" + examId + "' and sc.subjectClassId = '" + subjectClassId + "'";

                    ArrayList subjectClassInfoRows = DbConfig.Retrieve(subjectClassInfoSql);

                    if (subjectClassInfoRows != null)
                    {
                        foreach (IDictionary subjectClassInfoCols in subjectClassInfoRows)
                        {
                            statusCount += Convert.ToInt32(subjectClassInfoCols["examCount"]);
                        }
                    }
                }
            }

            return statusCount;
        }

        public int GetFilteredMarkStatusCount(string searchValue, int examId)
        {
            string sql = "select distinct(subjectClassId) from result where marks is null or marks = ''";

            ArrayList rows = DbConfig.Retrieve(sql);

            List<Class> statuses = new List<Class>();

            int statusCount = 0;

            if (rows != null)
            {

                foreach (IDictionary cols in rows)
                {
                    int subjectClassId = Convert.ToInt32(cols["subjectClassId"]);

                    string subjectClassInfoSql = "select count(e.examId) as examCount from SubjectClasses sc, ExamSubjects es, Exam e, Subject s, Class c, Teacher t, Admin a where sc.examSubjectId = es.examSubjectId and es.examId = e.examId and es.subjectId = s.subjectId and sc.classId = c.classId and sc.teacherId = t.teacherId and sc.adminId = a.adminId and e.examId = '" + examId + "' and sc.subjectClassId = '" + subjectClassId + "' AND s.subjectName LIKE '%" + searchValue + "%'";

                    ArrayList subjectClassInfoRows = DbConfig.Retrieve(subjectClassInfoSql);

                    if (subjectClassInfoRows != null)
                    {
                        foreach (IDictionary subjectClassInfoCols in subjectClassInfoRows)
                        {
                            statusCount += Convert.ToInt32(subjectClassInfoCols["examCount"]);
                        }
                    }
                }
            }

            return statusCount;
        }

        public List<Class> GetTargetStatusList(int start, int length, string searchValue, string sortColumnName, string sortDirection, int examId)
        {
            string sql = "select distinct es.subjectId, sb.subjectName, e.examName, e.examYear, e.examMarkType, e.examLevel, t.teacherName, a.adminName from result r, SubjectClasses sc, ExamSubjects es, Exam e, Subject sb, Class c, Admin a, Teacher t where r.subjectClassId = sc.subjectClassId and sc.classId = c.classId and es.subjectId = sb.subjectId and sc.examSubjectId = es.examSubjectId and sc.adminId = a.adminId and sc.teacherId = t.teacherId and es.examId = e.examId and e.examId = '" + examId + "' AND sb.subjectName LIKE '%" + searchValue + "%' ORDER BY " + sortColumnName + " " + sortDirection + " OFFSET " + start + " ROWS FETCH NEXT " + length + " ROWS ONLY";

            ArrayList rows = DbConfig.Retrieve(sql);

            List<Class> statuses = new List<Class>();

            if (rows != null)
            {


                foreach (IDictionary cols in rows)
                {

                    Exam exam = new Exam();
                    exam.ExamName = cols["examName"].ToString();
                    exam.ExamLevel = cols["examLevel"].ToString();
                    exam.ExamYear = cols["examYear"].ToString();
                    exam.ExamMarkType = cols["examMarkType"].ToString();

                    Admin admin = new Admin();
                    admin.AdminName = cols["adminName"].ToString();

                    Teacher teacher = new Teacher();
                    teacher.TeacherName = cols["teacherName"].ToString();

                    Subject subject = new Subject();
                    subject.SubjectId = Convert.ToInt32(cols["subjectId"]);
                    subject.SubjectName = cols["subjectName"].ToString();
                    subject.SubjectExam = exam;

                    Class kelas = new Class();

                    kelas.ClassAdmin = admin;
                    kelas.ClassTeacher = teacher;
                    kelas.ClassSubject = subject;

                    string targetSql = "select * from target where (tov is null or tov = '' or oti1 is null or oti1 = '' or oti2 is null or oti2 = '' or etr is null or etr = '') and subjectId = '" + subject.SubjectId + "' and examYear = '" + exam.ExamYear + "'";

                    ArrayList targetRows = DbConfig.Retrieve(targetSql);

                    if (targetRows != null)
                    {
                        statuses.Add(kelas);
                    }



                }
            }

            return statuses;
        }

        public int GetTargetStatusCount(int examId)
        {
            string sql = "select distinct es.subjectId, sb.subjectName, e.examName, e.examYear, e.examMarkType, e.examLevel, t.teacherName, a.adminName from result r, SubjectClasses sc, ExamSubjects es, Exam e, Subject sb, Class c, Admin a, Teacher t where r.subjectClassId = sc.subjectClassId and sc.classId = c.classId and es.subjectId = sb.subjectId and sc.examSubjectId = es.examSubjectId and sc.adminId = a.adminId and sc.teacherId = t.teacherId and es.examId = e.examId and e.examId = '" + examId + "'";

            ArrayList rows = DbConfig.Retrieve(sql);

            int statusCount = 0;

            List<Class> statuses = new List<Class>();

            if (rows != null)
            {


                foreach (IDictionary cols in rows)
                {

                    Exam exam = new Exam();
                    exam.ExamName = cols["examName"].ToString();
                    exam.ExamLevel = cols["examLevel"].ToString();
                    exam.ExamYear = cols["examYear"].ToString();
                    exam.ExamMarkType = cols["examMarkType"].ToString();

                    Admin admin = new Admin();
                    admin.AdminName = cols["adminName"].ToString();

                    Teacher teacher = new Teacher();
                    teacher.TeacherName = cols["teacherName"].ToString();

                    Subject subject = new Subject();
                    subject.SubjectId = Convert.ToInt32(cols["subjectId"]);
                    subject.SubjectName = cols["subjectName"].ToString();
                    subject.SubjectExam = exam;

                    Class kelas = new Class();

                    kelas.ClassAdmin = admin;
                    kelas.ClassTeacher = teacher;
                    kelas.ClassSubject = subject;

                    string targetSql = "select * from target where (tov is null or tov = '' or oti1 is null or oti1 = '' or oti2 is null or oti2 = '' or etr is null or etr = '') and subjectId = '" + subject.SubjectId + "' and examYear = '" + exam.ExamYear + "'";

                    ArrayList targetRows = DbConfig.Retrieve(targetSql);

                    if (targetRows != null)
                    {
                        statuses.Add(kelas);
                    }
                }
            }

            statusCount = statuses.Count;

            return statusCount;
        }

        public int GetFilteredTargetStatusCount(string searchValue, int examId)
        {
            string sql = "select distinct es.subjectId, sb.subjectName, e.examName, e.examYear, e.examMarkType, e.examLevel, t.teacherName, a.adminName from result r, SubjectClasses sc, ExamSubjects es, Exam e, Subject sb, Class c, Admin a, Teacher t where r.subjectClassId = sc.subjectClassId and sc.classId = c.classId and es.subjectId = sb.subjectId and sc.examSubjectId = es.examSubjectId and sc.adminId = a.adminId and sc.teacherId = t.teacherId and es.examId = e.examId and e.examId = '" + examId + "' AND sb.subjectName LIKE '%" + searchValue + "%'";

            ArrayList rows = DbConfig.Retrieve(sql);

            int statusCount = 0;

            List<Class> statuses = new List<Class>();

            if (rows != null)
            {


                foreach (IDictionary cols in rows)
                {

                    Exam exam = new Exam();
                    exam.ExamName = cols["examName"].ToString();
                    exam.ExamLevel = cols["examLevel"].ToString();
                    exam.ExamYear = cols["examYear"].ToString();
                    exam.ExamMarkType = cols["examMarkType"].ToString();

                    Admin admin = new Admin();
                    admin.AdminName = cols["adminName"].ToString();

                    Teacher teacher = new Teacher();
                    teacher.TeacherName = cols["teacherName"].ToString();

                    Subject subject = new Subject();
                    subject.SubjectId = Convert.ToInt32(cols["subjectId"]);
                    subject.SubjectName = cols["subjectName"].ToString();
                    subject.SubjectExam = exam;

                    Class kelas = new Class();

                    kelas.ClassAdmin = admin;
                    kelas.ClassTeacher = teacher;
                    kelas.ClassSubject = subject;

                    string targetSql = "select * from target where (tov is null or tov = '' or oti1 is null or oti1 = '' or oti2 is null or oti2 = '' or etr is null or etr = '') and subjectId = '" + subject.SubjectId + "' and examYear = '" + exam.ExamYear + "'";

                    ArrayList targetRows = DbConfig.Retrieve(targetSql);

                    if (targetRows != null)
                    {
                        statuses.Add(kelas);
                    }



                }
            }

            statusCount = statuses.Count;

            return statusCount;
        }


        public ArrayList GetMarkStatus(int id)
        {
            //string sql = "select distinct(sc.subjectClassId), e.examName, e.examLevel, e.examYear, e.examMarkType, sb.subjectName, c.className, t.teacherName, a.adminName from result r, SubjectClasses sc, ExamSubjects es, Exam e, Subject sb, Class c, Admin a, Teacher t where r.subjectClassId = sc.subjectClassId and sc.classId = c.classId and es.subjectId = sb.subjectId and sc.examSubjectId = es.examSubjectId and sc.adminId = a.adminId and sc.teacherId = t.teacherId and es.examId = e.examId and e.examId = '"+id+"' and r.marks = '' or r.marks = null ";

            string sql = "select distinct(subjectClassId) from result where marks is null or marks = ''";

            ArrayList rows = DbConfig.Retrieve(sql);

            ArrayList statuses = new ArrayList();

            if (rows != null)
            {
                
                foreach (IDictionary cols in rows)
                {
                    int subjectClassId = Convert.ToInt32(cols["subjectClassId"]);

                    string subjectClassInfoSql = "select * from SubjectClasses sc, ExamSubjects es, Exam e, Subject s, Class c, Teacher t, Admin a where sc.examSubjectId = es.examSubjectId and es.examId = e.examId and es.subjectId = s.subjectId and sc.classId = c.classId and sc.teacherId = t.teacherId and sc.adminId = a.adminId and sc.subjectClassId = '"+ subjectClassId + "'";

                    ArrayList subjectClassInfoRows = DbConfig.Retrieve(subjectClassInfoSql);

                    if (subjectClassInfoRows != null)
                    {
                        foreach (IDictionary subjectClassInfoCols in subjectClassInfoRows)
                        {
                            Exam exam = new Exam();

                            exam.ExamName = subjectClassInfoCols["examName"].ToString();
                            exam.ExamLevel = subjectClassInfoCols["examLevel"].ToString();
                            exam.ExamYear = subjectClassInfoCols["examYear"].ToString();
                            exam.ExamMarkType = subjectClassInfoCols["examMarkType"].ToString();

                            Admin admin = new Admin();
                            admin.AdminName = subjectClassInfoCols["adminName"].ToString();

                            Teacher teacher = new Teacher();
                            teacher.TeacherName = subjectClassInfoCols["teacherName"].ToString();

                            Subject subject = new Subject();
                            subject.SubjectName = subjectClassInfoCols["subjectName"].ToString();
                            subject.SubjectExam = exam;

                            Class kelas = new Class();

                            kelas.ClassName = subjectClassInfoCols["className"].ToString();
                            kelas.ClassAdmin = admin;
                            kelas.ClassTeacher = teacher;
                            kelas.ClassSubject = subject;

                            statuses.Add(kelas);
                        }
                    }
                }
            }

            return statuses;
        }

        public ArrayList GetTargetStatus(int id)
        {
            string sql = "select distinct es.subjectId, sb.subjectName, e.examName, e.examYear, e.examMarkType, e.examLevel, t.teacherName, a.adminName from result r, SubjectClasses sc, ExamSubjects es, Exam e, Subject sb, Class c, Admin a, Teacher t where r.subjectClassId = sc.subjectClassId and sc.classId = c.classId and es.subjectId = sb.subjectId and sc.examSubjectId = es.examSubjectId and sc.adminId = a.adminId and sc.teacherId = t.teacherId and es.examId = e.examId and e.examId = '"+id+"'";

            ArrayList rows = DbConfig.Retrieve(sql);

            ArrayList statuses = new ArrayList();

            if (rows != null)
            {


                foreach (IDictionary cols in rows)
                {

                    Exam exam = new Exam();
                    exam.ExamName = cols["examName"].ToString();
                    exam.ExamLevel = cols["examLevel"].ToString();
                    exam.ExamYear = cols["examYear"].ToString();
                    exam.ExamMarkType = cols["examMarkType"].ToString();

                    Admin admin = new Admin();
                    admin.AdminName = cols["adminName"].ToString();

                    Teacher teacher = new Teacher();
                    teacher.TeacherName = cols["teacherName"].ToString();

                    Subject subject = new Subject();
                    subject.SubjectId = Convert.ToInt32(cols["subjectId"]);
                    subject.SubjectName = cols["subjectName"].ToString();
                    subject.SubjectExam = exam;

                    Class kelas = new Class();

                    kelas.ClassAdmin = admin;
                    kelas.ClassTeacher = teacher;
                    kelas.ClassSubject = subject;

                    string targetSql = "select * from target where (tov is null or tov = '' or oti1 is null or oti1 = '' or oti2 is null or oti2 = '' or etr is null or etr = '') and subjectId = '"+ subject.SubjectId + "' and examYear = '"+ exam.ExamYear + "'";

                    ArrayList targetRows = DbConfig.Retrieve(targetSql);

                    if(targetRows != null)
                    {
                        statuses.Add(kelas);
                    }

                    

                }
            }

            return statuses;
        }

        public bool CheckExistence()
        {
            string sql = "SELECT * FROM Exam where examLevel = '" + this.ExamLevel + "' AND examYear = '" + this.ExamYear + "' AND examMarkType = '" + this.ExamMarkType + "'";

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

    }
}