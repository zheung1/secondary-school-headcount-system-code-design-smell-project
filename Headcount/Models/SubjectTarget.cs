using Headcount.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace Headcount.Models
{
    public class SubjectTarget
    {
        private Subject subject;

        public SubjectTarget()
        {
            
        }

        public SubjectTarget(Subject subject)
        {
            this.subject = subject;
        }

        public int[,] GetGradeCounts(int subjectId, string examYear, string form, string examLevel)
        {
            Stopwatch timer = new Stopwatch();

            timer.Start();

            Grade grade;

            if (examLevel == "Menengah Bawah")
            {
                grade = new Pt3Grade();

                int[,] gradeCounts = grade.GetGradeCounts(subjectId, examYear, form);

                timer.Stop();

                System.Diagnostics.Debug.WriteLine("GetGradeCounts() : " + timer.Elapsed);

                return gradeCounts;
            }
            else if (examLevel == "Menengah Atas")
            {
                grade = new SpmGrade();

                int[,] gradeCounts = grade.GetGradeCounts(subjectId, examYear, form);

                timer.Stop();

                System.Diagnostics.Debug.WriteLine("GetGradeCounts() : " + timer.Elapsed);

                return gradeCounts;
            }

            return null;
        }


        public double[,] GetGradePercentage(int[,] gradeCounts, String examLevel)
        {
            Stopwatch timer = new Stopwatch();

            timer.Start();

            Grade grade;

            if (examLevel == "Menengah Bawah")
            {
                grade = new Pt3Grade();
                double[,] gradePercentage = grade.GetGradePercentage(gradeCounts);

                timer.Stop();

                System.Diagnostics.Debug.WriteLine("GetGradePercentage() : " + timer.Elapsed);

                return gradePercentage;
            }
            else if(examLevel == "Menengah Atas")
            {
                grade = new SpmGrade();
                double[,] gradePercentage = grade.GetGradePercentage(gradeCounts);

                timer.Stop();

                System.Diagnostics.Debug.WriteLine("GetGradePercentage() : " + timer.Elapsed);

                return gradePercentage;
            }

            return null;
        }

        public double[] GetGpmp(int[,] gradeCounts, string subjectLevel)
        {
            double[] gpmpSum = new double[7] { 0, 0, 0, 0, 0, 0, 0 };
            double[] gpmp = new double[7] { 0, 0, 0, 0, 0, 0, 0 };
            int[] total = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

            for (int i = 0; i < gradeCounts.GetLength(0); i++)
            {
                for (int j = 0; j < gradeCounts.GetLength(1) - 1; j++)
                {

                    if (subjectLevel == "Menengah Bawah")
                    {
                        total[i] += gradeCounts[i, j];
                        gpmpSum[i] += gradeCounts[i, j] * (j + 1);
                    }
                    else if (subjectLevel == "Menengah Atas")
                    {
                        total[i] += gradeCounts[i, j];
                        gpmpSum[i] += gradeCounts[i, j] * j;
                    }

                }
            }

            for (int i = 0; i < gpmp.Length; i++)
            {

                if (subjectLevel == "Menengah Bawah")
                {
                    if (total[i] != 0)
                        gpmp[i] = Math.Round(gpmpSum[i] / total[i], 2);
                }
                else if (subjectLevel == "Menengah Atas")
                {
                    if (total[i] != 0)
                        gpmp[i] = Math.Round(gpmpSum[i] / total[i], 2);
                }

            }

            return gpmp;
        }

        public int[] GetTotalStudent(int[,] gradeCounts)
        {
            int[] total = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

            for (int i = 0; i < gradeCounts.GetLength(0); i++)
            {
                for (int j = 0; j < gradeCounts.GetLength(1); j++)
                {
                    total[i] += gradeCounts[i, j];
                }
            }

            return total;
        }

        public int[] GetPassNumber(int[] total, int[,] gradeCounts, string subjectLevel)
        {
            int[] passes = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

            for (int i = 0; i < total.Length; i++)
            {

                if (subjectLevel == "Menengah Bawah")
                {
                    passes[i] += total[i] - gradeCounts[i, 4] - gradeCounts[i, 5];
                }
                else if (subjectLevel == "Menengah Atas")
                {
                    passes[i] += total[i] - gradeCounts[i, 9] - gradeCounts[i, 10];
                }
            }

            return passes;
        }

        public int[] GetFailNumber(int[,] gradeCounts, string subjectLevel)
        {
            int[] fails = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

            for (int i = 0; i < fails.Length; i++)
            {
                if (subjectLevel == "Menengah Bawah")
                {
                    fails[i] += gradeCounts[i, 4];
                }
                else if (subjectLevel == "Menengah Atas")
                {
                    fails[i] += gradeCounts[i, 9];
                }


            }

            return fails;
        }

        public double[] GetPassPercent(int[] passes, int[] fails)
        {
            double[] passPercent = new double[7] { 0, 0, 0, 0, 0, 0, 0 };

            for (int i = 0; i < passPercent.Length; i++)
            {
                if ((passes[i] + fails[i]) != 0)
                {
                    passPercent[i] = Math.Round(((double)passes[i] / (double)(passes[i] + fails[i])) * 100, 2);


                }

            }

            return passPercent;
        }

        public double[] GetFailPercent(int[] passes, int[] fails)
        {
            double[] failPercent = new double[7] { 0, 0, 0, 0, 0, 0, 0 };

            for (int i = 0; i < failPercent.Length; i++)
            {
                if ((passes[i] + fails[i]) != 0)
                {
                    failPercent[i] = Math.Round(((double)fails[i] / (double)(passes[i] + fails[i])) * 100, 2);
                }

            }

            return failPercent;
        }

    }

    

    

}