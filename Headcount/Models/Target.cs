using Headcount.Controllers;
using System;
using System.Collections;

namespace Headcount.Models
{
    public class Target
    {
        private int targetId,
            subjectClassId,
            studentId,
            subjectId;
        
        private Class targetClass;
        //private Subject targetSubject;
        private string examYear,
            studentName,
            studentIc,
            studentForm,
            tov,
            oti1,
            oti2,
            etr,
            tovYear,
            tovMarkType;

        private int tovForm;
        
        public int TargetId { get; set; }
        public int SubjectClassId { get; set; }
        public int StudentId { get; set; }
        public int SubjectId { get; set; }
        //public Student TargetStudent { get; set; }
        public Class TargetClass { get; set; }
        //public Subject TargetSubject { get; set; }
        public string ExamYear { get; set; }
        public string StudentName { get; set; }
        public string StudentIc { get; set; }
        public string StudentForm { get; set; }
        public string Tov { get; set; }
        public string Oti1 { get; set; }
        public string Oti2 { get; set; }
        public string Etr { get; set; }
        public string TovYear { get; set; }
        public int TovForm { get; set; }
        public string TovMarkType { get; set; }

        public int GetTargetCount()
        {
            // get examSubjectId
            string getExamSubjectIdSql = "SELECT * FROM ExamSubjects WHERE examId = '" + this.TargetClass.ClassSubject.SubjectExam.ExamId + "' AND subjectId = '" + this.TargetClass.ClassSubject.SubjectId + "'";

            ArrayList examSubjectRows = DbConfig.Retrieve(getExamSubjectIdSql);

            int examSubjectId = 0;

            if (examSubjectRows != null)
            {
                foreach (IDictionary cols in examSubjectRows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);
                }
            }

            string getSubjectClassIdSql = "SELECT * FROM SubjectClasses WHERE examSubjectId = '" + examSubjectId + "' AND classId = '" + this.TargetClass.ClassId + "'";

            ArrayList subjectClassRows = DbConfig.Retrieve(getSubjectClassIdSql);

            int subjectClassId = 0;

            if (subjectClassRows != null)
            {
                foreach (IDictionary cols in subjectClassRows)
                {
                    subjectClassId = Convert.ToInt32(cols["subjectClassId"]);
                }
            }

            string sql = "SELECT count(s.studentId) as targetCount FROM Result r, Student s where r.studentId = s.studentId AND r.subjectClassId = '" + subjectClassId + "'";

            ArrayList targetRows = DbConfig.Retrieve(sql);

            int targetCount = 0;

            if (targetRows != null)
            {
               
                foreach (IDictionary cols in targetRows)
                {

                    targetCount = Convert.ToInt32(cols["targetCount"]);
                    /*
                    Target target = new Target();
                    target.SubjectId = this.TargetClass.ClassSubject.SubjectId;

                    string getTovTargetSql = "SELECT count(studentId) as targetCount FROM Target where examYear = '" + this.ExamYear + "' AND studentForm = '" + this.StudentForm + "' AND studentId = '" + target.StudentId + "' AND subjectId = '" + this.TargetClass.ClassSubject.SubjectId + "'";

                    ArrayList tovTargetRows = DbConfig.Retrieve(getTovTargetSql);

                    

                    if (tovTargetRows != null)
                    {
                        foreach (IDictionary targetCols in tovTargetRows)
                        {
                            targetCount = Convert.ToInt32(targetCols["targetCount"]);
                        }
                    }
                    */
                }
               
            }

            return targetCount;

        }

        public ArrayList GetTargetList(int pageNumber = 0)
        {
            // get examSubjectId
            string getExamSubjectIdSql = "SELECT * FROM ExamSubjects WHERE examId = '" + this.TargetClass.ClassSubject.SubjectExam.ExamId + "' AND subjectId = '" + this.TargetClass.ClassSubject.SubjectId + "'";

            ArrayList examSubjectRows = DbConfig.Retrieve(getExamSubjectIdSql);

            int examSubjectId = 0;

            if (examSubjectRows != null)
            {
                foreach (IDictionary cols in examSubjectRows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);
                }
            }

            string getSubjectClassIdSql = "SELECT * FROM SubjectClasses WHERE examSubjectId = '" + examSubjectId + "' AND classId = '" + this.TargetClass.ClassId + "'";

            ArrayList subjectClassRows = DbConfig.Retrieve(getSubjectClassIdSql);

            int subjectClassId = 0;

            if (subjectClassRows != null)
            {
                foreach (IDictionary cols in subjectClassRows)
                {
                    subjectClassId = Convert.ToInt32(cols["subjectClassId"]);
                }
            }

            int offset = (pageNumber - 1) * 10;

            string sql = "";

            if (pageNumber == 0)
            {
                sql = "SELECT r.subjectClassId, r.studentId, s.studentName, s.studentIc, r.marks FROM Result r, Student s where r.studentId = s.studentId AND r.subjectClassId = '" + subjectClassId + "'";
            }
            else
            {
                sql = "SELECT r.subjectClassId, r.studentId, s.studentName, s.studentIc, r.marks FROM Result r, Student s where r.studentId = s.studentId AND r.subjectClassId = '" + subjectClassId + "'  ORDER BY s.studentName ASC OFFSET " + offset + " ROWS FETCH NEXT 10 ROWS ONLY";
            }


            ArrayList targetRows = DbConfig.Retrieve(sql);
            ArrayList targets = new ArrayList();

            if (targetRows != null)
            {
                foreach (IDictionary cols in targetRows)
                {
                    Target target = new Target();
                    target.SubjectClassId = Convert.ToInt32(cols["subjectClassId"]);
                    target.StudentId = Convert.ToInt32(cols["studentId"]);
                    target.StudentName = cols["studentName"].ToString();
                    target.StudentIc = cols["studentIc"].ToString();
                    target.ExamYear = this.ExamYear;
                    target.StudentForm = this.StudentForm;
                    target.SubjectId = this.TargetClass.ClassSubject.SubjectId;

                    string getTovTargetSql = "SELECT * FROM Target where examYear = '"+ this.ExamYear +"' AND studentForm = '"+this.StudentForm+"' AND studentId = '"+ target.StudentId + "' AND subjectId = '"+ this.TargetClass.ClassSubject.SubjectId + "'";

                    ArrayList tovTargetRows = DbConfig.Retrieve(getTovTargetSql);

                    if (tovTargetRows != null)
                    {
                        foreach (IDictionary targetCols in tovTargetRows)
                        {
                            
                            target.Tov = targetCols["tov"].ToString();
                            target.Oti1 = targetCols["oti1"].ToString();
                            target.Oti2 = targetCols["oti2"].ToString();
                            target.Etr = targetCols["etr"].ToString();

                            if (target.Tov == null || target.Tov == "") //try find tov in previous exam
                            {
                                string getTovExamSql = "";
                                
                                if (target.StudentForm == "peralihan" || target.StudentForm == "1" || target.StudentForm == "4")
                                {
                                    //getTovExamSql = "select * from result r, subjectClasses sc, ExamSubjects es, Exam e, Class c where r.subjectClassId = sc.subjectClassId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and sc.classId = c.classId and examMarkType = '" + this.TovMarkType + "' and examYear = '" + this.TovYear + "' and classForm = '" + this.TovForm + "' AND studentId = '" + target.StudentId + "' AND sc.subjectClassId = '" + subjectClassId + "'";

                                    getTovExamSql = "select * from Result r, SubjectClasses sc, ExamSubjects es, Exam e where r.subjectClassId = sc.subjectClassId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and examYear = '" + this.TovYear + "' and examMarkType = 'AR2' and subjectId = '" + target.SubjectId + "' and studentId = '" + target.StudentId + "'";
                                }
                                else if (target.StudentForm == "2" || target.StudentForm == "3" || target.StudentForm == "5")
                                {
                                    getTovExamSql = "select * from Result r, SubjectClasses sc, ExamSubjects es, Exam e where  r.subjectClassId = sc.subjectClassId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and examYear = '" + this.TovYear + "' and examMarkType = 'Markah Akhir Tahun' and subjectId = '" + target.SubjectId + "' and studentId = '" + target.StudentId + "'";
                                }
                                

                                ArrayList tovExamRows = DbConfig.Retrieve(getTovExamSql);

                                if (tovExamRows != null)
                                {
                                    foreach (IDictionary targetExamCols in tovExamRows)
                                    {
                                        target.Tov = targetExamCols["marks"].ToString();
                                    }
       
                                }

                            }
                            
                            if (target.Tov != "")
                            {
                                if (target.Etr == null || target.Etr == "")
                                {
                                    double forecastEtr = Convert.ToDouble(target.Tov) + 5;

                                    if (forecastEtr > 100)
                                    {
                                        target.Etr = target.Tov;
                                    }
                                    else
                                    {
                                        target.Etr = forecastEtr.ToString();
                                    }
                                }
                            }
                            

                            
                        }
                    }

                    targets.Add(target);
                }      
            }

            return targets;

        }

        public Target GetTarget()
        {
            // get examSubjectId
            string getExamSubjectIdSql = "SELECT * FROM ExamSubjects WHERE examId = '" + this.TargetClass.ClassSubject.SubjectExam.ExamId + "' AND subjectId = '" + this.TargetClass.ClassSubject.SubjectId + "'";

            ArrayList examSubjectRows = DbConfig.Retrieve(getExamSubjectIdSql);

            int examSubjectId = 0;

            if (examSubjectRows != null)
            {
                foreach (IDictionary cols in examSubjectRows)
                {
                    examSubjectId = Convert.ToInt32(cols["examSubjectId"]);
                }
            }

            string getSubjectClassIdSql = "SELECT * FROM SubjectClasses WHERE examSubjectId = '" + examSubjectId + "' AND classId = '" + this.TargetClass.ClassId + "'";

            ArrayList subjectClassRows = DbConfig.Retrieve(getSubjectClassIdSql);

            int subjectClassId = 0;

            if (subjectClassRows != null)
            {
                foreach (IDictionary cols in subjectClassRows)
                {
                    subjectClassId = Convert.ToInt32(cols["subjectClassId"]);
                }
            }

            string sql = "SELECT r.subjectClassId, r.studentId, s.studentName, s.studentIc, r.marks FROM Result r, Student s where r.studentId = s.studentId AND r.subjectClassId = '" + subjectClassId + "' and r.studentId = '" + this.StudentId + "'";

            ArrayList targetRows = DbConfig.Retrieve(sql);
            
            Target target = new Target();

            if (targetRows != null)
            {
                foreach (IDictionary cols in targetRows)
                {
                    
                    target.SubjectClassId = Convert.ToInt32(cols["subjectClassId"]);
                    target.StudentId = Convert.ToInt32(cols["studentId"]);
                    target.StudentName = cols["studentName"].ToString();
                    target.StudentIc = cols["studentIc"].ToString();
                    target.ExamYear = this.ExamYear;
                    target.StudentForm = this.StudentForm;
                    target.SubjectId = this.TargetClass.ClassSubject.SubjectId;

                    string getTovTargetSql = "SELECT * FROM Target where examYear = '" + this.ExamYear + "' AND studentForm = '" + this.StudentForm + "' AND studentId = '" + target.StudentId + "' AND subjectId = '" + this.TargetClass.ClassSubject.SubjectId + "'";

                    ArrayList tovTargetRows = DbConfig.Retrieve(getTovTargetSql);

                    if (tovTargetRows != null)
                    {
                        foreach (IDictionary targetCols in tovTargetRows)
                        {

                            target.Tov = targetCols["tov"].ToString();
                            target.Oti1 = targetCols["oti1"].ToString();
                            target.Oti2 = targetCols["oti2"].ToString();
                            target.Etr = targetCols["etr"].ToString();

                            if (target.Tov == null || target.Tov == "") //try find tov in previous exam
                            {
                                string getTovExamSql = "select * from result r, subjectClasses sc, ExamSubjects es, Exam e, Class c where r.subjectClassId = sc.subjectClassId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and sc.classId = c.classId and examMarkType = '" + this.TovMarkType + "' and examYear = '" + this.TovYear + "' and classForm = '" + this.TovForm + "' AND studentId = '" + target.StudentId + "'";

                                ArrayList tovExamRows = DbConfig.Retrieve(getTovExamSql);

                                if (tovExamRows != null)
                                {
                                    foreach (IDictionary targetExamCols in tovExamRows)
                                    {
                                        target.Tov = targetExamCols["marks"].ToString();
                                    }

                                }

                            }
                        }
                    }

                }
            }

            return target;

        }

        public void SaveTarget()
        {
            string sql = "UPDATE Target SET tov = '" + this.Tov + "', oti1 = '" + this.Oti1 + "', oti2 = '" + this.Oti2 + "', etr = '" + this.Etr + "' WHERE studentId = '" + this.StudentId + "' AND subjectId = '" + this.SubjectId + "'  AND examYear = '" + this.ExamYear + "'  AND studentForm = '" + this.StudentForm + "'";

            DbConfig.UpdateData(sql);
        }
    }
}