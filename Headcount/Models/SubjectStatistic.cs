using Headcount.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace Headcount.Models
{
    
    public class SubjectStatistic
    {
        private Subject subject;

        public SubjectStatistic(Subject subject)
        {
            this.subject = subject;
        }

        public ArrayList GetClassHourAverage()
        {
            Stopwatch timer = new Stopwatch();

            timer.Start();

            string sql = "SELECT avg(cast(replace(marks, '-', 0)as float)) as averageMarks, classHour FROM Result r, SubjectClasses sc, ExamSubjects es, Exam e WHERE r.subjectClassId = sc.subjectClassId and sc.examSubjectId = es.examSubjectId and e.examId = es.examId and e.examYear = '" + this.subject.SubjectExam.ExamYear + "' and subjectId = '" + this.subject.SubjectId + "' group by classHour";

            ArrayList rows = DbConfig.Retrieve(sql);

            ArrayList averages = new ArrayList();

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    ArrayList avg = new ArrayList();

                    avg.Add(Convert.ToDouble(col["averageMarks"]));

                    avg.Add(Convert.ToInt32(col["classHour"]));

                    averages.Add(avg);
                }
            }

            timer.Stop();

            System.Diagnostics.Debug.WriteLine("GetClassHourAverage() : " + timer.Elapsed);

            return averages;

        }

        public ArrayList GetTeachingMethodAverage()
        {
            Stopwatch timer = new Stopwatch();

            timer.Start();

            string sql = "SELECT avg(cast(replace(marks, '-', 0)as float)) as averageMarks, methodName FROM Result r, SubjectClasses sc, ExamSubjects es, Exam e, TeachingMethod t WHERE t.methodId = sc.methodId and r.subjectClassId = sc.subjectClassId and sc.examSubjectId = es.examSubjectId and e.examId = es.examId and e.examYear = '" + this.subject.SubjectExam.ExamYear + "' and subjectId = '" + this.subject.SubjectId + "' group by t.methodName";

            ArrayList rows = DbConfig.Retrieve(sql);

            ArrayList averages = new ArrayList();

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    ArrayList avg = new ArrayList();

                    avg.Add(Convert.ToDouble(col["averageMarks"]));

                    avg.Add(col["methodName"].ToString());

                    averages.Add(avg);
                }
            }

            timer.Stop();

            System.Diagnostics.Debug.WriteLine("GetTeachingMethodAverage() : " + timer.Elapsed);

            return averages;

        }

        public int GetSubjectYearCount()
        {
            string sql = "SELECT count(distinct(e.examYear)) as yearCount FROM subjectClasses sc, examSubjects es, exam e, subject s where sc.examSubjectId = es.examSubjectId and es.subjectId = s.subjectId and s.subjectId = '" + this.subject.SubjectId + "' and e.examId = es.examId";

            ArrayList rows = DbConfig.Retrieve(sql);
            int yearCount = 0;

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    yearCount = Convert.ToInt32(col["yearCount"]);
                }
            }

            return yearCount;
        }

        public ArrayList GetSubjectYears(int pageNumber = 0)
        {
            int offset = pageNumber * 10;

            string sql = "SELECT distinct(examYear) FROM subjectClasses sc, examSubjects es, exam e, subject s where sc.examSubjectId = es.examSubjectId and es.subjectId = s.subjectId and s.subjectId = '" + this.subject.SubjectId + "' and e.examId = es.examId order by examYear asc OFFSET " + offset + " ROWS FETCH NEXT 10 ROWS ONLY";

            ArrayList rows = DbConfig.Retrieve(sql);
            ArrayList examYears = new ArrayList();

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    examYears.Add(col["examYear"].ToString());
                }
            }

            return examYears;
        }

    }

}