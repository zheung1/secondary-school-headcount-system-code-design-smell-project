using Headcount.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Headcount.Models
{
    public class Announcement
    {
        private int announcementId;

        private string title,
            body;

        private int authorId;

        private string postDate;

        private string uuid;

        public int AnnouncementId { get; set; }
        public string Title { get; set; }

        public string Body { get; set; }

        public string PostDate { get; set; }
        public int AuthorId { get; set; }

        public string Uuid { get; set; }

        public List<Announcement> GetAnnouncementList(int start, int length, string searchValue, string sortColumnName, string sortDirection)
        {

            string sql = "SELECT convert(varchar, postDate, 3) as postDate, title, announcementId FROM Announcement WHERE uuid = '" + this.Uuid + "' AND title LIKE '%" + searchValue + "%' ORDER BY " + sortColumnName + " " + sortDirection + " OFFSET " + start + " ROWS FETCH NEXT " + length + " ROWS ONLY";

            List<Announcement> announcements = new List<Announcement>();

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {

                    Announcement announcement = new Announcement();

                    announcement.AnnouncementId = Convert.ToInt32(col["announcementId"]);
                    announcement.Title = col["title"].ToString();
                    announcement.PostDate = col["postDate"].ToString();


                    announcements.Add(announcement);

                }
            }

            return announcements;
        }

        public int GetAnnouncementCount()
        {
            string sql = "SELECT count(announcementId) as announcementCount FROM Announcement WHERE uuid = '" + this.Uuid + "'";

            int announcementCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    announcementCount = Convert.ToInt32(col["announcementCount"]);
                }
            }

            return announcementCount;
        }

        public int GetFilteredCount(string searchValue)
        {
            string sql = "SELECT count(announcementId) as announcementCount FROM Announcement WHERE uuid = '" + this.Uuid + "' AND title LIKE '%" + searchValue + "%'";

            int announcementCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    announcementCount = Convert.ToInt32(col["announcementCount"]);
                }
            }

            return announcementCount;
        }

        public void SaveAnnouncement()
        {
            string sql = "INSERT INTO announcement (title, body, uuid) Output Inserted.announcementId VALUES ('" + this.Title + "', '" + this.Body + "', '"+this.Uuid+"')";

            DbConfig.InsertData(sql);
        }

        public ArrayList GetAnnouncementList(bool home = false)
        {
            ArrayList announcements = new ArrayList();

            string sql = "SELECT convert(varchar, postDate, 3) as postDate, title, body, announcementId FROM Announcement WHERE uuid = '"+this.Uuid+"' order by announcementId DESC";


            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                int counter = 0;

                foreach (IDictionary cols in rows)
                {

                    Announcement announcement = new Announcement();

                    announcement.AnnouncementId = Convert.ToInt32(cols["announcementId"]);
                    announcement.Title = cols["title"].ToString();
                    announcement.Body = cols["body"].ToString();
                    announcement.PostDate = cols["postDate"].ToString();

                    announcements.Add(announcement);

                    counter++;

                    //limit 4
                    if ((counter > 3) && home)
                    {
                        break;
                    }
                }
            }

            return announcements;
        }

        public Announcement GetAnnouncement(int id)
        {
            string sql = "SELECT * FROM Announcement WHERE announcementId = " + id;

            ArrayList rows = DbConfig.Retrieve(sql);

            Announcement announcement = new Announcement();

            foreach (IDictionary cols in rows)
            {
                announcement.AnnouncementId = Convert.ToInt32(cols["announcementId"]);
                announcement.Title = cols["title"].ToString();
                announcement.Body = cols["body"].ToString();
            }

            return announcement;
        }

        public void UpdateAnnouncement()
        {
            string sql = "UPDATE Announcement SET title = '" + this.Title + "', body = '" + this.Body + "' WHERE announcementId = '" + this.AnnouncementId + "'";

            DbConfig.UpdateData(sql);
        }

        public void DeleteAnnouncement(int id)
        {
            string sql = "DELETE FROM Announcement WHERE announcementId = " + id;

            DbConfig.DeleteData(sql);
        }
    }

    




}