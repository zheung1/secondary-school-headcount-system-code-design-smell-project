using Headcount.Controllers;
using System;
using System.Collections;

namespace Headcount.Models
{
    public class User
    {
        protected int userId;

        protected String username,
            password,
            type,
            uuid;

        public int UserId { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public String Type { get; set; }
        public String Uuid { get; set; }
        public String ValidateLogin()
        {
            string sql = "SELECT * FROM Users where username = '" + DbConfig.EscapeString(this.Username) + "' and password = '" + DbConfig.EscapeString(this.Password) + "'";

            ArrayList rows = DbConfig.Retrieve(sql);

            string userType = "";

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    userType = cols["type"].ToString();
                }
            }

            return userType;
        }

        public String GetUuid()
        {
            string sql = "SELECT * FROM Users where username = '" + DbConfig.EscapeString(this.Username) + "'";

            ArrayList rows = DbConfig.Retrieve(sql);

            string uuid = "";

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    uuid = cols["uuid"].ToString();
                }
            }

            return uuid;
        }

        

    }
}