using Headcount.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Headcount.Models
{
    public interface Grade
    {
        double[,] GetGradePercentage(int[,] gradeCounts);
        int[,] GetGradeCounts(int subjectId, string examYear, string form);

       
    }

    public class SpmGrade : Grade
    {
        private double[,] gradePercentage = new double[7, 11] {
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        };

        int[,] gradeCounts = new int[7, 11] {
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        };

        private double[] total = new double[7] { 0, 0, 0, 0, 0, 0, 0 };

        public double[,] GetGradePercentage(int[,] gradeCounts)
        {


            for (int i = 0; i < gradeCounts.GetLength(0); i++)
            {
                for (int j = 0; j < gradeCounts.GetLength(1); j++)
                {
                    total[i] += gradeCounts[i, j];

                }
            }

            for (int i = 0; i < gradeCounts.GetLength(0); i++)
            {
                for (int j = 0; j < gradeCounts.GetLength(1); j++)
                {
                    if (total[i] != 0)
                        gradePercentage[i, j] = Math.Round((gradeCounts[i, j] / total[i]) * 100, 2);

                }
            }

            return gradePercentage;
        }

        public int[,] GetGradeCounts(int subjectId, string examYear, string form)
        {
            string tovYear = "";

            string tovMarkType = "";

            string tovForm = "";

            if (form == "4")
            {
                tovMarkType = "AR2";

                tovForm = "4";

                tovYear = examYear;
            }
            else if (form == "5")
            {
                tovMarkType = "Markah Akhir Tahun";

                tovForm = "4";

                tovYear = (Convert.ToInt32(examYear) - 1).ToString();
            }

            string getTovSql = "SELECT tov as marks, studentId FROM Target where examYear = '" + examYear + "' AND studentForm = '" + form + "' AND subjectId = '" + subjectId + "'";

            ArrayList rows = DbConfig.Retrieve(getTovSql);

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    Target target = new Target();

                    target.Tov = cols["marks"].ToString();
                    int tovGrade = 0;


                    if (target.Tov == null || target.Tov == "")
                    {
                        int studentId = Convert.ToInt32(cols["studentId"]);

                        //if tov is null, find in previous exam
                        string getTovExamSql = "select marks from Result r, SubjectClasses sc, ExamSubjects es, Exam e, Class c where r.subjectClassId = sc.subjectClassId and sc.classId = c.classId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and examYear = '" + tovYear + "' and subjectId = '" + subjectId + "' and examMarkType = '" + tovMarkType + "' and classForm = '" + tovForm + "' AND studentId = '" + studentId + "'";

                        ArrayList tovExamRows = DbConfig.Retrieve(getTovExamSql);

                        if (tovExamRows != null)
                        {
                            foreach (IDictionary targetExamCols in tovExamRows)
                            {
                                target.Tov = targetExamCols["marks"].ToString();
                            }

                            tovGrade = Result.GetGrade(target.Tov, "Menengah Atas");

                        }
                        else
                        {
                            tovGrade = Result.GetGrade(target.Tov, "Menengah Atas");
                        }
                    }
                    else
                    {
                        tovGrade = Result.GetGrade(target.Tov, "Menengah Atas");
                    }


                    gradeCounts[0, tovGrade] += 1;
                }
            }

            string getTargetSql = "select * from Target t where subjectId = '" + subjectId + "' and examYear = '" + examYear + "' and studentForm = '" + form + "'";

            ArrayList targetRows = DbConfig.Retrieve(getTargetSql);

            if (targetRows != null)
            {
                foreach (IDictionary cols in targetRows)
                {
                    int oti1Grade = Result.GetGrade(cols["oti1"].ToString(), "Menengah Atas");

                    gradeCounts[1, oti1Grade] += 1;

                    int oti2Grade = Result.GetGrade(cols["oti2"].ToString(), "Menengah Atas");

                    gradeCounts[3, oti2Grade] += 1;

                    int etrGrade = Result.GetGrade(cols["etr"].ToString(), "Menengah Atas");

                    gradeCounts[5, etrGrade] += 1;
                }
            }

            string getMarkSql = "select marks, examMarkType from Result r, SubjectClasses sc, ExamSubjects es, Exam e, Class c where r.subjectClassId = sc.subjectClassId and sc.classId = c.classId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and examYear = '" + examYear + "' and subjectId = '" + subjectId + "' and classForm = '" + form + "'";

            ArrayList markRows = DbConfig.Retrieve(getMarkSql);

            if (markRows != null)
            {
                foreach (IDictionary cols in markRows)
                {
                    string examMarkType = cols["examMarkType"].ToString();

                    if (examMarkType == "AR1")
                    {
                        int ar1Grade = Result.GetGrade(cols["marks"].ToString(), "Menengah Atas");
                        gradeCounts[2, ar1Grade] += 1;
                    }
                    else if (examMarkType == "AR2")
                    {
                        int ar2Grade = Result.GetGrade(cols["marks"].ToString(), "Menengah Atas");
                        gradeCounts[4, ar2Grade] += 1;
                    }
                    else if (examMarkType == "Markah Akhir Tahun")
                    {
                        int finalGrade = Result.GetGrade(cols["marks"].ToString(), "Menengah Atas");
                        gradeCounts[6, finalGrade] += 1;
                    }
                }
            }

            return gradeCounts;


        }

    }

    public class Pt3Grade : Grade
    {
        private double[,] gradePercentage = new double[7, 7] {
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                };

        private int[,] gradeCounts = new int[7, 7] {
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                };

        private double[] total = new double[7] { 0, 0, 0, 0, 0, 0, 0 };

        public double[,] GetGradePercentage(int[,] gradeCounts)
        {

            for (int i = 0; i < gradeCounts.GetLength(0); i++)
            {
                for (int j = 0; j < gradeCounts.GetLength(1); j++)
                {
                    total[i] += gradeCounts[i, j];

                }
            }

            for (int i = 0; i < gradeCounts.GetLength(0); i++)
            {
                for (int j = 0; j < gradeCounts.GetLength(1); j++)
                {
                    if (total[i] != 0)
                        gradePercentage[i, j] = Math.Round((double)((gradeCounts[i, j] / total[i]) * 100), 2);

                }
            }

            return gradePercentage;
        }

        public int[,] GetGradeCounts(int subjectId, string examYear, string form)
        {
            string tovYear = (Convert.ToInt32(examYear) - 1).ToString();

            string tovMarkType = "";

            string tovForm = "";

            if (form == "peralihan" || form == "1")
            {
                tovMarkType = "AR2";

                tovForm = form;
            }
            else if (form == "2" || form == "3")
            {
                tovMarkType = "Markah Akhir Tahun";

                tovForm = (Convert.ToInt32(form) - 1).ToString();
            }


            string getTovSql = "SELECT tov as marks FROM Target where examYear = '" + examYear + "' AND studentForm = '" + form + "' AND subjectId = '" + subjectId + "'";


            ArrayList rows = DbConfig.Retrieve(getTovSql);

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    string tov = cols["marks"].ToString();
                    int tovGrade = 0;

                    if (tov != null || tov != "")
                    {
                        tovGrade = Result.GetGrade(tov, "Menengah Bawah");
                    }
                    else
                    {
                        //if tov is null, find in previous exam
                        string getTovExamSql = "select marks from Result r, SubjectClasses sc, ExamSubjects es, Exam e, Class c where r.subjectClassId = sc.subjectClassId and sc.classId = c.classId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and examYear = '" + tovYear + "' and subjectId = '" + subjectId + "' and examMarkType = '" + tovMarkType + "' and classForm = '" + tovForm + "'";

                        ArrayList tovExamRows = DbConfig.Retrieve(getTovExamSql);

                        if (tovExamRows != null)
                        {
                            foreach (IDictionary targetExamCols in tovExamRows)
                            {
                                tov = targetExamCols["marks"].ToString();
                            }

                            tovGrade = Result.GetGrade(tov, "Menengah Bawah");

                        }
                        else
                        {
                            tovGrade = Result.GetGrade(tov, "Menengah Bawah");
                        }
                    }

                    gradeCounts[0, tovGrade] += 1;
                }
            }

            string getTargetSql = "select * from Target t where subjectId = '" + subjectId + "' and examYear = '" + examYear + "' and studentForm = '" + form + "'";

            ArrayList targetRows = DbConfig.Retrieve(getTargetSql);

            if (targetRows != null)
            {
                foreach (IDictionary cols in targetRows)
                {
                    int oti1Grade = Result.GetGrade(cols["oti1"].ToString(), "Menengah Bawah");

                    gradeCounts[1, oti1Grade] += 1;

                    int oti2Grade = Result.GetGrade(cols["oti2"].ToString(), "Menengah Bawah");

                    gradeCounts[3, oti2Grade] += 1;

                    int etrGrade = Result.GetGrade(cols["etr"].ToString(), "Menengah Bawah");

                    gradeCounts[5, etrGrade] += 1;
                }
            }

            string getMarkSql = "select marks, examMarkType from Result r, SubjectClasses sc, ExamSubjects es, Exam e, Class c where r.subjectClassId = sc.subjectClassId and sc.classId = c.classId and sc.examSubjectId = es.examSubjectId and es.examId = e.examId and examYear = '" + examYear + "' and subjectId = '" + subjectId + "' and classForm = '" + form + "'";

            ArrayList markRows = DbConfig.Retrieve(getMarkSql);

            if (markRows != null)
            {
                foreach (IDictionary cols in markRows)
                {
                    string examMarkType = cols["examMarkType"].ToString();

                    if (examMarkType == "AR1")
                    {
                        int ar1Grade = Result.GetGrade(cols["marks"].ToString(), "Menengah Bawah");
                        gradeCounts[2, ar1Grade] += 1;
                    }
                    else if (examMarkType == "AR2")
                    {
                        int ar2Grade = Result.GetGrade(cols["marks"].ToString(), "Menengah Bawah");
                        gradeCounts[4, ar2Grade] += 1;
                    }
                    else if (examMarkType == "Markah Akhir Tahun")
                    {
                        int finalGrade = Result.GetGrade(cols["marks"].ToString(), "Menengah Bawah");
                        gradeCounts[6, finalGrade] += 1;
                    }
                }
            }

            return gradeCounts;
        }

    }

}