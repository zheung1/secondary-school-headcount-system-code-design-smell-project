using Headcount.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Headcount.Models
{
    public class Admin : User
    {
        private int adminId,
            isSystemOwner;

        private string adminName,
            adminHomeTel,
            adminOfficeTel,
            adminHp,
            adminAddress,
            adminEmail,
            adminRemarks;

        public int AdminId { get; set; }
        public int IsSystemOwner { get; set; }
        public string AdminName { get; set; }

        public string AdminHomeTel { get; set; }
        public string AdminOfficeTel { get; set; }
        public string AdminHp { get; set; }
        public string AdminAddress { get; set; }
        public string AdminEmail { get; set; }
        public string AdminRemarks { get; set; }

        public List<Admin> GetAdminList(int start, int length, string searchValue, string sortColumnName, string sortDirection)
        {

            string sql = "SELECT * FROM Admin g, Users u WHERE g.userId = u.userId AND g.uuid = '" + this.Uuid + "' AND adminName LIKE '%" + searchValue + "%' ORDER BY " + sortColumnName + " " + sortDirection + " OFFSET " + start + " ROWS FETCH NEXT " + length + " ROWS ONLY";

            List<Admin> admins = new List<Admin>();

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {

                    Admin admin = new Admin();

                    admin.UserId = Convert.ToInt32(col["userId"]);
                    admin.Username = col["username"].ToString();
                    admin.AdminEmail = col["adminEmail"].ToString();
                    admin.AdminName = col["adminName"].ToString();
                    admin.AdminOfficeTel = col["adminOfficeTel"].ToString();
                    admin.AdminHp = col["adminHp"].ToString();

                    admins.Add(admin);

                }
            }

            return admins;
        }

        public int GetAdminCount()
        {
            string sql = "SELECT count(adminId) as adminCount FROM Users u, Admin g WHERE u.userId = g.userId and g.uuid = '" + this.Uuid + "'";

            int adminCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    adminCount = Convert.ToInt32(col["adminCount"]);
                }
            }

            return adminCount;
        }

        public int GetFilteredCount(string searchValue)
        {
            string sql = "SELECT count(adminId) as adminCount FROM Admin WHERE uuid = '" + this.Uuid + "' AND adminName LIKE '%" + searchValue + "%'";

            int adminCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    adminCount = Convert.ToInt32(col["adminCount"]);
                }
            }

            return adminCount;
        }

        public ArrayList GetAdminList ()
        {
            string sql = "SELECT * FROM Users u, Admin a WHERE u.userId = a.userId AND u.uuid = '"+this.Uuid+"'";

            ArrayList admins = new ArrayList();

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {

                    Admin admin = new Admin();

                    admin.UserId = Convert.ToInt32(col["userId"]);
                    admin.Username = col["username"].ToString();
                    admin.AdminEmail = col["adminEmail"].ToString();
                    admin.AdminName = col["adminName"].ToString();
                    admin.AdminOfficeTel = col["adminOfficeTel"].ToString();
                    admin.AdminHp = col["adminHp"].ToString();

                    admins.Add(admin);

                }
            }

            return admins;
        }

        public Admin GetAdmin(int id)
        {
            string sql = "SELECT * FROM Users u, Admin a WHERE u.userId = a.userId AND u.userId = " + id;

            ArrayList rows = DbConfig.Retrieve(sql);
            
            Admin admin = new Admin();

            foreach (IDictionary cols in rows)
            {
                admin.UserId = Convert.ToInt32(cols["userId"]);
                admin.Username = cols["username"].ToString();
                admin.Password = cols["password"].ToString();
                admin.AdminEmail = cols["adminEmail"].ToString();
                admin.AdminId = Convert.ToInt32(cols["adminId"]);
                admin.AdminName = cols["adminName"].ToString();
                admin.AdminHomeTel = cols["adminHomeTel"].ToString();
                admin.AdminOfficeTel = cols["adminOfficeTel"].ToString();
                admin.AdminHp = cols["adminHp"].ToString();
                admin.AdminRemarks = cols["adminRemarks"].ToString();
                admin.AdminAddress = cols["adminAddress"].ToString();
            }

            return admin;
        }

        public void UpdateAdmin()
        {
            string updateUserSql = "UPDATE Users SET username = '" + this.Username + "', password = '" + this.Password + "' WHERE userId = " + this.UserId;

            DbConfig.UpdateData(updateUserSql);

            string sql = "UPDATE Admin SET adminName = '" + this.AdminName + "', adminEmail = '" + this.AdminEmail + "', adminHomeTel = '" + this.AdminHomeTel + "', adminOfficeTel = '" + this.AdminOfficeTel + "', adminHp = '" + this.AdminHp + "', adminRemarks = '" + this.AdminRemarks + "', adminAddress = '" + this.AdminAddress + "' WHERE userId = " + this.UserId;

            DbConfig.UpdateData(sql);
        }

        public void SaveAdmin()
        {
            string insertUserSql = "INSERT INTO Users (username, password, type, uuid) Output Inserted.userId VALUES ('" + this.Username + "', '" + this.Password + "','" + this.Type + "', '" + this.Uuid + "')";

            int id = DbConfig.InsertData(insertUserSql);

            if (id != 0)
            {
                string insertAdministratorSql = "INSERT INTO Admin (adminName, adminHomeTel, adminOfficeTel, adminHp, adminEmail, adminRemarks, adminAddress, userId, uuid) Output Inserted.adminId VALUES ('" + this.AdminName + "', '" + this.AdminHomeTel + "', '" + this.AdminOfficeTel + "', '" + this.AdminHp + "' , '" + this.AdminEmail + "', '" + this.AdminRemarks + "', '" + this.AdminAddress + "','" + id + "', '" + this.Uuid + "')";

                DbConfig.InsertData(insertAdministratorSql);
            }
        }

        public ArrayList GetAdminDropdownList()
        {
            ArrayList dropdownOptions = new ArrayList();

            string sql = "SELECT * FROM Users u, Admin a WHERE u.userId = a.userId";

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {

                    DropdownOption dropdownOption = new DropdownOption();

                    dropdownOption.id = Convert.ToInt32(cols["adminId"]);
                    dropdownOption.username = cols["username"].ToString();
                    dropdownOption.text = cols["adminName"].ToString() + " (" + dropdownOption.username + ")";

                    dropdownOptions.Add(dropdownOption);

                }
            }

            return dropdownOptions;
        }

        public int GetOwnerId()
        {
            string getOwnerIdSql = "select a.adminId from Users u, Admin a where u.userId = a.userId and u.uuid = '" + this.Uuid + "' and isSystemOwner = 1";

            int ownerId = 0;

            ArrayList rows = DbConfig.Retrieve(getOwnerIdSql);

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    ownerId = Convert.ToInt32(cols["adminId"]);
                }
            }

            return ownerId;
        }

        public void DeleteAdmin(int ownerId, int userId)
        {
            Admin admin = new Admin();
            admin = admin.GetAdmin(userId);

            //update class
            string updateClassSql = "Update SubjectClasses SET adminId = '" + ownerId + "' WHERE adminId = '" + admin.AdminId + "'";

            DbConfig.UpdateData(updateClassSql);

            //update subject
            string updateSubjectSql = "Update ExamSubjects SET adminId = '" + ownerId + "' WHERE adminId = '" + admin.AdminId + "'";

            DbConfig.UpdateData(updateSubjectSql);

            //update exam
            string updateExamSql = "Update Exam SET adminId = '" + ownerId + "' WHERE adminId = '" + admin.AdminId + "'";

            DbConfig.UpdateData(updateExamSql);

            //delete the selected admin

            string deleteAdminSql = "DELETE FROM Admin WHERE userId = " + userId;

            DbConfig.DeleteData(deleteAdminSql);

            string deleteUserSql = "DELETE FROM Users WHERE userId = " + userId;

            DbConfig.DeleteData(deleteUserSql);


        }
    }
}