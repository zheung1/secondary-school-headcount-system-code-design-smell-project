using Headcount.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Headcount.Models
{
    public class Teacher : User
    {
        private int teacherId,
            isSystemOwner;

        private string teacherName,
            teacherHomeTel,
            teacherOfficeTel,
            teacherHp,
            teacherAddress,
            teacherEmail,
            teacherRemarks;
            

        public int TeacherId { get; set; }
        public int IsSystemOwner { get; set; }
        public string TeacherName { get; set; }

        public string TeacherHomeTel { get; set; }
        public string TeacherOfficeTel { get; set; }
        public string TeacherHp { get; set; }
        public string TeacherAddress { get; set; }
        public string TeacherEmail { get; set; }
        public string TeacherRemarks { get; set; }

        public List<Teacher> GetTeacherList(int start, int length, string searchValue, string sortColumnName, string sortDirection)
        {

            string sql = "SELECT * FROM Teacher g, Users u WHERE g.userId = u.userId AND g.uuid = '" + this.Uuid + "' AND teacherName LIKE '%" + searchValue + "%' ORDER BY " + sortColumnName + " " + sortDirection + " OFFSET " + start + " ROWS FETCH NEXT " + length + " ROWS ONLY";

            List<Teacher> teachers = new List<Teacher>();

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {

                    Teacher teacher = new Teacher();

                    teacher.UserId = Convert.ToInt32(col["userId"]);
                    teacher.Username = col["username"].ToString();
                    teacher.TeacherEmail = col["teacherEmail"].ToString();
                    teacher.TeacherName = col["teacherName"].ToString();
                    teacher.TeacherOfficeTel = col["teacherOfficeTel"].ToString();
                    teacher.TeacherHp = col["teacherHp"].ToString();

                    teachers.Add(teacher);

                }
            }

            return teachers;
        }

        public int GetTeacherCount()
        {
            string sql = "SELECT count(teacherId) as teacherCount FROM Users u, Teacher g WHERE u.userId = g.userId and g.uuid = '" + this.Uuid + "'";

            int teacherCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    teacherCount = Convert.ToInt32(col["teacherCount"]);
                }
            }

            return teacherCount;
        }

        public int GetFilteredCount(string searchValue)
        {
            string sql = "SELECT count(teacherId) as teacherCount FROM Teacher WHERE uuid = '" + this.Uuid + "' AND teacherName LIKE '%" + searchValue + "%'";

            int teacherCount = 0;

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {
                    teacherCount = Convert.ToInt32(col["teacherCount"]);
                }
            }

            return teacherCount;
        }



        public ArrayList GetTeacherList ()
        {
            string sql = "SELECT * FROM Users u, Teacher a WHERE u.userId = a.userId and u.uuid = '"+this.Uuid+"'";

            ArrayList teachers = new ArrayList();

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary col in rows)
                {

                    Teacher teacher = new Teacher();

                    teacher.UserId = Convert.ToInt32(col["userId"]);
                    teacher.Username = col["username"].ToString();
                    teacher.TeacherEmail = col["teacherEmail"].ToString();
                    teacher.TeacherName = col["teacherName"].ToString();
                    teacher.TeacherOfficeTel = col["teacherOfficeTel"].ToString();
                    teacher.TeacherHp = col["teacherHp"].ToString();

                    teachers.Add(teacher);

                }
            }

            return teachers;
        }

        public Teacher GetTeacher(int id)
        {
            string sql = "SELECT * FROM Users u, Teacher a WHERE u.userId = a.userId AND u.userId = " + id;

            ArrayList rows = DbConfig.Retrieve(sql);
            
            Teacher teacher = new Teacher();

            foreach (IDictionary cols in rows)
            {
                teacher.UserId = Convert.ToInt32(cols["userId"]);
                teacher.Username = cols["username"].ToString();
                teacher.Password = cols["password"].ToString();
                teacher.TeacherEmail = cols["teacherEmail"].ToString();
                teacher.TeacherId = Convert.ToInt32(cols["teacherId"]);
                teacher.TeacherName = cols["teacherName"].ToString();
                teacher.TeacherHomeTel = cols["teacherHomeTel"].ToString();
                teacher.TeacherOfficeTel = cols["teacherOfficeTel"].ToString();
                teacher.TeacherHp = cols["teacherHp"].ToString();
                teacher.TeacherRemarks = cols["teacherRemarks"].ToString();
                teacher.TeacherAddress = cols["teacherAddress"].ToString();
            }

            return teacher;
        }

        public void UpdateTeacher()
        {
            string updateUserSql = "UPDATE Users SET username = '" + this.Username + "', password = '" + this.Password + "' WHERE userId = " + this.UserId;

            DbConfig.UpdateData(updateUserSql);

            string sql = "UPDATE Teacher SET teacherName = '" + this.TeacherName + "', teacherEmail = '" + this.TeacherEmail + "', teacherHomeTel = '" + this.TeacherHomeTel + "', teacherOfficeTel = '" + this.TeacherOfficeTel + "', teacherHp = '" + this.TeacherHp + "', teacherRemarks = '" + this.TeacherRemarks + "', teacherAddress = '" + this.TeacherAddress + "' WHERE userId = " + this.UserId;

            DbConfig.UpdateData(sql);
        }

        public void SaveTeacher()
        {
            string insertUserSql = "INSERT INTO Users (username, password, type, uuid) Output Inserted.userId VALUES ('" + this.Username + "', '" + this.Password + "','" + this.Type + "', '" + this.Uuid + "')";

            int id = DbConfig.InsertData(insertUserSql);

            if (id != 0)
            {
                string insertTeacherSql = "INSERT INTO Teacher (teacherName, teacherHomeTel, teacherOfficeTel, teacherHp, teacherEmail, teacherRemarks, teacherAddress, userId, uuid) Output Inserted.teacherId VALUES ('" + this.TeacherName + "', '" + this.TeacherHomeTel + "', '" + this.TeacherOfficeTel + "', '" + this.TeacherHp + "' , '" + this.TeacherEmail + "', '" + this.TeacherRemarks + "', '" + this.TeacherAddress + "','" + id + "', '" + this.Uuid + "')";

                DbConfig.InsertData(insertTeacherSql);
            }
        }

        public ArrayList GetTeacherDropdownList()
        {
            ArrayList dropdownOptions = new ArrayList();

            string sql = "SELECT * FROM Users u, Teacher a WHERE u.userId = a.userId";

            ArrayList rows = DbConfig.Retrieve(sql);

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {

                    DropdownOption dropdownOption = new DropdownOption();

                    dropdownOption.id = Convert.ToInt32(cols["teacherId"]);
                    dropdownOption.username = cols["username"].ToString();
                    dropdownOption.text = cols["teacherName"].ToString() + " (" + dropdownOption.username + ")";

                    dropdownOptions.Add(dropdownOption);

                }
            }

            return dropdownOptions;
        }

        public int GetOwnerId()
        {
            string getOwnerIdSql = "select t.teacherId from Users u, Teacher t where u.userId = t.userId and u.uuid = '" + this.Uuid + "' and isSystemOwner = 1";

            int ownerId = 0;

            ArrayList rows = DbConfig.Retrieve(getOwnerIdSql);

            if (rows != null)
            {
                foreach (IDictionary cols in rows)
                {
                    ownerId = Convert.ToInt32(cols["teacherId"]);
                }
            }

            return ownerId;
        }

        public void DeleteTeacher(int ownerId, int userId)
        {
            Teacher teacher = new Teacher();
            teacher = teacher.GetTeacher(userId);

            //update class
            string updateClassSql = "Update SubjectClasses SET teacherId = '" + ownerId + "' WHERE teacherId = '" + teacher.TeacherId + "'";

            DbConfig.UpdateData(updateClassSql);

            //delete the selected teacher

            string deleteTeacherSql = "DELETE FROM Teacher WHERE userId = " + userId;

            DbConfig.DeleteData(deleteTeacherSql);

            string deleteUserSql = "DELETE FROM Users WHERE userId = " + userId;

            DbConfig.DeleteData(deleteUserSql);

        }
    }
}